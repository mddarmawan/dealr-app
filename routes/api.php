<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| API Kasir
|--------------------------------------------------------------------------
*/
Route::post('kasir/showroom/last', ['middleware' => 'cors','uses'=>'Api\Kasir\ApiShowroomController@getLast']);
Route::post('kasir/showroom/delete/{id}', ['middleware' => 'cors','uses'=>'Api\Kasir\ApiShowroomController@destroy']);
Route::post('kasir/showroom/{id}', ['middleware' => 'cors','uses'=>'Api\Kasir\ApiShowroomController@update']);
Route::post('kasir/showroom', ['middleware' => 'cors','uses'=>'Api\Kasir\ApiShowroomController@store']);
Route::get('kasir/showroom/{startDate}/{endDate}', ['middleware' => 'cors','uses'=>'Api\Kasir\ApiShowroomController@dateFilter']);
Route::get('kasir/showroom', ['middleware' => 'cors','uses'=>'Api\Kasir\ApiShowroomController@data']);

Route::get('kasir/operasional/{startDate}/{endDate}', ['middleware' => 'cors','uses'=>'Api\Kasir\ApiOperasionalController@dateFilter']);
Route::get('kasir/operasional', ['middleware' => 'cors','uses'=>'Api\Kasir\ApiOperasionalController@data']);

/*
|--------------------------------------------------------------------------
| API Umum
|--------------------------------------------------------------------------
*/
Route::get('/umum/pelanggan', 	['middleware' => 'cors','uses'=>'Api\Umum\ApiPelangganController@data']);

Route::post('/umum/nospk/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\Umum\ApiNoSPKController@destroy']);
Route::post('/umum/nospk', 	['middleware' => 'cors','uses'=>'Api\Umum\ApiNoSPKController@store']);
Route::get('/umum/nospk', 	['middleware' => 'cors','uses'=>'Api\Umum\ApiNoSPKController@data']);

/*
|--------------------------------------------------------------------------
| API Persediaan Kendaraan
|--------------------------------------------------------------------------
*/
Route::post('persediaan/stok/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\Persediaan\ApiStokController@destroy']);
Route::post('persediaan/stok/{id}', ['middleware' => 'cors','uses'=>'Api\Persediaan\ApiStokController@update']);
Route::post('persediaan/stok/get/{id}', ['middleware' => 'cors','uses'=>'Api\Persediaan\ApiStokController@getOne']);
// Route::post('persediaan/stok/{id}', 		['middleware' => 'cors','uses'=>'Api\Persediaan\ApiStokController@update']);
Route::post('persediaan/stok', 			['middleware' => 'cors','uses'=>'Api\Persediaan\ApiStokController@store']);
Route::get('persediaan/stok',			['middleware' => 'cors','uses'=>'Api\Persediaan\ApiStokController@data']);

Route::post('persediaan/pindah/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\Persediaan\ApiPindahController@destroy']);
Route::post('persediaan/pindah/{id}', 		['middleware' => 'cors','uses'=>'Api\Persediaan\ApiPindahController@update']);
Route::post('persediaan/pindah', 			['middleware' => 'cors','uses'=>'Api\Persediaan\ApiPindahController@store']);
Route::get('persediaan/pindah',			['middleware' => 'cors','uses'=>'Api\Persediaan\ApiPindahController@data']);
/*
|--------------------------------------------------------------------------
| API Pembelian Kendaraan
|--------------------------------------------------------------------------
*/
Route::post('pembelian/import/rrn', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiRRNController@import']);
Route::post('pembelian/import/onhand', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiOnHandController@import']);
Route::post('pembelian/import/beli', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiPembelianController@import']);

Route::get('pembelian/pembelian/{startDate}/{endDate}', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiPembelianController@dateFilter']);
Route::post('pembelian/pembelian/delete/{id}', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiPembelianController@destroy']);
Route::post('pembelian/pembelian/{id}', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiPembelianController@update']);
Route::post('pembelian/pembelian', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiPembelianController@store']);
Route::get('pembelian/pembelian', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiPembelianController@data']);

Route::post('pembelian/retur/delete/{id}', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiReturController@destroy']);
Route::post('pembelian/retur/{id}', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiReturController@update']);
Route::post('pembelian/retur', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiReturController@store']);
Route::get('pembelian/retur', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiReturController@data']);

Route::post('pembelian/pesanan/delete/{id}', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiPesananController@destroy']);
Route::post('pembelian/pesanan/{id}', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiPesananController@update']);
Route::post('pembelian/pesanan', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiPesananController@store']);
Route::get('pembelian/pesanan', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiPesananController@data']);

Route::post('pembelian/rrn/delete/{id}', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiRRNController@destroy']);
Route::post('pembelian/rrn/{id}', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiRRNController@update']);
Route::post('pembelian/rrn', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiRRNController@store']);
Route::get('pembelian/rrn', ['middleware' => 'cors','uses'=>'Api\Pembelian\ApiRRNController@data']);

/*
|--------------------------------------------------------------------------
| API PENJUALAN
|--------------------------------------------------------------------------
*/

Route::get('penjualan/matching/{startDate}/{endDate}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiMatchingController@dateFilter']);
Route::post('penjualan/matching/delete/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiMatchingController@destroy']);
Route::post('penjualan/matching/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiMatchingController@update']);
Route::post('penjualan/matching', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiMatchingController@store']);
Route::get('penjualan/matching', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiMatchingController@data']);

Route::get('penjualan/samsat/{startDate}/{endDate}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiProsesSamsatController@dateFilter']);
Route::post('penjualan/samsat/delete/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiProsesSamsatController@destroy']);
Route::post('penjualan/samsat/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiProsesSamsatController@update']);
Route::post('penjualan/samsat', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiProsesSamsatController@store']);
Route::get('penjualan/samsat', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiProsesSamsatController@data']);

Route::get('penjualan/leasing/{startDate}/{endDate}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiTagihanLeasingController@dateFilter']);
Route::post('penjualan/leasing/delete/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiTagihanLeasingController@destroy']);
Route::post('penjualan/leasing/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiTagihanLeasingController@update']);
Route::post('penjualan/leasing', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiTagihanLeasingController@store']);
Route::get('penjualan/leasing', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiTagihanLeasingController@data']);

Route::post('penjualan/diskon/delete/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiDiskonsController@destroy']);
Route::post('penjualan/diskon/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiDiskonsController@update']);
Route::post('penjualan/diskon', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiDiskonsController@store']);
Route::get('penjualan/diskon/{filter}/{startDate?}/{endDate?}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiDiskonsController@filter']);
Route::get('penjualan/diskon', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiDiskonsController@data']);

Route::get('penjualan/spk/get/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@getOne']);
Route::get('penjualan/spk/DO/last', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@getLastDO']);
Route::get('penjualan/spk/PELANGGAN/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@pelanggan']);
Route::post('penjualan/spk/SALES/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@sales']);
Route::post('penjualan/spk/MANUAL/matching/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@matching']);
Route::post('penjualan/spk/terbitkan/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@terbitkan']);
Route::post('penjualan/spk/terima/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@terima_spk']);
Route::post('penjualan/spk/revisi/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@revisi_spk']);
Route::post('penjualan/spk/delete/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@destroy']);
Route::post('penjualan/spk/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@update']);
Route::post('penjualan/spk', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@store']);
Route::get('penjualan/spk/{filter}/{startDate?}/{endDate?}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@filter']);
Route::get('penjualan/spk', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@data']);
Route::get('penjualan/aksesoris/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@data_aksesoris']);
Route::get('penjualan/bayar/{id}', ['middleware' => 'cors','uses'=>'Api\Penjualan\ApiSPKController@data_bayar']);


/*
|--------------------------------------------------------------------------
| API UMUM
|--------------------------------------------------------------------------
*/
Route::post('kontak/tipe/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiKontakTipeController@destroy']);
Route::post('kontak/tipe/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiKontakTipeController@update']);
Route::post('kontak/tipe', 			['middleware' => 'cors','uses'=>'Api\ApiKontakTipeController@store']);
Route::get('kontak/tipe',			['middleware' => 'cors','uses'=>'Api\ApiKontakTipeController@data']);
Route::post('kontak/klasifikasi/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiKontakKategoriController@destroy']);
Route::post('kontak/klasifikasi/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiKontakKategoriController@update']);
Route::post('kontak/klasifikasi', 			['middleware' => 'cors','uses'=>'Api\ApiKontakKategoriController@store']);
Route::get('kontak/klasifikasi',		['middleware' => 'cors','uses'=>'Api\ApiKontakKategoriController@data']);
Route::post('kontak/id/{kode}', 			['middleware' => 'cors','uses'=>'Api\ApiKontakController@generateid']);
Route::post('kontak/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiKontakController@destroy']);
Route::post('kontak/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiKontakController@update']);
Route::post('kontak', 			['middleware' => 'cors','uses'=>'Api\ApiKontakController@store']);
Route::get('kontak',				['middleware' => 'cors','uses'=>'Api\ApiKontakController@data']);
Route::post('gudang/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiGudangController@destroy']);
Route::post('gudang/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiGudangController@update']);
Route::post('gudang', 			['middleware' => 'cors','uses'=>'Api\ApiGudangController@store']);
Route::get('gudang',				['middleware' => 'cors','uses'=>'Api\ApiGudangController@data']);
/*
|--------------------------------------------------------------------------
| API BARANG
|--------------------------------------------------------------------------
// */
Route::post('/aksesoris/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiAksesorisController@destroy']);
Route::post('aksesoris/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiAksesorisController@update']);
Route::post('aksesoris', 			['middleware' => 'cors','uses'=>'Api\ApiAksesorisController@store']);
Route::get('aksesoris',				['middleware' => 'cors','uses'=>'Api\ApiAksesorisController@data']);
Route::post('warna/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiWarnaController@destroy']);
Route::post('warna/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiWarnaController@update']);
Route::post('warna', 			['middleware' => 'cors','uses'=>'Api\ApiWarnaController@store']);
Route::get('warna',				['middleware' => 'cors','uses'=>'Api\ApiWarnaController@data']);

/*
|--------------------------------------------------------------------------
| API KEPEGAWAIAN
|--------------------------------------------------------------------------
*/
Route::post('grade/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiGradeController@destroy']);
Route::post('grade/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiGradeController@update']);
Route::post('grade', 			['middleware' => 'cors','uses'=>'Api\ApiGradeController@store']);
Route::get('grade',				['middleware' => 'cors','uses'=>'Api\ApiGradeController@data']);
Route::post('jabatan/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiJabatanController@destroy']);
Route::post('jabatan/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiJabatanController@update']);
Route::post('jabatan', 			['middleware' => 'cors','uses'=>'Api\ApiJabatanController@store']);
Route::get('jabatan',				['middleware' => 'cors','uses'=>'Api\ApiJabatanController@data']);
Route::post('team/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiTeamController@destroy']);
Route::post('team/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiTeamController@update']);
Route::post('team', 			['middleware' => 'cors','uses'=>'Api\ApiTeamController@store']);
Route::get('team',				['middleware' => 'cors','uses'=>'Api\ApiTeamController@data']);
Route::post('bank/kategori/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiBankKategoriController@destroy']);
Route::post('bank/kategori/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiBankKategoriController@update']);
Route::post('bank/kategori', 			['middleware' => 'cors','uses'=>'Api\ApiBankKategoriController@store']);
Route::get('bank/kategori',				['middleware' => 'cors','uses'=>'Api\ApiBankKategoriController@data']);

Route::post('karyawan/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiKaryawanController@destroy']);
Route::post('karyawan/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiKaryawanController@update']);
Route::post('karyawan', 			['middleware' => 'cors','uses'=>'Api\ApiKaryawanController@store']);
Route::get('karyawan',				['middleware' => 'cors','uses'=>'Api\ApiKaryawanController@data']);

Route::post('salesman/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiSalesmanController@set']);
Route::get('salesman',				['middleware' => 'cors','uses'=>'Api\ApiSalesmanController@data']);

Route::post('safa/show/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiSafaController@show']);
Route::post('safa/{id}/{kode}', 		['middleware' => 'cors','uses'=>'Api\ApiSafaController@set']);
Route::get('safa',				['middleware' => 'cors','uses'=>'Api\ApiSafaController@data']);
Route::post('salmon/show/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiSalmonController@show']);
Route::post('salmon/{id}/{kode}', 		['middleware' => 'cors','uses'=>'Api\ApiSalmonController@set']);
Route::get('salmon',				['middleware' => 'cors','uses'=>'Api\ApiSalmonController@data']);




/*
|--------------------------------------------------------------------------
| API Kendaraan Type
|--------------------------------------------------------------------------
*/
Route::post('kendaraan/tipe/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiKendaraanTypeController@destroy']);
Route::post('kendaraan/tipe/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiKendaraanTypeController@update']);
Route::post('kendaraan/tipe', 			['middleware' => 'cors','uses'=>'Api\ApiKendaraanTypeController@store']);
Route::get('kendaraan/tipe',			['middleware' => 'cors','uses'=>'Api\ApiKendaraanTypeController@data']);

/*
|--------------------------------------------------------------------------
| API Kendaraan Variant
|--------------------------------------------------------------------------
*/
Route::post('kendaraan/variant/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiKendaraanVariantController@destroy']);
Route::post('kendaraan/variant/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiKendaraanVariantController@update']);
Route::post('kendaraan/variant', 			['middleware' => 'cors','uses'=>'Api\ApiKendaraanVariantController@store']);
Route::get('kendaraan/variant',			['middleware' => 'cors','uses'=>'Api\ApiKendaraanVariantController@data']);

/*
|--------------------------------------------------------------------------
| API Kendaraan Warna
|--------------------------------------------------------------------------
*/
Route::post('kendaraan/warna/delete/{id}', 	['middleware' => 'cors','uses'=>'Api\ApiKendaraanWarnaController@destroy']);
Route::post('kendaraan/warna/{id}', 		['middleware' => 'cors','uses'=>'Api\ApiKendaraanWarnaController@update']);
Route::post('kendaraan/warna', 			['middleware' => 'cors','uses'=>'Api\ApiKendaraanWarnaController@store']);
Route::get('kendaraan/warna',			['middleware' => 'cors','uses'=>'Api\ApiKendaraanWarnaController@data']);
