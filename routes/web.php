<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

/*
|--------------------------------------------------------------------------
| CRUD Pengaturan
|--------------------------------------------------------------------------
*/

Route::get('/p_kontak', 'UmumController@kontak');
Route::get('/p_warna', 'UmumController@warna');
Route::get('/p_tipe_kontak', 'UmumController@tipe');
Route::get('/p_klasifikasi_kontak', 'UmumController@klasifikasi');
Route::get('/p_gudang', 'UmumController@gudang');
Route::get('/p_bank_kategori', 'UmumController@bank');
Route::get('/p_aksesoris', 'KendaraanController@aksesoris');
Route::get('/p_tipe_kendaraan', 'KendaraanController@tipe');
Route::get('/p_variasi_kendaraan', 'KendaraanController@variant');
Route::get('/p_warna_kendaraan', 'KendaraanController@warna');
Route::get('/p_harga_kendaraan', 'KendaraanController@harga');
Route::get('/p_jabatan', 'KepegawaianController@jabatan');
Route::get('/p_grade', 'KepegawaianController@grade');
Route::get('/p_team', 'KepegawaianController@team');
Route::get('/p_karyawan', 'KepegawaianController@karyawan');
Route::get('/p_salesman', 'KepegawaianController@salesman');
Route::get('/p_safa', 'PenggunaController@safa');
Route::get('/p_salmon', 'PenggunaController@salmon');

Route::get('/j_permintaan', 'PenjualanController@permintaan');
Route::get('/j_pesanan', 'PenjualanController@pesanan');
Route::get('/j_diskon', 'PenjualanController@diskon');
Route::get('/j_matching', 'PenjualanController@matching');
Route::get('/j_leasing', 'PenjualanController@leasing');
Route::get('/j_penjualan', 'PenjualanController@penjualan');
Route::get('/j_samsat', 'PenjualanController@samsat');

Route::get('/b_pesanan', 'PembelianController@pesanan');
Route::get('/b_pembelian', 'PembelianController@pembelian');
Route::get('/b_retur', 'PembelianController@retur');
Route::get('/b_rrn', 'PembelianController@rrn');

Route::get('/i_stok', 'PersediaanController@stok');
Route::get('/i_pindah', 'PersediaanController@pindah');

Route::get('/k_showroom', 'KasirController@showroom');
Route::get('/k_operasional', 'KasirController@operasional');

Route::get('/nospk', 'UmumController@nospk');
Route::get('/pelanggan', 'UmumController@pelanggan');