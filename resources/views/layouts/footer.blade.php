	<script type="text/javascript" src="{{ asset('bower_components') }}/jquery/dist/jquery.min.js"></script>

	<script type="text/javascript" src="{{ asset('assets') }}/lib/tether/js/tether.min.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/cldr.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/cldr/event.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/cldr/supplemental.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/globalize.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/globalize/message.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/globalize/number.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/globalize/currency.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/globalize/date.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/dxgrid/jszip.min.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/lib/dxgrid/dx.all.js"></script>

	<script type="text/javascript" src="{{ asset('assets') }}/js/app.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/js/extra.js"></script>
	<script type="text/javascript" src="{{ asset('assets') }}/js/helper.js"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			App.init();
		});
	</script>

		@yield('scripts')
