	<div class="header_encar">
		<nav class="navbar mai-sub-header">
			<div class="container">
				<!-- Mega Menu structure-->
				<nav class="navbar navbar-toggleable-sm x-nav-fixed">
					<div id="mai-navbar-collapse" class="navbar-collapse collapse mai-nav-tabs">
						<ul class="nav navbar-nav">
							<?php
								if (count($modul)>0){
									$pos = 1;
									foreach($modul as $m){
								?>
								<li id="{{$m['modul_nama']}}" class="nav-item parent <?php echo $pos==1?"open":""; ?>">
									<a href="#" role="button" aria-expanded="false" class="nav-link"><span>{{$m['modul_nama']}}</span></a>
								<?php
										if (count($m['modul_sub'])>0){
											echo '<ul class="mai-nav-tabs-sub mai-sub-nav nav">';
											foreach($m['modul_sub'] as $s){
													if (count($s['modul_child'])>0){
														?>
														<li class="nav-item dropdown mega-menu parent">
															<a href="<?php echo $s['modul_link'] != null?$s['modul_link']:"#"; ?>" data-toggle="dropdown" class="nav-link"><span class="name">{{$s['modul_nama']}}</span></a>
															<div role="menu" class="dropdown-menu mai-mega-menu mai-sub-nav">
																	<div class="mai-mega-menu-row">
																<?php
																	$single="";
																	foreach($s['modul_child'] as $c){
																		if (count($c['modul_item'])>0){
																			?>
																				<div class="mai-mega-menu-column">
																					<div class="mai-mega-menu-section parent">
																							<a  class="nav-link"><span class="icon s7-angle-right"></span><span>{{$c['modul_nama']}}</span></a>
																							<div class="mai-mega-menu-sub-items mai-sub-nav">
																								<?php
																									foreach($c['modul_item'] as $i){
																								?>
																										<a href="<?php echo $i->modul_link != null?$i->modul_link:"#"; ?>" class="dropdown-item">{!!$i->modul_nama!!}</a>
																								<?php
																									}
																								 ?>
																							</div>
																					</div>
																				</div>
																			<?php
																		}else{
																			$single.="<a href='$c[modul_link]' class='dropdown-item'>$c[modul_nama]</a>";
																		}
																	}

																	if ($single!=""){
																	?>
																	<div class="mai-mega-menu-column">
																		<div class="mai-mega-menu-section parent">
																			<div class="mai-mega-menu-sub-items mai-sub-nav">
																			{!!$single!!}
																			</div>
																		</div>
																	</div>
																	<?php
																	}
																?>
																</div>
															</div>
														</li>
														<?php
													}else{
								?>
									<li class="nav-item"><a href="<?php echo $s['modul_link'] != null?$s['modul_link']:"#"; ?>" class="nav-link">
										<span class="name">{{$s['modul_nama']}}</span></a>
									</li>
								<?php
												}
											}
											echo "</ul>";
										}
										echo "</li>";
										$pos++;
									}
								}
							 ?>

						</ul>
					</div>

					<ul class="nav navbar-nav float-lg-right mai-user-nav">
          <li class="dropdown nav-item">
						<a href="#" data-toggle="dropdown" role="button" aria-expanded="true" class="dropdown-toggle nav-link"><span class="user-name">Administrator</span> <span class="angle-down s7-angle-down"></span></a>
            <div role="menu" class="dropdown-menu">
							<a href="#" class="dropdown-item"><span class="icon s7-user"> </span>Profile</a>
							<a href="#" class="dropdown-item"> <span class="icon s7-power"> </span>Log Out</a>
						</div>
          </li>
        </ul>
				</nav>
			</div>
			<div class="box-logo">
				<img class="logo_encar" src="assets/img/logo.png">
			</div>
		</nav>
	</div>
