	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="{{ asset('assets') }}/img/favicon.html">
	<title>Encar Daihatsu</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/lib/stroke-7/style.css"/>
	<link type="text/css" href="{{ asset('assets') }}/css/app.css" rel="stylesheet">
	<link type="text/css" href="{{ asset('assets') }}/css/custom.css" rel="stylesheet">

	<link type="text/css" rel="stylesheet" href="{{ asset('assets') }}/lib/dxgrid/dx.common.css" />
	<link type="text/css" rel="dx-theme" data-theme="generic.light.compact" href="{{ asset('assets') }}/lib/dxgrid/dx.light.compact.css" />
	<link type="text/css" href="{{ asset('assets') }}/lib/dxgrid/style.css" rel="stylesheet">
	<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
