@extends('layouts.app')

@section('content')

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Kasir Showroom</span><br/><small>Kasir \ Showroom</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>
	
	<div id="x-custom-add"></div>

@endsection

@section('scripts')

	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/FirebaseController.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/kasir/ShowroomController.js') }}" type="text/javascript"></script>
	{{-- <script src="{{ asset('/js/kasir/ShowroomController.js') }}" type="text/javascript"></script> --}}

	<script type="text/javascript">
		var url_api = "{{env('API_WEB', false).'api/kasir/showroom'}}/";
		var title = $("#x-title").html();
		var spk = {!!$data['spk']!!};
		var pelanggan = {!!$data['pelanggan']!!};
		var variant = {!!$data['variant']!!};
		var warna = {!!$data['warna']!!};
		var sales = {!!$data['sales']!!};
		var team = {!!$data['team']!!};
		var akun = {!!$data['akun']!!};
		var data = getData(url_api, "spk_bayar_id");

		$("#x-data").dxDataGrid({
			dataSource				: data,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true},
			editing : {
				mode: "popup",
				popup: {
					title: "Tambah "+title,
					showTitle: true,
					width: 650,
					height: 600,
					position: {
						my: "center",
						at: "center",
						of: window
					}
				},
				form: {
					colCount: 2,
					items: [{
						itemType: "group",
						caption: "SPK",
						items: [{
							dataField: "spk_bayar_spk", 
							caption: "SPK", 
							lookup: {
								dataSource: spk, 
								displayExpr: "spk_id", 
								valueExpr: "spk_id"
							}
						}, {
							dataField: "spk_pel_id", 
							caption: "NAMA PEMESAN", 
						}, 
						{
							dataField: "spk_kendaraan", 
							caption: "VARIANT", 
						}]
					}, {
						itemType: "group",
						caption: "IKHTISAR",
						items: ["spk_harga", "jumlah", "piutang"]
					}, {
						itemType: "group",
						caption: "PEMBAYARAN",
						colSpan: 2,
						items: [{
							dataField: "spk_bayar_id", 
							caption: "NO. KWITANSI",
						}, {
							dataField: "spk_bayar_tgl", 
							caption: "TGL. BAYAR"
						}, {
							dataField: "spk_bayar_jumlah", 
							caption: "JUMLAH"
						}, {
							dataField: "spk_bayar_akun", 
							caption: "AKUN",
							lookup: {
								dataSource: akun, 
								displayExpr: "bank_an",
								valueExpr: "bank_id"
							}
						}, {
							dataField: "spk_bayar_ket", 
							caption: "KETERANGAN"
						}]
					}]
				}
			},

			columns: [
				{dataField: "spk_bayar_tgl", caption: "TGL. BAYAR", alignment: "center", width: 150, dataType: "date", format:"dd/MM/yyyy", validationRules: [{ type: "required" }]},
				{dataField: "spk_bayar_id", caption: "NO. KWITANSI", alignment: "center", allowEditing: false, width: 150},
				{
					dataField: "spk_bayar_spk", 
					caption: "SPK", 
					alignment: "center", 
					width: 150,
					lookup: {
						dataSource: spk, 
						displayExpr: "spk_id", 
						valueExpr: "spk_id"
					},
					formItem: {
						colSpan: 1,
						label: {
							text: "SPK", 
							location: "top"
						}
					}
				},
				{
					dataField: "spk_pel_id", 
					caption: "NAMA PEMESAN", 
					allowEditing: false,
					width: 140,
					lookup: {
						dataSource: pelanggan, 
						displayExpr: "pel_nama", 
						valueExpr: "pel_id"
					},
					formItem: {
						colSpan: 1,
						label: {
							text: "NAMA PEMESAN", 
							location: "top"
						}
					}
				},
				{
					dataField: "spk_kendaraan", 
					caption: "VARIANT", 
					allowEditing: false,
					width: 170,
					lookup: {
						dataSource: variant, 
						displayExpr: "variant_nama", 
						valueExpr: "variant_id"
					},
					formItem: {
						colSpan: 1,
						label: {
							text: "VARIANT", 
							location: "top"
						}
					}
				},
				{
					dataField: "warna_id", 
					caption: "WARNA", 
					width: 200,
					lookup: {
						dataSource: warna, 
						displayExpr: "warna_nama", 
						valueExpr: "warna_id"
					}
				},
				{
					dataField: "spk_sales", 
					caption: "SALES", 
					width: 150,
					lookup: {
						dataSource: sales, 
						displayExpr: "karyawan_nama", 
						valueExpr: "sales_uid"
					}
				},
				{
					dataField: "team_id", 
					caption: "TEAM", 
					width: 100,
					lookup: {
						dataSource: team, 
						displayExpr: "team_nama", 
						valueExpr: "team_id"
					}
				},
				{
					dataField: "spk_pembayaran",
					caption: "CARA BAYAR",
					width: 100,
					dataType:"boolean",
					alignment:"center",
					lookup: {
						dataSource: ["0", "1"],
						displayExpr: function (t) {
							return t == 0 ? "CASH" : "CREDIT"
						}
					},
					cellTemplate: function (container, options) {
						if (options.value == 1) {
							$("<div>")
								.append($("<span>", { "class": "dx-inactive" }).html("CASH"))
								.appendTo(container);
						} else {
							$("<div>")
								.append($("<span>", { "class": "dx-active" }).html("CREDIT"))
								.appendTo(container);
						}
					}
				},
				{
					dataField: "spk_bayar_akun", 
					caption: "AKUN", 
					width: 150,
					lookup: {
						dataSource: akun, 
						displayExpr: "bank_an",
						valueExpr: "bank_id"
					}
				},
				{dataField: "spk_bayar_jumlah", caption: "JUMLAH", width: 150, format: "currency"},
				{dataField: "spk_bayar_ket", caption: "KETERANGAN"},
				{
					dataField: "spk_harga", 
					visible: false, 
					allowEditing: false, 
					format: "currency",
					formItem: {
						colSpan: 1,
						label: {
							text: "TOTAL INVOICE", 
							location: "top"
						}
					}
				},
				{
					dataField: "jumlah", 
					visible: false, 
					allowEditing: false, 
					format: "currency",
					formItem: {
						colSpan: 1,
						label: {
							text: "TOTAL BAYAR", 
							location: "top"
						}
					}
				},
				{
					dataField: "piutang", 
					visible: false, 
					allowEditing: false, 
					format: "currency",
					formItem: {
						colSpan: 1,
						label: {
							text: "KURANG BAYAR", 
							location: "top"
						}
					}
				}
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
				items = this.getDataSource().items();
				$(".x-detail").unbind('click').click(function(event){
					index = $(this).data("index");
					id = $(this).data("id");
					showInfo(id, items[index]);
				});
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				dataGrid = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "before",
						widget: "dxButton",
						options: {
							icon: "add",
							hint: "Add",
							onClick: function() {
								dataGrid.addRow();
								$(".dx-popup-normal .dx-toolbar-before .dx-toolbar-item-content>div").html("Tambah "+title);
							}
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal']").val()=="" ? "ALL":$("input[name='x-filter-date-awal']").val();
								endDate = $("input[name='x-filter-date-akhir']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir']").val();
								$filtered = getData(url_api+startDate+"/"+endDate,"spk_id");
								dataGrid.option("dataSource", $filtered);
								dataGrid.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid.repaint();
								dataGrid.option("dataSource", data);
								dataGrid.refresh();
							}
						}
					}
				);
			},

			onEditorPreparing: function(e) {
				switch (e.dataField) {
					case "spk_pel_id":
						spk_pel_id = e.id;
						break;
						
					case "spk_kendaraan":
						spk_kendaraan = e.id;
						break;
						
					case "spk_harga":
						spk_harga = e.id;
						break;
						
					case "jumlah":
						jumlah = e.id;
						break;
						
					case "piutang":
						piutang = e.id;
						break;

					default:
						break;
				}

				if (e.dataField == "spk_bayar_id" && e.id !=null) {
					id = e.id;
					$.ajax({
						method: "POST",
						url: "{{env('API_WEB', false).'api/kasir/showroom/last'}}/",
						success: function(result) {
							$("#"+id).val(result.result);
						},
						error: function(e) {
							console.log("ID Loading Error");
						},
						timeout: 10000
					});
				}

				if (e.dataField == "spk_bayar_spk" && e.id !=null) {
					var standardHandler = e.editorOptions.onValueChanged;
					e.editorOptions.onValueChanged = function(e) {
						$.ajax({
							method: "GET",
							url: "{{env('API_WEB', false).'api/penjualan/spk/get'}}/" + e.value,

							success: function(result) {
								$("#"+spk_pel_id).next().remove();
								$("#"+spk_kendaraan).next().remove();
								
								$("#"+spk_pel_id).val(result[0].spk_pel_nama);
								$("#"+spk_kendaraan).val(result[0].variant_nama);
								$("#"+spk_harga).val(result[0].spk_harga);
								$("#"+jumlah).val(result[0].jumlah);
								$("#"+piutang).val(result[0].piutang);
							},

							error: function(e) {
								console.log("ID Loading Error");
							},

							timeout: 10000
						});

						standardHandler(e);
					}
				}
			}
		});
	</script>

@endsection