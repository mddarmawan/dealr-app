@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="panel-header-encar">
		<h6 class="left">
			<small style="margin-bottom: 40px">Master Data</small>
			<br>
		</h6>
		 <h5>|_BASE_|</h5>
	</div>
	<div class="nav-aksi">
		<button onclick="javascript:add();" class="btn-tambah"><i class="icon icon-left s7-plus"> </i> Tambah</button>
	</div>
	<div class="content_encar">
		<div class="main-content ">
			<div class="col-sm-12">
				<div class="panel panel-default panel-table">
					<div class="panel-body">
						<div class="nav-left">
							<ul class="tab" id="|_BASE_|Tipe">
								<li class="active"><a data-id="" >Semua</a></li>
							</ul>
						</div>
						<div id="data|_BASE_|">

						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>

	<div id="add" class="avgrund-popup" >
		<form id="form" action="/|_BASE_|" method="post">
			{{ csrf_field() }}
			<input id="form_method" type="hidden" name="_method" value="put">
			<input id="|_BASE_|_id" type="hidden" name="|_BASE_|_id">
			<div class="header-modal">
				<a href="#" class="close-modal" onclick="javascript:closeDialog();"><span class="s7-close"></span></a>
				<div class="title-modal">
					Tambah |_BASE_|
					<br>
				</div>
			</div>
			<div class="content-modal">
				<div class="bingkai">
					<div class="label-bingkai">Data |_BASE_|</div>
					<div class="row" style="padding: 10px;">
						<div class="col-md-6" style="display: inline-block;">
							<table class="table-form">
								<tr>
									<td width="140px">INPUT</td>
									<td>:</td>
									<td><input id="|_BASE_|_INPUT" type="text" placeholder="" name="|_BASE_|_INPUT" class="form-control form-control-xs"></td>
								</tr>
							</table>
						</div>
						<div class="col-md-6" style="display: inline-block;">
							<table class="table-form">
								<tr>
									<td width="140px">INPUT</td>
									<td>:</td>
									<td><input id="|_BASE_|_INPUT" type="text" placeholder="" name="|_BASE_|_INPUT" class="form-control form-control-xs"></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-modal">
				<div style="float: right; margin-right: 30px" >
					<button type="submit" name="submit" class="simpan">Simpan</button>	
				</div>
			</div>
		</form>
	</div>

@endsection

@section('scripts')

	<script type="text/javascript">
		App.init();
		var status = "";
		var url = "";

		/*
		 * --------------------------------------------------------------------------
		 * Tab Aksi Data
		 * --------------------------------------------------------------------------
		 */
		$("#|_BASE_|Tipe").append("<li><a data-id='0'>Non Aktif</a></li>");
		$("#|_BASE_|Tipe").append("<li><a data-id='trash'>Trash</a></li>");
		$(".tab li a").click(function(e){
			e.preventDefault();

			status = $(this).data("id");
			
			loadData();
			$(".tab li").removeClass("active");
			$(this).parent().addClass("active");
		});

		/*
		 * --------------------------------------------------------------------------
		 * Aksi Filter Data
		 * --------------------------------------------------------------------------
		 */
		$("#filter").click(function(e){
			e.preventDefault();
			loadData();
		});

		/*
		 * --------------------------------------------------------------------------
		 * Aksi Reset Data
		 * --------------------------------------------------------------------------
		 */
		$("#reset").click(function(e){
			e.preventDefault();
			$(".tab li").removeClass("active");
			$(".tab li a[data-id='']").parent().addClass("active");
			loadData();
		});

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Membersihkan Form
		 * --------------------------------------------------------------------------
		 */
		function clearForm() {	
			$("#|_BASE_|_INPUT").val('');
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Close Modal
		 * --------------------------------------------------------------------------
		 */
		function closeDialog() {
			Avgrund.hide( "#add" );
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Tambah Modal
		 * --------------------------------------------------------------------------
		 */
		function add() {
			clearForm();

			$("#form_method").prop("disabled", "disabled");
			$("#|_BASE_|_id").prop("disabled", "disabled");
			$("#form").attr("action", "/|_BASE_|/");

			Avgrund.show( "#add" );
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Modal Edit
		 * --------------------------------------------------------------------------
		 */
		function edit(id) {
			clearForm();

			$.ajax({
				type: "GET",
				url: "{{url('api/|_BASE_|')}}/" + id
			}).done(function(response){
				var data = response[0];

				$("#form").attr("action", "/|_BASE_|/" + id);
				$("#|_BASE_|_INPUT").val(data.|_BASE_|_INPUT);
			});
			

			$("#form_method").removeAttr("disabled");
			$("#|_BASE_|_id").removeAttr("disabled");
			
			Avgrund.show( "#add" );
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Utama Load Data
		 * --------------------------------------------------------------------------
		 */
		function loadData() {
			var db = {
				loadData: function(filter) {
					if (status != "") {
						if (status == 0) {
							url = "{{url('api/|_BASE_|/inactive')}}";
						} else if (status == "trash") {
							url = "{{url('api/|_BASE_|/deleted')}}";
						} else {
							url = "";
						}
					} else {
						url = "{{url('api/|_BASE_|')}}";
					}

					return $.ajax({
						type: "GET",
						url: url
					});
				},
			};
		 
			$("#data|_BASE_|").jsGrid({
				width: "100%",
				height: "410px",
				sorting: true,
				paging: true,
				filtering: true,
				autoload: true,
				
				deleteConfirm: "Anda yakin akan menghapus data ini?",
					 
				controller: db,
					 
				fields: [
					{ name: "|_BASE_|_id", title:"ID |_BASE_|", type: "text", width: 100 },
					{ name: "edit", title:"Action", type: "text", width: 80, validate: "required", align:"center" },
				]

			});
		}

		load|_BASE_|Tipe();
		loadData();
	</script>

@endsection