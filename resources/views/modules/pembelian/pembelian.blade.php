@extends('layouts.app')

@section('content')

	<div id="x-import"></div>
	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
				<h3 class="x-title"><span id="x-title">Monitoring Pembelian</span><br/><small>Pembelian \ Monitoring Pembelian</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

@endsection

@section('scripts')

	<script src="http://malsup.github.com/jquery.form.js"></script>
	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/pembelian/PembelianController.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		var url_api = "{{env('API_WEB', false).'api/pembelian/pembelian'}}/";
		var title = $("#x-title").html();
		var type = {!!$data['type']!!};
		var variant = {!!$data['variant']!!};
		var warna = {!!$data['warna']!!};
		var data = getData(url_api, "kend_beli_dh");

		$("#x-data").dxDataGrid({
			dataSource				: data,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},
			editing : {
				mode: "popup",
				allowUpdating: true,
				allowDeleting: true,
				popup: {
					title: "Editing",
					showTitle: true,
					width: 700,
					height: 300,
					position: {
						my: "center",
						at: "center",
						of: window
					}
				}
			},

			columns: [
				{dataField: "kend_beli_tgl", caption: "TANGGAL", width:"100px",dataType: "date", format:"dd/MM/yyyy", validationRules: [{ type: "required" }]},
				{dataField: "kend_beli_dh", caption: "NO. DH", validationRules: [{ type: "required" }]},
				{
					dataField: "type_id", 
					caption: "TYPE", 
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: type, 
						displayExpr: "type_nama", 
						valueExpr: "type_id"
					}
				},
				{
					dataField: "variant_id", 
					caption: "VARIAN", 
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: variant, 
						displayExpr: "variant_nama", 
						valueExpr: "variant_id"
					}
				},
				{
					dataField: "warna_id", 
					caption: "WARNA", 
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: warna, 
						displayExpr: "warna_nama", 
						valueExpr: "warna_id"
					}
				},
				{dataField: "kend_beli_rangka", caption: "NO. RANGKA", width:"200px", validationRules: [{ type: "required" }]},
				{dataField: "kend_beli_mesin", caption: "NO. MESIN", width:"200px", validationRules: [{ type: "required" }]},
				{dataField: "kend_beli_tgltempo", caption: "TEMPO", width:"150px",dataType: "date", format:"dd/MM/yyyy", validationRules: [{ type: "required" }]},
				{dataField: "kend_beli_tahun", caption: "TAHUN", width:"100px", validationRules: [{ type: "required" }]},
				{dataField: "kend_beli_harga", caption: "HARGA (RP)", width:"150px", format: "currency", validationRules: [{ type: "required" }]},
				{dataField: "kend_beli_diskon", caption: "DISKON (RP)", width:"150px", format: "currency", validationRules: [{ type: "required" }]},
				{dataField: "kend_beli_pbm", caption: "PBM (RP)", width:"150px", format: "currency", validationRules: [{ type: "required" }]},
				{dataField: "kend_beli_interest", caption: "INTEREST (RP)", width:"150px", format: "currency", validationRules: [{ type: "required" }]},
				{dataField: "kend_onhand_rrn", caption: "RRN", validationRules: [{ type: "required" }]}
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
			},

			onCellPrepared: function(e) {
				if (e.rowType === "data" && e.column.command === "edit") {
					var isEditing = e.row.isEditing,
						$links = e.cellElement.find(".dx-link");

					$links.text("");

					if (isEditing) {
						$links.filter(".dx-link-save").addClass("dx-icon-save");
						$links.filter(".dx-link-cancel").addClass("dx-icon-revert");
					} else {
						$links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
						$links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
					}
				}
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				dataGrid = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal']").val()=="" ? "ALL":$("input[name='x-filter-date-awal']").val();
								endDate = $("input[name='x-filter-date-akhir']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir']").val();
								$filtered = getData(url_api+startDate+"/"+endDate, "spk_po_spk");
								dataGrid.option("dataSource", $filtered);
								dataGrid.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid.repaint();
								dataGrid.option("dataSource", data);
								dataGrid.refresh();
							}
						}
					},
					{
						location: "before",
						widget: "dxButton",
						options: {
							icon: "upload",
							hint: "Import Files",
							onClick: function() {
								showImport();
							}
						}
					}
				);
			}
		});
	</script>

@endsection