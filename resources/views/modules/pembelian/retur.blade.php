@extends('layouts.app')

@section('content')

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Retur Kendaraan</span><br/><small>Pembelian \ Monitoring Retur Kendaraan</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

@endsection

@section('scripts')

	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		var url_api = "{{env('API_WEB', false).'api/pembelian/retur'}}/";
		var title = $("#x-title").html();
		var type = {!!$data['type']!!};
		var variant = {!!$data['variant']!!};
		var warna = {!!$data['warna']!!};
		var stok = {!!$data['stok']!!};
		var data = getData(url_api, "kend_retur_dh");

		$("#x-data").dxDataGrid({
			dataSource				: data,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},
			editing: {
				mode: "popup",
				allowDeleting: true,
				popup: {
					closeOnOutsideClick: true,
					title:"Tambah "+title,
					showTitle:true,
					width: 700,
					height: 330,
					shadingColor: "rgba(0,0,0,0.18)",
					position: {
						my: "center",
						at: "center",
						of: window
					},
				}
			},

			columns: [
				{
					dataField: "kend_beli_dh", 
					caption: "NO. DH", 
					lookup: {
						dataSource: stok, 
						displayExpr: "kend_beli_dh", 
						valueExpr: "kend_beli_dh"
					}
				},
				// {dataField: "spk_id", allowEditing: false, caption: "NO. REF", width:"200px"},
				{dataField: "kend_beli_faktur", allowEditing: false, caption: "NO. INVOICE"},
				{dataField: "kend_beli_tgl", allowEditing: false, caption: "TANGGAL PEMBELIAN", width:"",dataType: "date", format:"dd/MM/yyyy"},
				{
					dataField: "type_id", 
					caption: "TYPE", 
					allowEditing: false, 
					lookup: {
						dataSource: type, 
						displayExpr: "type_nama", 
						valueExpr: "type_id"
					}
				},
				{
					dataField: "variant_id", 
					caption: "NAMA VARIASI", 
					allowEditing: false, 
					lookup: {
						dataSource: variant, 
						displayExpr: "variant_nama", 
						valueExpr: "variant_id"
					}
				},
				{
					dataField: "kend_beli_warna", 
					caption: "WARNA",
					allowEditing: false, 
					lookup: {
						dataSource: warna, 
						displayExpr: "warna_nama", 
						valueExpr: "warna_id"
					}
				},
				{dataField: "variant_serial", allowEditing: false, caption: "SERIAL"},
				{dataField: "kend_beli_rangka", allowEditing: false, caption: "NO. RANGKA"},
				{dataField: "kend_beli_mesin", allowEditing: false, caption: "NO. MESIN"},
				{dataField: "kend_retur_tgl", caption: "TANGGAL RETUR", width:"",dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "kend_onhand_rrn", allowEditing: false, caption: "RRN"},
				{dataField: "kend_retur_ket", caption: "KET. RETUR"},
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
			},

			onCellPrepared: function(e) {
				if (e.rowType === "data" && e.column.command === "edit") {
					var isEditing = e.row.isEditing,
						$links = e.cellElement.find(".dx-link");

					$links.text("");

					if (isEditing) {
						$links.filter(".dx-link-save").addClass("dx-icon-save");
						$links.filter(".dx-link-cancel").addClass("dx-icon-revert");
					} else {
						$links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
						$links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
					}
				}
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				dataGrid = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "before",
						widget: "dxButton",
						options: {
							icon: "add",
							hint: "Add",
							onClick: function() {
								dataGrid.addRow();
								$(".dx-popup-normal .dx-toolbar-before .dx-toolbar-item-content>div").html("Tambah "+title);
							}
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal']").val()=="" ? "ALL":$("input[name='x-filter-date-awal']").val();
								endDate = $("input[name='x-filter-date-akhir']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir']").val();
								$filtered = getData(url_api+"/spk_po_waktu/"+startDate+"/"+endDate, "spk_po_spk");
								dataGrid.option("dataSource", $filtered);
								dataGrid.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid.repaint();
								dataGrid.option("dataSource", data);
								dataGrid.refresh();
							}
						}
					}
				);
			},

			onEditorPreparing: function(e) {
				switch (e.dataField) {
					// case "spk_id":
					// 	spk_id = e.id;
					// 	break;

					case "kend_beli_faktur":
						kend_beli_faktur = e.id;
						break;
						
					case "kend_beli_tgl":
						kend_beli_tgl = e.id;
						break;
						
					case "kend_onhand_rrn":
						kend_onhand_rrn = e.id;
						break;
						
					case "type_id":
						type_id = e.id;
						break;
						
					case "variant_id":
						variant_id = e.id;
						break;
						
					case "kend_beli_warna":
						kend_beli_warna = e.id;
						break;
						
					case "variant_serial":
						variant_serial = e.id;
						break;
						
					case "kend_beli_rangka":
						kend_beli_rangka = e.id;
						break;
						
					case "kend_beli_mesin":
						kend_beli_mesin = e.id;
						break;

					default:
						break;
				}

				if (e.dataField == "kend_beli_dh" && e.id !=null) {
					var standardHandler = e.editorOptions.onValueChanged;
					e.editorOptions.onValueChanged = function(e) {

						$.ajax({
							method: "POST",
							url: "{{env('API_WEB', false).'api/persediaan/stok/get'}}/" + e.value,

							success: function(result) {
								$("#"+kend_beli_warna).next().remove();
								$("#"+type_id).next().remove();
								$("#"+variant_id).next().remove();

								// $("#"+spk_id).val(result[0].spk_id);
								$("#"+kend_beli_faktur).val(result[0].kend_beli_faktur);
								$("#"+kend_beli_tgl).val(result[0].kend_beli_tgl);
								$("#"+kend_onhand_rrn).val(result[0].kend_onhand_rrn);
								$("#"+variant_serial).val(result[0].variant_serial);
								$("#"+kend_beli_warna).val(result[0].warna_nama);
								$("#"+kend_beli_mesin).val(result[0].kend_beli_mesin);
								$("#"+type_id).val(result[0].type_nama);
								$("#"+variant_id).val(result[0].variant_nama);
								$("#"+kend_beli_rangka).val(result[0].kend_beli_rangka);
							},

							error: function(e) {
								console.log("ID Loading Error");
							},

							timeout: 10000
						});

						standardHandler(e);
					}
				}
			}
		});
	</script>

@endsection