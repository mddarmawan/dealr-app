<script type="text/javascript">
	$(document).ready(function(){
		var db_type = {
			loadData: function(filter) {
				return $.ajax({
					type: "GET",
					url: "{{url('api/kendaraan/type')}}",
					data: filter
				});
			},
		};
		
		$("#dataType").jsGrid({
			width: "100%",

			sorting: true,
			paging: true,
			filtering: true,
			autoload: true,
	 
			deleteConfirm: "Anda yakin akan menghapus data ini?",
	 
			controller: db_type,
	 
			fields: [
				{ name: "type_nama", title:"Type", type: "text", width: 120, validate: "required" },
				{ name: "type_poin", title:"Point", type: "number", width: 80, validate: "required" },
			]
		});
	});
</script>