@extends('layouts.app')

@section('content')

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Pindah Stok</span><br/><small>Persediaan \ Pindah Stok</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

@endsection

@section('scripts')

	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		var url_api = "{{env('API_WEB', false).'api/persediaan/pindah'}}/";
		var title = $("#x-title").html();
		var type = {!!$data['type']!!};
		var variant = {!!$data['variant']!!};
		var warna = {!!$data['warna']!!};
		var stok = {!!$data['stok']!!};
		var gudang = {!!$data['gudang']!!};
		var data = getData(url_api, "kend_gudang_beli");

		$("#x-data").dxDataGrid({
			dataSource				: data,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true},
			editing: {
				mode: "popup",
				allowDeleting: true,
				popup: {
					closeOnOutsideClick: true,
					title:"Tambah "+title,
					showTitle:true,
					width: 700,
					height: 330,
					position: {
						my: "center",
						at: "center",
						of: window
					},
				}
			},

			columns: [
				{
					dataField: "kend_beli_dh", 
					caption: "NO. DH", 
					lookup: {
						dataSource: stok, 
						displayExpr: "kend_beli_dh", 
						valueExpr: "kend_beli_dh"
					}
				},
				{dataField: "kend_beli_tgl", allowEditing: false, caption: "TANGGAL PEMBELIAN", width:"",dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "kend_beli_tahun", allowEditing: false, caption: "TAHUN UNIT"},
				{
					dataField: "type_id", 
					caption: "TYPE", 
					allowEditing: false, 
					lookup: {
						dataSource: type, 
						displayExpr: "type_nama", 
						valueExpr: "type_id"
					}
				},
				{dataField: "variant_serial", allowEditing: false, caption: "ID"},
				{
					dataField: "variant_id", 
					caption: "NAMA VARIASI", 
					allowEditing: false, 
					lookup: {
						dataSource: variant, 
						displayExpr: "variant_nama", 
						valueExpr: "variant_id"
					}
				},
				{
					dataField: "warna_id", 
					caption: "WARNA", 
					allowEditing: false, 
					lookup: {
						dataSource: warna, 
						displayExpr: "warna_nama", 
						valueExpr: "warna_id"
					}
				},
				{
					dataField: "kend_gudang_lokasi", 
					caption: "DARI", 
					lookup: {
						dataSource: gudang, 
						displayExpr: "gudang_nama", 
						valueExpr: "gudang_id"
					}
				},
				{dataField: "kend_beli_mesin", allowEditing: false, caption: "NO. MESIN"},
				{
					dataField: "kend_gudang_tujuan", 
					caption: "KE", 
					lookup: {
						dataSource: gudang, 
						displayExpr: "gudang_nama", 
						valueExpr: "gudang_id"
					}
				},
				{dataField: "kend_beli_rangka", allowEditing: false, caption: "NO. RANGKA"},
				{dataField: "kend_gudang_keluar", allowEditing: true, caption: "TGL. KELUAR", width:"",dataType: "date", format:"dd/MM/yyyy"},
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
			},

			onCellPrepared: function(e) {
				if (e.rowType === "data" && e.column.command === "edit") {
					var isEditing = e.row.isEditing,
						$links = e.cellElement.find(".dx-link");

					$links.text("");

					if (isEditing) {
						$links.filter(".dx-link-save").addClass("dx-icon-save");
						$links.filter(".dx-link-cancel").addClass("dx-icon-revert");
					} else {
						$links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
						$links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
					}
				}
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				var dataGrid = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "before",
						widget: "dxButton",
						options: {
							icon: "add",
							hint: "Add",
							onClick: function() {
								dataGrid.addRow();
								$(".dx-popup-normal .dx-toolbar-before .dx-toolbar-item-content>div").html("Tambah "+title);
							}
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal']").val()=="" ? "ALL":$("input[name='x-filter-date-awal']").val();
								endDate = $("input[name='x-filter-date-akhir']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir']").val();
								$filtered = getData(url_api+"/spk_po_waktu/"+startDate+"/"+endDate, "spk_po_spk");
								dataGrid.option("dataSource", $filtered);
								dataGrid.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid.repaint();
								dataGrid.option("dataSource", data);
								dataGrid.refresh();
							}
						}
					}
				);
			},

			onEditorPreparing: function(e) {
				switch (e.dataField) {
					case "variant_serial":
						variant_serial = e.id;
						break;

					case "kend_beli_tahun":
						kend_beli_tahun = e.id;
						break;
						
					case "warna_id":
						warna_id = e.id;
						break;
						
					case "kend_beli_mesin":
						kend_beli_mesin = e.id;
						break;
						
					case "kend_beli_tgl":
						kend_beli_tgl = e.id;
						break;
						
					case "type_id":
						type_id = e.id;
						break;
						
					case "variant_id":
						variant_id = e.id;
						break;
						
					case "kend_beli_rangka":
						kend_beli_rangka = e.id;
						
					default:
						break;
				}

				if (e.dataField == "kend_beli_dh" && e.id !=null) {
					var standardHandler = e.editorOptions.onValueChanged;
					e.editorOptions.onValueChanged = function(e) {

						$.ajax({
							method: "POST",
							url: "{{env('API_WEB', false).'api/persediaan/stok/get'}}/" + e.value,

							success: function(result) {
								$("#"+warna_id).next().remove();
								$("#"+type_id).next().remove();
								$("#"+variant_id).next().remove();

								$("#"+variant_serial).val(result[0].variant_serial);
								$("#"+kend_beli_tahun).val(result[0].kend_beli_tahun);
								$("#"+warna_id).val(result[0].warna_nama);
								$("#"+kend_beli_mesin).val(result[0].kend_beli_mesin);
								$("#"+kend_beli_tgl).val(date_format(result[0].kend_beli_tgl));
								$("#"+type_id).val(result[0].type_nama);
								$("#"+variant_id).val(result[0].variant_nama);
								$("#"+kend_beli_rangka).val(result[0].kend_beli_rangka);
							},

							error: function(e) {
								console.log("ID Loading Error");
							},

							timeout: 10000
						});

						standardHandler(e);
					}
				}
			}
		});
	</script>

@endsection