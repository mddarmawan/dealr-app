@extends('layouts.app')

@section('content')

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Stok Kendaraan</span><br/><small>Persediaan \ Stok Kendaraan</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

@endsection

@section('scripts')

	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		var url_api = "{{env('API_WEB', false).'api/persediaan/stok'}}/";
		var title = $("#x-title").html();
		var type = {!!$data['type']!!};
		var variant = {!!$data['variant']!!};
		var warna = {!!$data['warna']!!};
		var gudang = {!!$data['gudang']!!};
		var ekspedisi = {!!$data['ekspedisi']!!};
		var data = getData(url_api, "kend_beli_dh");

		$("#x-data").dxDataGrid({
			dataSource				: data,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},
			editing: {
				mode: "popup",
				allowUpdating: true,
				popup: {
					closeOnOutsideClick: true,
					title:"Tambah "+title,
					showTitle:true,
					width: 700,
					height: 400,
					position: {
						my: "center",
						at: "center",
						of: window
					},
				}
			},

			columns: [
				{
					dataField: "kend_beli_status", 
					caption: "STATUS", 
					allowEditing: false,
					width:"100px", 
					alignment:"center", 
					lookup: {
						dataSource: ["STOK","MATCHED","RETUR","GI","IN-TRANSIT"],
						displayExpr: function(t) { 
							switch (t) {
								case "STOK":
									return "STOK";
								break;

								case "MATCHED":
									return "MATCHED";
								break;

								case "RETUR":
									return "RETUR";
								break;

								case "GI":
									return "GI";
								break;

								default:
									return "IN-TRANSIT";
								break;
							}
						}
					},
					cellTemplate: function (container, options) {
						switch (options.value) {
							case "STOK":
								$("<div>")
									.append($("<span>", { "class": "dx-active" }).html("STOK"))
									.appendTo(container);
							break;

							case "MATCHED":
								$("<div>")
									.append($("<span>", { "class": "dx-primary" }).html("MATCHED"))
									.appendTo(container);
							break;

							case "RETUR":
								$("<div>")
									.append($("<span>", { "class": "dx-warning" }).html("RETUR"))
									.appendTo(container);
							break;

							case "GI":
								$("<div>")
									.append($("<span>", { "class": "dx-info" }).html("GI"))
									.appendTo(container);
							break;

							default:
								$("<div>")
									.append($("<span>", { "class": "dx-default" }).html("IN-TRANSIT"))
									.appendTo(container);
							break;
						}
					}
				},
				{dataField: "kend_beli_dh", allowEditing: false, caption: "NO. DH"},
				{dataField: "kend_beli_tgl", allowEditing: false, caption: "TGL. MASUK", width:"",dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "kend_beli_tahun", allowEditing: false, caption: "TAHUN UNIT"},
				{
					dataField: "type_id", 
					caption: "TYPE", 
					allowEditing: false, 
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: type, 
						displayExpr: "type_nama", 
						valueExpr: "type_id"
					}
				},
				{
					dataField: "variant_id", 
					caption: "NAMA VARIASI", 
					allowEditing: false, 
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: variant, 
						displayExpr: "variant_nama", 
						valueExpr: "variant_id"
					}
				},
				{
					dataField: "warna_id", 
					caption: "WARNA", 
					allowEditing: false, 
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: warna, 
						displayExpr: "warna_nama", 
						valueExpr: "warna_id"
					}
				},
				{dataField: "kend_beli_rangka", allowEditing: false, caption: "NO. RANGKA"},
				{dataField: "kend_beli_mesin", allowEditing: false, caption: "NO. MESIN"},
				{dataField: "variant_serial", allowEditing: false, caption: "ID"},
				{
					dataField: "kend_beli_ekspedisi", 
					caption: "EKSPEDISI",
					lookup: {
						dataSource: ekspedisi, 
						displayExpr: "kontak_nama", 
						valueExpr: "kontak_id"
					}
				},
				{
					dataField: "kend_beli_lokasi", 
					caption: "LOKASI", 
					lookup: {
						dataSource: gudang, 
						displayExpr: "gudang_nama", 
						valueExpr: "gudang_id"
					}
				},
				{dataField: "kend_beli_faktur", allowEditing: false, caption: "NO. INVOCE"},
				{dataField: "kend_beli_tgl", allowEditing: false, caption: "TGL. INVOCE", width:"",dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "kend_onhand_rrn", allowEditing: false, caption: "RRN"},
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
			},

			onCellPrepared: function(e) {
				if (e.rowType === "data" && e.column.command === "edit") {
					var isEditing = e.row.isEditing,
						$links = e.cellElement.find(".dx-link");

					$links.text("");

					if (isEditing) {
						$links.filter(".dx-link-save").addClass("dx-icon-save");
						$links.filter(".dx-link-cancel").addClass("dx-icon-revert");
					} else {
						$links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
						$links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
					}
				}
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				dataGrid = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal']").val()=="" ? "ALL":$("input[name='x-filter-date-awal']").val();
								endDate = $("input[name='x-filter-date-akhir']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir']").val();
								$filtered = getData(url_api+"/spk_po_waktu/"+startDate+"/"+endDate, "spk_po_spk");
								dataGrid.option("dataSource", $filtered);
								dataGrid.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid.repaint();
								dataGrid.option("dataSource", data);
								dataGrid.refresh();
							}
						}
					}
				);
			}
		});
	</script>

@endsection