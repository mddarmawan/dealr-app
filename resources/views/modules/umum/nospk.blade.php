@extends('layouts.app')

@section('content')

	<div id="x-import"></div>
	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
				<h3 class="x-title"><span id="x-title">Pembagian Nomer SPK</span><br/><small>Umum \ No. SPK</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

@endsection

@section('scripts')

	<script src="http://malsup.github.com/jquery.form.js"></script>
	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/pembelian/RRNController.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		var url_api = "{{env('API_WEB', false).'api/umum/nospk'}}/";
		var title = $("#x-title").html();
		var sales = {!!$data['sales']!!};
		var spk = {!!$data['spk']!!};
		var team = {!!$data['team']!!};
		var data = getData(url_api, "sales_spk_id");

		$("#x-data").dxDataGrid({
			dataSource				: data,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},
			editing : {
				mode: "popup",
				allowDeleting: true,
				popup: {
					title: "Editing",
					showTitle: true,
					width: 300,
					height: 240,
					position: {
						my: "center",
						at: "center",
						of: window
					}
				}
			},

			columns: [
				{
					dataField: "sales_spk_sales", 
					caption: "SALES",
					validationRules: [{ type: "required" }],
					formItem: {
						colSpan: 2,
						label: {
							text: "SALES", 
							location: "top"
						}
					},
					lookup: {
						dataSource: sales, 
						displayExpr: "karyawan_nama", 
						valueExpr: "sales_uid"
					}
				},
				{
					dataField: "sales_spk_id_0", 
					visible: false,
					validationRules: [{ type: "required" }],
					formItem: {
						colSpan: 1,
						label: {
							text: "SPK 1", 
							location: "top",
						}
					}
				},
				{
					dataField: "sales_spk_id_1", 
					visible: false,
					formItem: {
						colSpan: 1,
						label: {
							text: "SPK 2", 
							location: "top",
						}
					}
				},
				{
					dataField: "sales_spk_id", 
					caption: "SPK",
					formItem: {
						visible: false
					}
				},
				{
					dataField: "sales_team", 
					caption: "TEAM", 
					allowEditing: false,
					formItem: {
						colSpan: 2,
						visible: false,
						label: {
							text: "TEAM", 
							location: "top"
						}
					},
					lookup: {
						dataSource: team, 
						displayExpr: "team_nama", 
						valueExpr: "team_id"
					}
				}
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
			},

			onCellPrepared: function(e) {
				if (e.rowType === "data" && e.column.command === "edit") {
					var isEditing = e.row.isEditing,
						$links = e.cellElement.find(".dx-link");

					$links.text("");

					if (isEditing) {
						$links.filter(".dx-link-save").addClass("dx-icon-save");
						$links.filter(".dx-link-cancel").addClass("dx-icon-revert");
					} else {
						$links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
						$links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
					}
				}
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				dataGrid = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "before",
						widget: "dxButton",
						options: {
							icon: "add",
							hint: "Add",
							onClick: function() {
								dataGrid.addRow();
								$(".dx-popup-normal .dx-toolbar-before .dx-toolbar-item-content>div").html("Tambah " + title);
							}
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal']").val()=="" ? "ALL":$("input[name='x-filter-date-awal']").val();
								endDate = $("input[name='x-filter-date-akhir']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir']").val();
								$filtered = getData(url_api+"/spk_po_waktu/"+startDate+"/"+endDate, "spk_po_spk");
								dataGrid.option("dataSource", $filtered);
								dataGrid.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid.repaint();
								dataGrid.option("dataSource", data);
								dataGrid.refresh();
							}
						}
					}
				);
			},

			onEditorPreparing: function(e) {
				switch (e.dataField) {
					case "sales_spk_id_0":
						sales_spk_id_0 = e.id;

						break;

					case "sales_spk_id_1":
						sales_spk_id_1 = e.id;

						break;
						
					default:
						break;
				}

				if (e.dataField == "sales_spk_sales" && e.id !=null) {
					var standardHandler = e.editorOptions.onValueChanged;
					e.editorOptions.onValueChanged = function(e) {
						var spk_count = 0;

						$.ajax({
							method: "POST",
							url: "{{env('API_WEB', false).'api/penjualan/spk/SALES'}}/" + e.value,

							success: function(result) {
								spk_count = result;

								if (spk_count == 0) {
									$("#"+sales_spk_id_0).removeAttr("disabled");
									$("#"+sales_spk_id_1).removeAttr("disabled");
								} else if (spk_count == 1) {
									$("#"+sales_spk_id_0).removeAttr("disabled");
									$("#"+sales_spk_id_1).attr("disabled", "disabled");
								} else {
									$("#"+sales_spk_id_0).attr("disabled", "disabled");
									$("#"+sales_spk_id_1).attr("disabled", "disabled");
								}
							},

							error: function(e) {
								console.log("ID Loading Error");
							},

							timeout: 10000
						});

						standardHandler(e);
					}
				}
			}
		});
	</script>

@endsection