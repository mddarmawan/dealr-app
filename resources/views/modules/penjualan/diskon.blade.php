@extends('layouts.app')

@section('content')
	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Diskon</span><br/><small>Penjualan \ Diskon</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

	<div id="x-detail">

	</div>
@endsection

@section('scripts')

	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/FirebaseController.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/penjualan/DiskonController.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
		var url_api = "{{env('API_WEB', false).'api/penjualan/diskon'}}/";
		var title = $("#x-title").html();
		var spk = {!!$data['spk']!!};
		var sales = {!!$data['sales']!!};
		var data = getData(url_api, "spk_id");

		$("#x-data").dxDataGrid({
			dataSource				: data,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},

			columns: [
				{dataField:"spk_id", width:"50px", caption:"", alignment:"center", allowSorting:false, allowFiltering:false, allowSearch:false,
					cellTemplate: function(container, options){
						$("<div>")
							.append($("<a>", { "class": "dx-link dx-link-edit dx-icon-search x-detail", "data-id":options.value, "data-index":options.rowIndex }))
							.appendTo(container);
					}
				},
				{dataField: "spk_tgl", caption: "TANGGAL", width:"100px", dataType: "date", format:"dd/MM/yyyy", validationRules: [{ type: "required" }]},
				{
					dataField: "spk_id", 
					caption: "NO SPK", 
					width:"100px", 
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: spk, 
						displayExpr: "spk_id", 
						valueExpr: "spk_id"
					}
				},
				{
					dataField: "spk_sales", 
					caption: "SALES", 
					lookup: {
						dataSource: sales, 
						displayExpr: "karyawan_nama", 
						valueExpr: "sales_uid"
					}
				},
				{dataField: "spk_diskon_cashback", caption: "CASHBACK (RP)", width:"200px", format: "currency", placeholder:"0", validationRules: [{ type: "required" }]},
				{dataField: "spk_diskon_komisi", caption: "KOMISI (RP)", width:"200px", validationRules: [{ type: "required" }]},
				{dataField: "spk_diskon_ket", caption: "KETERANGAN", width:"", validationRules: [{ type: "required" }]}
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
				items = this.getDataSource().items();
				$(".x-detail").unbind('click').click(function(event){
					index = $(this).data("index");
					id = $(this).data("id");
					showInfo(id, items[index]);
				});
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				dataGrid = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal']").val()=="" ? "ALL":$("input[name='x-filter-date-awal']").val();
								endDate = $("input[name='x-filter-date-akhir']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir']").val();
								$filtered = getData(url_api+"/"+startDate+"/"+endDate,"spk_id");
								dataGrid.option("dataSource", $filtered);
								dataGrid.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid.repaint();
								dataGrid.option("dataSource", data);
								dataGrid.refresh();
							}
						}
					}
				);
			}
		});
	</script>

@endsection