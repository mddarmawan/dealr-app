@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Matching Unit</span><br/><small>Penjualan \ Matching Unit</small></h3>
			</div>
		</div>
		<div class="x-body-panel" style="width: 99%;">
			<div class="row" style="height: calc(100%);">
				<div class="col-lg-6">
					<div id="x-data-spk" class="x-table"></div>
				</div>
				<div class="col-lg-6">
					<div id="x-data-stok" class="x-table"></div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')

	<script type="text/javascript">
		function getData(dataURL, id){		
			return new DevExpress.data.CustomStore({
				loadMode: "raw",
				key:id,

				load: function (loadOptions) {
					var deferred = $.Deferred();
					
					$.ajax({
						url: dataURL,
						success: function(result) {
							deferred.resolve(result);
						},
						error: function(e) {
							deferred.reject("Data Loading Error");
						},
						timeout: 10000
					});
			
					return deferred.promise();
				},

				update: function (key, values){
					var deferred = $.Deferred();
					$.ajax({
						url: dataURL + "matching/" + key,
						method: "POST",
						dataType: "json",
						data: {data:JSON.stringify(values)}
					}).done(function(){
						deferred.resolve(key);
					});
					return deferred.promise();
				}
			});
		}
	</script>

	<script type="text/javascript">
		var url_api_stok = "{{env('API_WEB', false).'api/penjualan/matching'}}/";
		var url_api_spk = "{{env('API_WEB', false).'api/penjualan/spk/MANUAL'}}/";
		var title = $("#x-title").html();
		var type = {!!$data['type']!!};
		var variant = {!!$data['variant']!!};
		var warna = {!!$data['warna']!!};
		var data_stok = getData(url_api_stok, "kend_beli_dh");
		var data_spk = getData(url_api_spk, "spk_id");

		$("#x-data-spk").dxDataGrid({
			dataSource				: data_spk,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},
			editing: {
				mode: "popup",
				allowUpdating: true,
				popup: {
					closeOnOutsideClick: true,
					title:"Tambah "+title,
					showTitle:true,
					width: 700,
					height: 300,
					position: {
						my: "center",
						at: "center",
						of: window
					},
				}
			},

			columns: [
				{
					dataField: "kend_beli_dh", 
					caption: "NO. DH",
					visible: false,
					lookup: {
						dataSource: data_stok, 
						displayExpr: "kend_beli_dh", 
						valueExpr: "kend_beli_dh"
					}
				},
				{dataField: "spk_tgl", allowEditing: false, caption: "TGL. SPK", width:"100px",dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "spk_id", allowEditing: false, caption: "NO. SPK"},
				{dataField: "spk_pel_nama", allowEditing: false, caption: "PELANGGAN"},
				// {
				// 	dataField: "type_id", 
				// 	caption: "TYPE", 
				// 	validationRules: [{ type: "required" }], 
				// 	lookup: {
				// 		dataSource: type, 
				// 		displayExpr: "type_nama", 
				// 		valueExpr: "type_id"
				// 	}
				// },
				{dataField: "karyawan_nama", allowEditing: false, caption: "SALES"},
				{dataField: "team_nama", allowEditing: false, caption: "TEAM"},
				{dataField: "spk_pel_kota", allowEditing: false, caption: "KOTA"},
				{
					dataField: "spk_pel_kategori", 
					caption: "KATEGORI", 
					allowEditing: false, 
					alignment:"center", 
					dataType:"boolean", 
					lookup: {
						dataSource: ["0", "1"],
						displayExpr: function (t) {
							return t == 0 ? "PRIBADI" : "PERUSAHAAN"
						}
					},
					cellTemplate: function (container, options) {
						if (options.value == 1) {
							$("<div>")
								.append($("<span>").html("PERUSAHAAN"))
								.appendTo(container);
						} else {
							$("<div>")
								.append($("<span>").html("PRIBADI"))
								.appendTo(container);
						}
					}
				},
				{
					dataField: "spk_kendaraan", 
					caption: "VARIAN", 
					allowEditing: false,
					visible: false,
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: variant, 
						displayExpr: "variant_nama", 
						valueExpr: "variant_id"
					}
				},
				{
					dataField: "spk_warna", 
					allowEditing: false,
					visible: false,
					caption: "WARNA", 
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: warna, 
						displayExpr: "warna_nama", 
						valueExpr: "warna_id"
					}
				},
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
			},

			onCellPrepared: function(e) {
				if (e.rowType === "data" && e.column.command === "edit") {
					var isEditing = e.row.isEditing,
						$links = e.cellElement.find(".dx-link");

					$links.text("");

					if (isEditing) {
						$links.filter(".dx-link-save").addClass("dx-icon-save");
						$links.filter(".dx-link-cancel").addClass("dx-icon-revert");
					} else {
						$links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
						$links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
					}
				}
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				dataGrid_spk = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal-spk",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir-spk",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal-spk']").val()=="" ? "ALL":$("input[name='x-filter-date-awal-spk']").val();
								endDate = $("input[name='x-filter-date-akhir-spk']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir-spk']").val();
								$filtered = getData(url_api_spk+startDate+"/"+endDate, "spk_id");
								dataGrid_spk.option("dataSource", $filtered);
								dataGrid_spk.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid_spk.repaint();
								dataGrid_spk.option("dataSource", data_spk);
								dataGrid_spk.refresh();
							}
						}
					}
				);
			}
		});

		$("#x-data-stok").dxDataGrid({
			dataSource				: data_stok,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},

			columns: [
				{dataField: "kend_beli_dh", caption: "NO. DH", validationRules: [{ type: "required" }]},
				{
					dataField: "variant_id", 
					caption: "VARIAN", 
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: variant, 
						displayExpr: "variant_nama", 
						valueExpr: "variant_id"
					}
				},
				{dataField: "kend_beli_mesin", caption: "NO. MESIN", width:"200px", validationRules: [{ type: "required" }]},
				{
					dataField: "warna_id", 
					caption: "WARNA", 
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: warna, 
						displayExpr: "warna_nama", 
						valueExpr: "warna_id"
					}
				},
				{dataField: "kend_beli_rangka", caption: "NO. RANGKA", width:"200px", validationRules: [{ type: "required" }]},
				// {
				// 	dataField: "type_id", 
				// 	caption: "TYPE", 
				// 	validationRules: [{ type: "required" }], 
				// 	lookup: {
				// 		dataSource: type, 
				// 		displayExpr: "type_nama", 
				// 		valueExpr: "type_id"
				// 	}
				// },
				// {dataField: "kend_beli_tgltempo", caption: "TEMPO", width:"150px",dataType: "date", format:"dd/MM/yyyy", validationRules: [{ type: "required" }]},
				// {dataField: "kend_beli_tahun", caption: "TAHUN", width:"100px", validationRules: [{ type: "required" }]},
				// {dataField: "kend_beli_harga", caption: "HARGA (RP)", width:"150px", format: "currency", validationRules: [{ type: "required" }]},
				// {dataField: "kend_beli_diskon", caption: "DISKON (RP)", width:"150px", format: "currency", validationRules: [{ type: "required" }]},
				// {dataField: "kend_beli_pbm", caption: "PBM (RP)", width:"150px", format: "currency", validationRules: [{ type: "required" }]},
				// {dataField: "kend_beli_interest", caption: "INTEREST (RP)", width:"150px", format: "currency", validationRules: [{ type: "required" }]},
				// {dataField: "kend_onhand_rrn", caption: "RRN", validationRules: [{ type: "required" }]}
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
			},

			onCellPrepared: function(e) {
				if (e.rowType === "data" && e.column.command === "edit") {
					var isEditing = e.row.isEditing,
						$links = e.cellElement.find(".dx-link");

					$links.text("");

					if (isEditing) {
						$links.filter(".dx-link-save").addClass("dx-icon-save");
						$links.filter(".dx-link-cancel").addClass("dx-icon-revert");
					} else {
						$links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
						$links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
					}
				}
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				dataGrid_stok = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal-stok",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir-stok",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal-stok']").val()=="" ? "ALL":$("input[name='x-filter-date-awal-stok']").val();
								endDate = $("input[name='x-filter-date-akhir-stok']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir-stok']").val();
								$filtered = getData(url_api_stok+startDate+"/"+endDate, "kend_beli_dh");
								dataGrid_stok.option("dataSource", $filtered);
								dataGrid_stok.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid_stok.repaint();
								dataGrid_stok.option("dataSource", data_stok);
								dataGrid_stok.refresh();
							}
						}
					}
				);
			}
		});
	</script>

@endsection