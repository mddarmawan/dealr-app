@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Tagihan Leasing</span><br/><small>Penjualan \ Tagihan Leasing</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>
	<iframe id="printf" name="printf"></iframe>

@endsection

@section('scripts')

	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/PrintController.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		var url_api = "{{env('API_WEB', false).'api/penjualan/leasing'}}/";
		var title = $("#x-title").html();
		var sales = {!!$data['sales']!!};
		var team = {!!$data['team']!!};
		var leasing = {!!$data['leasing']!!};
		var asuransi = {!!$data['asuransi']!!};
		var ajenis = {!!$data['ajenis']!!};
		var data = getData(url_api, "spk_po_spk");

		$("#x-data").dxDataGrid({
			dataSource				: data,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},
			editing : {
				mode: "popup",
				allowUpdating: true,
				popup: {
					title: "Tagihan Leasing",
					showTitle: true,
					width: 600,
					height: 550,
					position: {
						my: "center",
						at: "center",
						of: window
					}
				},
				form: {
					colCount: 2,
					items: [{
						itemType: "group",
						caption: "SPK",
						items: ["spk_po_spk", "spk_tgl", "spk_pel_nama"]
					}, {
						itemType: "group",
						caption: "Leasing",
						items: ["spk_po_leasing", "spk_po_asuransi", "spk_po_jenis_asuransi"]
					}, {
						colSpan: 2,
						itemType: "group",
						caption: "Tanggal",
						items: ["spk_gi_tgl", "spk_po_cetak", "spk_po_tagihan", "spk_po_lunas", "spk_po_refund", "spk_po_jumlah_refund"]
					}]
				}
			},

			columns: [
				{dataField:"spk_id", width:"50px", caption:"", alignment:"center", allowSorting:false, allowFiltering:false, allowSearch:false, fixed: true,
					cellTemplate: function(container, options){
						$("<div>")
							.append($("<a>", { "class": "dx-link dx-link-edit dx-icon-upload x-cetak", "data-id":options.value, "data-index":options.rowIndex }))
							.appendTo(container);
					}
				},
				{
					dataField: "spk_status", 
					caption: "STATUS", 
					fixed: true, 
					width:"100px", 
					dataType:"boolean", 
					alignment:"center", 
					lookup: {
						dataSource: ["0","1"],
						displayExpr: function(t){return t==0?"FAKTUR":"REFUND"}
					},
					cellTemplate: function (container, options) {
						if (options.value==1) {
							$("<div>")
								.append($("<span>", { "class": "dx-active" }).html("REFUND"))
								.appendTo(container);
						} else {
							$("<div>")
								.append($("<span>", { "class": "dx-inactive" }).html("FAKTUR"))
								.appendTo(container);
						}
					}
				},
				{dataField: "spk_tgl", fixed: true, allowEditing: false, caption: "TANGGAL", dataType: "date", format:"dd/MM/yyyy", validationRules: [{ type: "required" }]},
				{dataField: "spk_po_spk", fixed: true, allowEditing: false, caption: "NO. SPK", validationRules: [{ type: "required" }]},
				{dataField: "spk_pel_nama", allowEditing: false, caption: "NAMA PELANGGAN", validationRules: [{ type: "required" }]},
				{
					dataField: "karyawan_id", 
					caption: "SALES", 
					allowEditing: false,
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: sales, 
						displayExpr: "karyawan_nama", 
						valueExpr: "karyawan_id"
					}
				},
				{
					dataField: "team_id", 
					caption: "TEAM", 
					allowEditing: false,
					validationRules: [{ type: "required" }], 
					lookup: {
						dataSource: team, 
						displayExpr: "team_nama", 
						valueExpr: "team_id"
					}
				},
				{
					dataField: "spk_po_leasing", 
					caption: "LEASING", 
					lookup: {
						dataSource: leasing, 
						displayExpr: "kontak_nama", 
						valueExpr: "kontak_id"
					}
				},
				{
					dataField: "spk_po_asuransi", 
					caption: "ASURANSI", 
					lookup: {
						dataSource: asuransi, 
						displayExpr: "kontak_nama", 
						valueExpr: "kontak_id"
					}
				},
				{
					dataField: "spk_po_jenis_asuransi", 
					caption: "JENIS ASURANSI", 
					lookup: {
						dataSource: ajenis, 
						displayExpr: "ajenis_nama", 
						valueExpr: "ajenis_id"
					}
				},
				{dataField: "spk_po_angsuran", allowEditing: false, caption: "ANGSURAN", format: "currency"},
				{dataField: "spk_po_waktu", allowEditing: false, caption: "JANGKA WAKTU"},
				{dataField: "spk_po_dp", allowEditing: false, allowFiltering:false, caption: "DP", format: "currency"},
				{dataField: "spk_po_droping", allowEditing: false, allowFiltering:false, caption: "DROPING", format: "currency"},
				{dataField: "piutang", allowEditing: false, allowFiltering:false, caption: "PIUTANG", format: "currency"},
				// {dataField: "spk_po_waktu", caption: "TGL. FAKTUR", dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "spk_po_wgi", allowSorting:false, allowFiltering:false, allowSearch:false, allowEditing: false, caption: ""},
				{dataField: "spk_gi_tgl", caption: "TGL. GI", dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "spk_po_wcetak", allowSorting:false, allowFiltering:false, allowSearch:false, allowEditing: false, caption: "", width:"50px"},
				{dataField: "spk_po_cetak", caption: "TGL. CETAK", dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "spk_po_wtagihan", allowSorting:false, allowFiltering:false, allowSearch:false, allowEditing: false, caption: "", width:"50px"},
				{dataField: "spk_po_tagihan", caption: "TGL. TAGIHAN", dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "spk_po_wlunas", allowSorting:false, allowFiltering:false, allowSearch:false, allowEditing: false, caption: "", width:"50px"},
				{dataField: "spk_po_lunas", caption: "TGL. LUNAS", dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "spk_po_wrefund", allowSorting:false, allowFiltering:false, allowSearch:false, allowEditing: false, caption: "", width:"50px"},
				{dataField: "spk_po_refund", caption: "TGL. REFUND", dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "spk_po_jumlah_refund", caption: "JUMLAH REFUND", format: "currency"},
				{dataField: "spk_po_dokumen", caption: "DOKUMEN PO", allowSorting: false, allowFiltering: false, allowSearch: false, dataType: "boolean"},
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
				items = this.getDataSource().items();
				
				$(".x-cetak").unbind('click').click(function(event){
					index = $(this).data("index");
					id = $(this).data("id");
					printTagihanLeasing(id, items[index]);
					printPembayaran(id, items[index]);
				});
			},

			onCellPrepared: function(e) {
				if (e.rowType === "data" && e.column.command === "edit") {
					var isEditing = e.row.isEditing,
						$links = e.cellElement.find(".dx-link");

					$links.text("");

					if (isEditing) {
						$links.filter(".dx-link-save").addClass("dx-icon-save");
						$links.filter(".dx-link-cancel").addClass("dx-icon-revert");
					} else {
						$links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
						$links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
					}
				}
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				dataGrid = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal']").val()=="" ? "ALL":$("input[name='x-filter-date-awal']").val();
								endDate = $("input[name='x-filter-date-akhir']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir']").val();
								$filtered = getData(url_api+startDate+"/"+endDate, "spk_po_spk");
								dataGrid.option("dataSource", $filtered);
								dataGrid.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid.repaint();
								dataGrid.option("dataSource", data);
								dataGrid.refresh();
							}
						}
					}
				);
			}
		});
	</script>

@endsection