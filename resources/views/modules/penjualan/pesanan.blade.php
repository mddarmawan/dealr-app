@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Monitoring SPK</span><br/><small>Penjualan \ SPK</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

	<div id="x-detail">

	</div>
	
	<iframe id="printf" name="printf"></iframe>
@endsection

@section('scripts')

	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/PrintController.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/penjualan/PesananController.js') }}" type="text/javascript"></script>
	
	<script type="text/javascript">
		var url_api = "{{env('API_WEB', false).'api/penjualan/spk/INVALID'}}/";
		var title = $("#x-title").html();
		var sales = {!!$data['sales']!!};
		var variant = {!!$data['variant']!!};
		var vendor = {!!$data['vendor']!!};
		var warna = {!!$data['warna']!!};
		var data = getData(url_api, "spk_id");

		$("#x-data").dxDataGrid({
			dataSource				: data,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},

			columns: [
				{dataField:"spk_id", fixed:true, caption:"", alignment:"center", allowSorting:false, allowFiltering:false, allowSearch:false,
					cellTemplate: function(container, options){
						$("<div>")
								.append($("<a>", { "class": "dx-link dx-link-edit dx-icon-search x-detail", "data-id":options.value, "data-index":options.rowIndex }))
								.appendTo(container);
					}
				},
				{dataField: "spk_status_number",  fixed:true, caption: "STATUS", width:"100px", validationRules: [{ type: "required" }],dataType:"boolean",alignment:"center",
					lookup: {
						dataSource: [0,1,2,3],displayExpr: function (t) {
							switch (t) {
								case 3:
									return "DO";
								break;

								case 2:
									return "MATCHED";
								break;

								case 1:
									return "VALID";
								break;

								default:
									return "INVALID";
								break;
							}
						}	
					},
					cellTemplate: function (container, options) {
						if (options.value == 3) {
							$("<div>")
								.append($("<span>", { "class": "dx-active" }).html("DO"))
								.appendTo(container);
						} else if (options.value == 2) {
							$("<div>")
								.append($("<span>", { "class": "dx-active" }).html("MATCHED"))
								.appendTo(container);
						} else if (options.value == 1) {
							$("<div>")
								.append($("<span>", { "class": "dx-active" }).html("VALID"))
								.appendTo(container);
						} else {
							$("<div>")
								.append($("<span>", { "class": "dx-inactive" }).html("INVALID"))
								.appendTo(container);
						}
					}
				},
				{dataField: "spk_tgl",  fixed:true, caption: "TANGGAL", width:"100px",dataType: "date", format:"dd/MM/yyyy", validationRules: [{ type: "required" }]},
				{dataField: "spk_id",  fixed:true, caption: "NO SPK", width:"100px", validationRules: [{ type: "required" }]},
				{dataField: "spk_sales", caption: "SALES", lookup: {dataSource: sales, displayExpr: "karyawan_nama",valueExpr: "sales_uid"}},
				{dataField: "spk_pel_kategori", caption: "KATEGORI", alignment:"center", dataType:"boolean", lookup: {dataSource: ["0", "1"],displayExpr: function (t) {return t == 0 ? "PRIBADI" : "PERUSAHAAN"}},
				cellTemplate: function (container, options) {
						if (options.value == 1) {
							$("<div>")
								.append($("<span>").html("PERUSAHAAN"))
								.appendTo(container);
						} else {
							$("<div>")
								.append($("<span>").html("PRIBADI"))
								.appendTo(container);
						}
					}
				},
				{dataField: "spk_pel_nama", caption: "NAMA PELANGGAN", width:"150px", validationRules: [{ type: "required" }]},
				{dataField: "spk_kendaraan", caption: "VARIAN", width:220, validationRules: [{ type: "required" }],lookup: {dataSource: variant, displayExpr: "variant_nama", valueExpr: "variant_id" }},
				{dataField: "warna_id", caption: "WARNA", width:200, validationRules: [{ type: "required" }], lookup: {dataSource: warna, displayExpr: "warna_nama", valueExpr: "warna_id" }},
				{dataField: "spk_harga", caption: "HARGA (RP)", width:"150px", format: "currency"},
				{dataField: "jumlah", caption: "PEMBAYARAN (RP)", width:"150px", format: "currency"},
				{dataField: "piutang", caption: "PIUTANG (RP)", width:"100px", format: "currency"},
				{dataField: "spk_pembayaran",
					caption: "CARA BAYAR",
					validationRules: [{ type: "required" }],
					dataType:"boolean",
					alignment:"center",
					lookup: {
						dataSource: ["0", "1"],
						displayExpr: function (t) {
							return t == 0 ? "CASH" : "CREDIT"
						}
					},
					cellTemplate: function (container, options) {
						if (options.value == 1) {
							$("<div>")
								.append($("<span>", { "class": "dx-inactive" }).html("CASH"))
								.appendTo(container);
						} else {
							$("<div>")
								.append($("<span>", { "class": "dx-active" }).html("CREDIT"))
								.appendTo(container);
						}
					}
				},
				{dataField: "spk_match", caption: "MATCHING", dataType: "boolean", trueText:"ADA", falseText:"TIDAK ADA"},
				{dataField: "spk_po", caption: "PO", dataType: "boolean", trueText:"ADA", falseText:"TIDAK ADA"},
				{dataField: "spk_diskon", caption: "DISKON", dataType: "boolean", trueText:"ADA", falseText:"TIDAK ADA"},
				{dataField: "spk_pel_kota", caption: "KOTA", width:"", validationRules: [{ type: "required" }]},
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
				items = this.getDataSource().items();
				$(".x-detail").unbind('click').click(function(event){
					index = $(this).data("index");
					id = $(this).data("id");
					rowData = items[index];
					showInfo(id, items[index]);
				});
			},


			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location="before";
					} else if (item.name === "saveButton") {
						item.location="before";
					}
				});

				dataGrid = e.component;
				e.toolbarOptions.items.unshift(
							{
								location: "after",
								widget: "dxDateBox",
								options: {
									type:"date",
									name:"x-filter-date-awal",
									showClearButton:true,
									acceptCustomValue:false,
									placeholder: "Tanggal Awal",
									displayFormat:"dd/MM/yyyy",
									width:"150px",
								}
							},
							{
								location: "after",
								template: function(){
									return $("<span />").html("s.d");
								}
							},
							{
								location: "after",
								widget: "dxDateBox",
								options: {
									type:"date",
									name:"x-filter-date-akhir",
									showClearButton:true,
									acceptCustomValue:false,
									placeholder: "Tanggal Akhir",
									displayFormat:"dd/MM/yyyy",
									width:"150px",
								}
							},
							{
								location: "after",
								widget: "dxButton",
								options: {
									icon: "filter",
									hint: "Filter",
									onClick: function() {
										startDate = $("input[name='x-filter-date-awal']").val()=="" ? "ALL":$("input[name='x-filter-date-awal']").val();
										endDate = $("input[name='x-filter-date-akhir']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir']").val();
										$filtered = getData("{{env('API_WEB', false).'api/penjualan/spk/ADH'}}/"+startDate+"/"+endDate,"spk_id");
										dataGrid.option("dataSource", $filtered);
										dataGrid.refresh();
									}
								}
							},
							{
								location: "after",
								widget: "dxButton",
								options: {
									icon: "refresh",
									hint: "Refresh & Reset",
									onClick: function() {
										dataGrid.repaint();
										dataGrid.option("dataSource", data);
										dataGrid.refresh();
									}
								}
							}
						);
			}
		});
	</script>

@endsection
