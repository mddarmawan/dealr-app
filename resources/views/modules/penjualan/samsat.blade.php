@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Proses Samsat</span><br/><small>Penjualan \ Proses Samsat</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>
	<iframe id="printf" name="printf"></iframe>

@endsection

@section('scripts')

	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/PrintController.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		var url_api = "{{env('API_WEB', false).'api/penjualan/samsat'}}/";
		var title = $("#x-title").html();
		var variant = {!!$data['variant']!!};
		var data = getData(url_api, "spk_samsat_spk");

		$("#x-data").dxDataGrid({
			dataSource              : data,
			allowColumnResizing     : true,
			columnMinWidth          : 50,
			columnAutoWidth         : true,
			showRowLines            : true,
			rowAlternationEnabled   : true,
			export                  : {enabled: true, fileName: title},
			sorting                 : {mode: "multiple"},
			selection               : {mode: "single"},
			filterRow               : {visible: true},
			paging                  : {pageSize: 15},
			pager                   : {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel             : {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},
			editing: {
				mode: "popup",
				allowUpdating: true,
				popup: {
					title:"Editing "+title,
					showTitle:true,
					width: 650,
					height: 500,
					position: {
						my: "center",
						at: "center",
						of: window
					},
				}
			},

			columns: [
				{dataField:"spk_samsat_spk", formItem: {visible: false}, width:"50px", caption:"", alignment:"center", allowSorting:false, allowFiltering:false, allowSearch:false,
					cellTemplate: function(container, options){
						$("<div>")
							.append($("<a>", { "class": "dx-link dx-link-edit dx-icon-upload x-cetak", "data-id":options.value, "data-index":options.rowIndex }))
							.appendTo(container);
					}
				},
				{
					dataField: "spk_samsat_spk", 
					caption: "NO SPK", 
					allowEditing: false
				},
				{
					dataField: "spk_tgl", 
					allowEditing: false, 
					caption: "TANGGAL", 
					dataType: "date", 
					format:"dd/MM/yyyy"
				},
				{
					dataField: "spk_pel_nama", 
					caption: "NAMA PELANGGAN", 
					allowEditing: false
				},
				{
					dataField: "spk_kendaraan", 
					caption: "VARIANT", 
					allowEditing: false,
					width:220,
					lookup: {dataSource: variant, displayExpr: "variant_nama", valueExpr: "variant_id" }
				},
				{
					dataField: "spk_samsat_tglfaktur", 
					caption: "TGL. FAKTUR", 
					dataType: "date", 
					format:"dd/MM/yyyy",
				},
				{
					dataField: "spk_samsat_nopol", 
					caption: "NO POLISI", 
				},
				{
					dataField: "spk_samsat_tglstnk", 
					caption: "TGL. STNK", 
					dataType: "date", 
					format:"dd/MM/yyyy"
				},
				{
					dataField: "spk_samsat_plat", 
					caption: "PLAT", 
					visible: false,
					dataType: "boolean",
					value:false
				},
				{
					dataField: "spk_samsat_tglbpkb", 
					caption: "TGL. PENYERAHAN BERKAS", 
					dataType: "date", 
					format:"dd/MM/yyyy"
				},
				{
					dataField: "spk_samsat_stnk", 
					caption: "STNK", 
					visible: false,
					dataType: "boolean",
					value:false
				},
				{dataField: "spk_samsat_plat", formItem: {visible: false}, caption: "PLAT", allowSorting: false, allowFiltering: false, allowSearch: false, dataType: "boolean"},
				{dataField: "spk_samsat_stnk", formItem: {visible: false}, caption: "STNK", allowSorting: false, allowFiltering: false, allowSearch: false, dataType: "boolean"},
				{dataField: "spk_samsat_notis", formItem: {visible: false}, caption: "NOTIS", allowSorting: false, allowFiltering: false, allowSearch: false, dataType: "boolean"},
				{dataField: "spk_samsat_ujitipe", caption: "TGL. UJI TYPE", dataType: "date", format:"dd/MM/yyyy"},
				{dataField: "spk_samsat_notis", caption: "NOTIS", visible: false,dataType: "boolean",value:false},
				{
					dataField: "spk_samsat_nama", 
					caption: "SAMSAT", 
					formItem: {
						colSpan: 1,
						label: {
							text: "SAMSAT", 
							location: "top"
						}
					},
				},
				{
					dataField: "spk_samsat_stck", 
					caption: "STCK", 
					formItem: {
						colSpan: 1,
						label: {
							text: "STCK", 
							location: "top"
						}
					},
				},
				{
					dataField: "spk_samsat_bpkb", 
					caption: "BPKB A/N", 
					formItem: {
						colSpan: 1,
						label: {
							text: "BPKB A/N", 
							location: "top"
						}
					},
				},
				{
					dataField: "spk_samsat_alamat", 
					caption: "ALAMAT", 
					width: 100,
					formItem: {
						colSpan: 1,
						label: {
							text: "ALAMAT", 
							location: "top"
						}
					},
				}
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
				items = this.getDataSource().items();

				$(".x-cetak").unbind('click').click(function(event){
					index = $(this).data("index");
					id = $(this).data("id");
					printTerimaBPKB(id, items[index]);
				});
			},

			onCellPrepared: function(e) {
				if (e.rowType === "data" && e.column.command === "edit") {
					var isEditing = e.row.isEditing,
						$links = e.cellElement.find(".dx-link");

					$links.text("");

					if (isEditing) {
						$links.filter(".dx-link-save").addClass("dx-icon-save");
						$links.filter(".dx-link-cancel").addClass("dx-icon-revert");
					} else {
						$links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
						$links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
					}
				}
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location = "before";
					} else if (item.name === "saveButton") {
						item.location = "before";
					}
				});

				dataGrid = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-awal",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Awal",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						template: function(){
							return $("<span />").html("s.d");
						}
					},
					{
						location: "after",
						widget: "dxDateBox",
						options: {
							type:"date",
							name:"x-filter-date-akhir",
							showClearButton:true,
							acceptCustomValue:false,
							placeholder: "Tanggal Akhir",
							displayFormat:"dd/MM/yyyy",
							width:"150px",
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "filter",
							hint: "Filter",
							onClick: function() {
								startDate = $("input[name='x-filter-date-awal']").val()=="" ? "ALL":$("input[name='x-filter-date-awal']").val();
								endDate = $("input[name='x-filter-date-akhir']").val()=="" ? "ALL":$("input[name='x-filter-date-akhir']").val();
								$filtered = getData(url_api+startDate+"/"+endDate, "spk_po_spk");
								dataGrid.option("dataSource", $filtered);
								dataGrid.refresh();
							}
						}
					},
					{
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							hint: "Refresh & Reset",
							onClick: function() {
								dataGrid.repaint();
								dataGrid.option("dataSource", data);
								dataGrid.refresh();
							}
						}
					}
				);
			}
		});
	</script>

@endsection