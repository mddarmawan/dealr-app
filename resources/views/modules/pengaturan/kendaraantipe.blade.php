@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Tipe Kendaraan</span><br/><small>Pengaturan \ Barang \ Tipe Kendaraan</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

@endsection

@section('scripts')

<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
				var title = $("#x-title").html();
				var data = getData("{{env('API_WEB', false).'api/kendaraan/tipe'}}/","type_id");

				$("#x-data").dxDataGrid({
					dataSource: data,
					columns: [
							{dataField: "type_nama", caption: "Nama Tipe",validationRules: [{ type: "required" }]},
							{dataField: "type_poin", caption: "Poin", width:"100px", alignment:"center",validationRules: [{ type: "required" }]},
							{dataField: "type_status", caption: "Status", width:"100px", validationRules: [{ type: "required" }], dataType:"boolean", alignment:"center", lookup: {dataSource: ["0","1"],displayExpr: function(t){return t==0?"Inactive":"Active"}},
                cellTemplate: function (container, options) {
										if(options.value==1){
	                    $("<div>")
	                        .append($("<span>", { "class": "dx-active" }).html("Active"))
	                        .appendTo(container);
										}else{
											$("<div>")
										 		 .append($("<span>", { "class": "dx-inactive" }).html("Inactive"))
										 		 .appendTo(container);
										}
                }},
					],
					allowColumnResizing: true,
					columnMinWidth: 50,
					columnAutoWidth: true,
					showRowLines: true,
					rowAlternationEnabled: true,
					export:{enabled:true,fileName: title},
					sorting: {mode: "multiple"},
					selection: {mode: "single"},
					filterRow:{visible:true},
					paging: {pageSize: 15},
					pager: {showPageSizeSelector: true,allowedPageSizes: [15, 30, 50],showInfo: true},
					searchPanel:{visible:true,placeholder:"Search...",searchVisibleColumnsOnly:true,},
					editing: {mode: "popup",allowUpdating: true,allowDeleting: true,
						popup: {
								closeOnOutsideClick: true,
								title:"Ubah "+title,
								showTitle:true,
                width: 700,
                height: 180,
								shadingColor: "rgba(0,0,0,0.18)",
                position: {my: "center",at: "center",of: window},
            }
        	},
					onContentReady: function(e){
            moveEditColumnToLeft(e.component);
	        },
	        onCellPrepared: function(e) {
	            if(e.rowType === "data" && e.column.command === "edit") {
	                var isEditing = e.row.isEditing,
	                    $links = e.cellElement.find(".dx-link");

	                $links.text("");

	                if(isEditing){
	                    $links.filter(".dx-link-save").addClass("dx-icon-save");
	                    $links.filter(".dx-link-cancel").addClass("dx-icon-revert");
	                } else {
	                    $links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
	                    $links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
	                }
	            }
	        },
					onToolbarPreparing: function(e) {
						var toolbarItems = e.toolbarOptions.items;
            $.each(toolbarItems, function(_, item) {
                if(item.name === "exportButton") {
                    item.location="before";
                }else if(item.name === "saveButton") {
                    item.location="before";
                }
            });

						var dataGrid = e.component;
						e.toolbarOptions.items.unshift(
							{
								location: "before",
								widget: "dxButton",
								options: {
									icon: "add",
                  hint: "Add",
									onClick: function() {
										dataGrid.addRow();
										$(".dx-popup-normal .dx-toolbar-before .dx-toolbar-item-content>div").html("Tambah" + title);
									}
								}
							},{
								location: "after",
								widget: "dxButton",
								options: {
									icon: "refresh",
									onClick: function() {
										dataGrid.refresh();
									}
								}
							}
						);
					}
				});
	</script>

@endsection
