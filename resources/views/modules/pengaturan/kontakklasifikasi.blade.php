@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Klasifikasi Kontak</span><br/><small>Pengaturan \ Umum \ Klasifikasi Kontak</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

@endsection

@section('scripts')

<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
				var title = $("#x-title").html();
				var data = getData("{{env('API_WEB', false).'api/kontak/klasifikasi'}}/","kontak_tipe_id");

				$("#x-data").dxDataGrid({
					dataSource: data,
					columns: [
							{dataField: "kontak_kategori_nama", caption: "Nama Klasifikasi",validationRules: [{ type: "required" }]},
					],
					allowColumnResizing: true,
					columnMinWidth: 50,
					columnAutoWidth: true,
					showRowLines: true,
					rowAlternationEnabled: true,
					export:{enabled:true,fileName: title},
					sorting: {mode: "multiple"},
					selection: {mode: "single"},
					filterRow:{visible:true},
					paging: {pageSize: 15},
					pager: {showPageSizeSelector: true,allowedPageSizes: [15, 30, 50],showInfo: true},
					searchPanel:{visible:true,placeholder:"Search...",searchVisibleColumnsOnly:true,},
					editing: {mode: "row",allowUpdating: true,allowDeleting: true,
						popup: {
								closeOnOutsideClick: true,
								title:"Ubah "+title,
								showTitle:true,
                width: 700,
                height: 150,
								shadingColor: "rgba(0,0,0,0.18)",
                position: {my: "center",at: "center",of: window},
            }
        	},
					onContentReady: function(e){
            moveEditColumnToLeft(e.component);
	        },
	        onCellPrepared: function(e) {
	            if(e.rowType === "data" && e.column.command === "edit") {
	                var isEditing = e.row.isEditing,
	                    $links = e.cellElement.find(".dx-link");

	                $links.text("");

	                if(isEditing){
	                    $links.filter(".dx-link-save").addClass("dx-icon-save");
	                    $links.filter(".dx-link-cancel").addClass("dx-icon-revert");
	                } else {
	                    $links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
	                    $links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
	                }
	            }
	        },
					onToolbarPreparing: function(e) {
						var toolbarItems = e.toolbarOptions.items;
            $.each(toolbarItems, function(_, item) {
                if(item.name === "exportButton") {
                    item.location="before";
                }else if(item.name === "saveButton") {
                    item.location="before";
                }
            });

						var dataGrid = e.component;
						e.toolbarOptions.items.unshift(
							{
								location: "before",
								widget: "dxButton",
								options: {
									icon: "add",
                  hint: "Add",
									onClick: function() {
										dataGrid.addRow();
										$(".dx-popup-normal .dx-toolbar-before .dx-toolbar-item-content>div").html("Tambah" + title);
									}
								}
							},{
								location: "after",
								widget: "dxButton",
								options: {
									icon: "refresh",
									onClick: function() {
										dataGrid.refresh();
									}
								}
							}
						);
					}
				});
	</script>

@endsection
