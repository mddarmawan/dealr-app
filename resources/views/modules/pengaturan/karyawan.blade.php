@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="content_encar">
		<div class="x-header-panel">
				<h3 class="x-title"><span id="x-title">Karyawan</span><br/><small>Pengaturan \ Kepegawaian \ Karyawan</small></h3>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

@endsection

@section('scripts')

<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
				var title = $("#x-title").html();

				var jabatan = {!!$data['jabatan']!!};
				var bank = {!!$data['bank']!!};
				var data = getData("{{env('API_WEB', false).'api/karyawan'}}/","karyawan_id");

				$("#x-data").dxDataGrid({
					dataSource: data,
					columns: [
						{dataField: "karyawan_status", caption: "Status", validationRules: [{ type: "required" }], dataType:"boolean", alignment:"center", lookup: {dataSource: ["0","1"],displayExpr: function(t){return t==0?"Inactive":"Active"}},
							cellTemplate: function (container, options) {
									if(options.value==1){
										$("<div>")
												.append($("<span>", { "class": "dx-active" }).html("Active"))
												.appendTo(container);
									}else{
										$("<div>")
											 .append($("<span>", { "class": "dx-inactive" }).html("Inactive"))
											 .appendTo(container);
									}
							}},
							{dataField: "karyawan_kerja", caption: "Tgl. Kerja",dataType: "date", format:"dd/MM/yyyy", validationRules: [{ type: "required" }]},
							{dataField: "karyawan_id", caption: "NIK", allowEditing:true, validationRules: [{ type: "required" }]},
							{dataField: "karyawan_nama", caption: "Nama Karyawan",validationRules: [{ type: "required" }]},
							{dataField: "karyawan_jabatan", caption: "Jabatan",validationRules: [{ type: "required" }], lookup: {dataSource: jabatan,displayExpr: "jabatan_nama",valueExpr: "jabatan_id"}},
							{dataField: "karyawan_jenis", caption: "Kategori", dataType:"boolean",validationRules: [{ type: "required" }], lookup: {dataSource: ["0","1"],displayExpr: function(t){return t==0?"Non Tetap":"Tetap"}},
							cellTemplate: function (container, options) {
									if(options.value==1){
										$("<div>")
												.append("Tetap")
												.appendTo(container);
									}else{
										$("<div>")
											 .append("Non Tetap")
											 .appendTo(container);
									}
							}},
							{dataField: "karyawan_identitas", caption: "No. Identitas"},
							{dataField: "karyawan_npwp", caption: "NPWP"},
							{dataField: "karyawan_lahir", caption: "Tgl. Lahir",dataType: "date", format:"dd/MM/yyyy"},
							{dataField: "karyawan_alamat", caption: "Alamat", },
							{dataField: "karyawan_telp", caption: "Telepon", dataType:"phone"},
							{dataField: "karyawan_email", caption: "Email",dataType: "email"},
							{dataField: "karyawan_bank", caption: "Bank", lookup: {dataSource: bank,displayExpr: "bank_kategori_nama",valueExpr: "bank_kategori_id"}},
							{dataField: "karyawan_rek", caption: "No. Rekening"},
							{dataField: "karyawan_an", caption: "Atas Nama"},
					],
					dateSerializationFormat:"dd/MM/yyyy",
					allowColumnResizing: true,
					columnMinWidth: 100,
					columnAutoWidth: true,
					showRowLines: true,
					rowAlternationEnabled: true,
					export:{enabled:true,fileName: title},
					sorting: {mode: "multiple"},
					selection: {mode: "single"},
					filterRow:{visible:true},
					paging: {pageSize: 15},
					pager: {showPageSizeSelector: true,allowedPageSizes: [15, 30, 50],showInfo: true},
					searchPanel:{visible:true,placeholder:"Search...",searchVisibleColumnsOnly:true,},
					editing: {mode: "popup",allowUpdating: true,allowDeleting: true,
						popup: {
								closeOnOutsideClick: true,
								title:"Ubah "+title,
								showTitle:true,
                width: 700,
                height: 390,
								shadingColor: "rgba(0,0,0,0.18)",
                position: {my: "center",at: "center",of: window},
            }
        	},
					onContentReady: function(e){
            moveEditColumnToLeft(e.component);
	        },
	        onCellPrepared: function(e) {
	            if(e.rowType === "data" && e.column.command === "edit") {
	                var isEditing = e.row.isEditing,
	                    $links = e.cellElement.find(".dx-link");

	                $links.text("");

	                if(isEditing){
	                    $links.filter(".dx-link-save").addClass("dx-icon-save");
	                    $links.filter(".dx-link-cancel").addClass("dx-icon-revert");
	                } else {
	                    $links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
	                    $links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
	                }
	            }
	        },
					onToolbarPreparing: function(e) {
						var toolbarItems = e.toolbarOptions.items;
            $.each(toolbarItems, function(_, item) {
                if(item.name === "exportButton") {
                    item.location="before";
                }else if(item.name === "saveButton") {
                    item.location="before";
                }
            });

						var dataGrid = e.component;
						e.toolbarOptions.items.unshift(
							{
								location: "before",
								widget: "dxButton",
								options: {
									icon: "add",
                  hint: "Add",
									onClick: function() {
										dataGrid.addRow();
										$(".dx-popup-normal .dx-toolbar-before .dx-toolbar-item-content>div").html("Tambah " + title);
									}
								}
							},{
								location: "after",
								widget: "dxButton",
								options: {
									icon: "refresh",
									onClick: function() {
										dataGrid.refresh();
									}
								}
							}
						);
					},onEditorPreparing: function(e) {
						if (e.dataField == "karyawan_email" && e.id !=null) {
							console.log(e.id);
							$("#"+e.id).attr("type","email");
						}
        }
				});
	</script>

@endsection
