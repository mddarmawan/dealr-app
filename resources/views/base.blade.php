@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="content_encar">
		<div class="x-header-panel">
			<div class="x-nav-item">
					<h3 class="x-title"><span id="x-title">Permintaan SPK</span><br/><small>Penjualan \ Permintaan SPK</small></h3>
			</div>
		</div>
		<div class="x-body-panel">
			<div id="x-data" class="x-table"></div>
		</div>
	</div>

@endsection

@section('scripts')

	<script src="{{ asset('/js/DataController.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
		var title = $("#x-title").html();
		var data = getData("{{ env('API_WEB', false) }}/api/grade", "grade_id");

		$("#x-data").dxDataGrid({
			dataSource				: data,
			allowColumnResizing		: true,
			columnMinWidth			: 50,
			columnAutoWidth			: true,
			showRowLines			: true,
			rowAlternationEnabled	: true,
			export 					: {enabled: true, fileName: title},
			sorting 				: {mode: "multiple"},
			selection 				: {mode: "single"},
			filterRow 				: {visible: true},
			paging 					: {pageSize: 15},
			pager 					: {showPageSizeSelector: true, allowedPageSizes: [15, 30, 50], showInfo: true},
			searchPanel 			: {visible: true, placeholder:"Search...", searchVisibleColumnsOnly: true,},
			editing 				: {mode: "form", allowUpdating: true, allowDeleting: true},

			columns: [
				{dataField: "grade_nama", caption: "Nama Grade", width:"150px",validationRules: [{ type: "required" }]},
				{dataField: "grade_target", caption: "Target", width:"100px", alignment:"center", validationRules: [{ type: "required" }]},
				{dataField: "grade_ket", caption: "Keterangan"},
			],

			onContentReady: function(e) {
				moveEditColumnToLeft(e.component);
			},

			onCellPrepared: function(e) {
				if (e.rowType === "data" && e.column.command === "edit") {
					var isEditing = e.row.isEditing,
						$links = e.cellElement.find(".dx-link");

					$links.text("");

					if (isEditing) {
						$links.filter(".dx-link-save").addClass("dx-icon-save");
						$links.filter(".dx-link-cancel").addClass("dx-icon-revert");
					} else {
						$links.filter(".dx-link-edit").addClass("dx-icon-edit dx-color-yellow");
						$links.filter(".dx-link-delete").addClass("dx-icon-trash dx-color-red");
					}
				}
			},

			onToolbarPreparing: function(e) {
				var toolbarItems = e.toolbarOptions.items;
				$.each(toolbarItems, function(_, item) {
					if (item.name === "exportButton") {
						item.location="before";
					} else if (item.name === "saveButton") {
						item.location="before";
					}
				});

				var dataGrid = e.component;
				e.toolbarOptions.items.unshift(
					{
						location: "before",
						widget: "dxButton",
						options: {
							icon: "add",
							hint: "Add",
							onClick: function() {
								dataGrid.addRow();
								$(".dx-popup-normal .dx-toolbar-before .dx-toolbar-item-content>div").html("Tambah "+title);
							}
						}
					}, {
						location: "after",
						widget: "dxButton",
						options: {
							icon: "refresh",
							onClick: function() {
								dataGrid.refresh();
							}
						}
					}
				);
			}
		});
	</script>

@endsection
