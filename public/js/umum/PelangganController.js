	var popup=null;
	var dataGrid=null;
	var currentData=null;
	var tabItem = [
		{ index:0, text: "Pelanggan" }
	];

	var showPelanggan = function(data, spk) {
		var daftar_spk = "";
		var no = 1;

		$.each(spk,function(id, item){
			daftar_spk += "<tr><td>"+(item.spk_tgl!=""?date_format(item.spk_tgl):"")+"</td><td>"+item.spk_pel_spk+"</td><td>"+item.spk_stnk_nama+"</td><td>"+item.karyawan_nama+"</td><td>"+item.type_nama+"</td><td>"+(item.spk_pembayaran=="1"?"CREDIT":"CASH")+"</td><td>"+item.kontak_nama+"</td><td>"+item.spk_do_tgl+"</td></tr>";
			no++;
		});

		return $("<div />").addClass("row m-0 p-10").append(
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label'>Nama Pelanggan</td><td class='text-center' width=30px>:</td><td> " + data.pel_nama + "</td></tr>"),
				$("<tr><td class='label'>Tanggal Lahir</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pel_lahir!=""?date_format(data.pel_lahir):"") + "</td></tr>"),
				$("<tr><td class='label'>Kode Pos</td><td class='text-center' width=30px>:</td><td> " + data.pel_pos + "</td></tr>"),
				$("<tr><td class='label' colspan=3>SPK:</td></tr>")
			),
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label'>Telepon</td><td class='text-center' width=30px>:</td><td> " + data.pel_telp + "</td></tr>"),
				$("<tr><td class='label'>Ponsel</td><td class='text-center' width=30px>:</td><td> " + data.pel_ponsel + "</td></tr>"),
				$("<tr><td class='label'>Email</td><td class='text-center' width=30px>:</td><td>" + data.pel_email + "</td></tr>"),
				$("<tr><td class='label'>Kota</td><td class='text-center' width=30px>:</td><td>" + data.pel_kota + "</td></tr>"),
			),
			$("<br><hr><h3>Daftar SPK</h3><hr><br>"),
			$("<table id='daftar_spk' class='col-12 table-field'/>").append(
				$("<tr><td class='label'>TANGGAL</td><td class='label'>NO. SPK</td><td class='label'>NAMA STNK</td><td class='label'>SALES</td><td class='label'>TYPE</td><td class='label'>PEMBAYARAN</td><td class='label'>LEASING</td><td class='label'>TGL. DO</td></tr>"),
				$(daftar_spk)
			)
		);
	}

	var showInfo = function(id, data) {
		var spk = "";
		popup =  $("#x-detail").dxPopup({
			showTitle: true,
			title:"Detail Pelanggan "+data.pel_id,
			width:800,
			height:380,
			visible: false,
			dragEnabled: true,
			shadingColor: "rgba(0,0,0,0.18)",
			closeOnOutsideClick: true,

			contentTemplate: function() {
				var template =  $("<div />");
				template.append("<div class='tabs'></div>");
				var tabcontainer =  $("<div />").addClass("tabs-container");
				$.each(tabItem,function(i,item){
					tabcontainer.append("<div id='"+ item.text +"' class='tabs-item'></div>")
				});
				template.append(tabcontainer);

				return template;
			},

			onShowing: function(e){
				$.ajax({
					url: base_url + "api/penjualan/spk/PELANGGAN/" + data.pel_id,
					dataType:"json",
					success: function(result) {
						$("#Pelanggan").html(showPelanggan(data, result)).addClass("show");;
					},error: function(e) {
						$("#Pelanggan").html(showPelanggan(data, "")).addClass("show");;
					},
					timeout: 10000
				});

				$(".dx-popup-content").addClass("p-0");
				var tabsInstance = $(".tabs").dxTabs({
					dataSource: tabItem,
					selectedIndex: 0,
					onItemClick: function(e) {
						$(".tabs-item").removeClass("show");
						$("#"+e.itemData.text).addClass("show");
					}
				}).dxTabs("instance");
			}
		}).dxPopup("instance");
		popup.show();
	};
