var config = {
  apiKey: "AIzaSyAbGQT6dUxS5arqAz5DgG1RskqUMX-XOPk",
  authDomain: "dealer-ms.firebaseapp.com",
  databaseURL: "https://dealer-ms.firebaseio.com",
  projectId: "dealer-ms",
  storageBucket: "dealer-ms.appspot.com",
  messagingSenderId: "950788435795"
};
firebase.initializeApp(config);
function getData(dataURL, id){
  return new DevExpress.data.CustomStore({
    loadMode: "raw",
    key:id,
    load: function (loadOptions) {
      var deferred = $.Deferred();

      $.ajax({
        url: dataURL,
        success: function(result) {
          deferred.resolve(result);
        },
        error: function(e) {
          deferred.reject("Data Loading Error");
        },
        timeout: 10000
      });

      return deferred.promise();
    },
    update: function (key, values){
      console.log(key);
      var deferred = $.Deferred();
      $.ajax({
        url: dataURL + "show/" + key.username,
        method: "POST",
        dataType: "json"
      }).done(function(response){
        if (response==1){
          //INSERT
          if(typeof values.pwd == "undefined"){
            alert("Kata Sandi belum diisi!");
            deferred.resolve(key);
          }else{
            firebase.auth().createUserWithEmailAndPassword(values.username, values.pwd).then(function(response){
              console.log(response);
              if (typeof response !== "undefined"){
                  var data = {username:values.username, password:values.pwd, uid:response.uid, scope:values.scope};
                  $.ajax({
                    url: dataURL + key.username + "/"+key.karyawan_id,
                    method: "POST",
                    dataType: "json",
                    data: {data:JSON.stringify(data)}
                  }).done(function(){
                    deferred.resolve(key);
                  });
              }
            }).catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode=="auth/email-already-in-use"){
                    alert("Alamat email telah digunakan! silahkan gunakan alamat email lain!");
                    deferred.resolve(key);
                }
            });
          }
        }else if (response==2){
          //update
          $.ajax({
            url: dataURL + key.username + "/"+key.karyawan_id,
            method: "POST",
            dataType: "json",
            data: {data:JSON.stringify(values)}
          }).done(function(){
            deferred.resolve(key);
          });
        }
      });

      return deferred.promise();
    }
  });
}
