	
	/**
	 * formatDate
	 *
	 */
	var formatDate = function(date)
	{
		var monthNames = [
		"Januari", "Februari", "Maret",
		"April", "Mei", "Juni", "Juli",
		"Agustus", "September", "Oktober",
		"November", "Desember"
		];

		var day = date.getDate();
		var monthIndex = date.getMonth();
		var year = date.getFullYear();

		return day + ' ' + monthNames[monthIndex] + ' ' + year;
	}

	/**
	 * printIframe
	 *
	 */
	var printIframe = function(html)
	{
		$("#printf").contents().find("body").append(html);
		$("#printf").get(0).contentWindow.print();
		$("#printf").contents().find("body").html("");
	}
	
	/**
	 * printFaktur
	 *
	 */
	var printFaktur = function(id, item)
	{
		var riwayat_pembayaran = '';

		// for (var i = (item.riwayat_pembayaran).length - 1; i >= 0; i--) {
		// 	riwayat_pembayaran += '\
		// 	<tr>\
		// 		<td width="50%">' + item.riwayat_pembayaran[i].spk_bayar_ket + '</td>\
		// 		<td width="50%" class="text-right"><p style="float: right; margin-bottom: 4px;">Rp. ' + item.riwayat_pembayaran[i].spk_bayar_jumlah + '</p></td>\
		// 	</tr>';
		// }

		var html = '\
			<style>\
				body {\
					font-size: 10px !important;\
					color:#BDBDBD !important;\
					font-family: Courier !important;\
				}\
				table {\
					font-size: 10px !important;\
					color:#BDBDBD !important;\
					font-family: Courier !important;\
				}\
				div {\
					font-size: 10px !important;\
					color:#BDBDBD !important;\
					font-family: Courier !important;\
				}\
			</style>\
			<body>\
				<table style="width: 100%;">\
					<tr>\
						<td style="width: 70%;">\
							<p style="margin-top: 20px;">\
								PT. Encartha Indonesia <br>\
								JL.Angkatan 45 Palembang 30137 <br>\
								Telp. 0711-374000 fax. 0711-374500 <br>\
								NPWP : 01.596.627.6.308.000 Tgl. Pengukuhan : 07-04-2008 <br>\
								BKB : 01.596.627.6.308.000 <br><br>\
								No. SPK : ' + item.spk_id + ' <br>\
								No. Indent : ' + item.spk_id + ' <br>\
								Referensi : ' + item.spk_id + '\
							</p>\
						</td>\
						<td>\
							<h4 style="margin-left: 20px;">FAKTUR KENDARAAN</h4>\
							Nomor : ' + item.spk_do_spk + ' <br>\
							Tanggal : ' + item.spk_do_tgl + ' <br><br>\
							Pemesan : <br>\
							' + item.spk_pel_nama + ' <br>\
							' + item.spk_pel_alamat + ' <br>\
							' + item.spk_pel_kota + '\
						</td>\
					</tr>\
				</table>\
				<div style="border-top: 1px solid #BDBDBD; margin: 10px 0;"></div>\
				<div style="margin-bottom: 10px;">\
					<div style="width: 70%; float: left;">\
						<table width="100%">\
							<tr>\
								<td width="10%">Keterangan Kendaraan</td>\
								<td width="2%"> : </td>\
								<td width="30%">1 (satu) Unit ' + item.type_nama + ' ' + item.variant_nama + '</td>\
							</tr>\
							<tr>\
								<td width="15%">Nomor Rangka</td>\
								<td width="2%"> : </td>\
								<td width="30%">' + item.kend_beli_rangka + '</td>\
							</tr>\
							<tr>\
								<td width="10%">Nomor Mesin</td>\
								<td width="2%"> : </td>\
								<td width="30%">' + item.kend_beli_mesin + '</td>\
							</tr>\
							<tr>\
								<td width="15%">Serial</td>\
								<td width="2%"> : </td>\
								<td width="30%">' + item.variant_serial + '</td>\
							</tr>\
							<tr>\
								<td width="15%">Warna</td>\
								<td width="2%"> : </td>\
								<td width="30%">' + item.warna_nama + '</td>\
							</tr>\
							<tr>\
								<td width="15%">No. DH</td>\
								<td width="2%"> : </td>\
								<td width="30%">' + item.spk_match_dh + '</td>\
							</tr>\
						</table>\
					</div>\
					<div style="width: 30%; float: right;">\
						<p style="margin:0; margin-bottom: 2px;">STNK atas Nama : </p>\
						<p style="margin:0; margin-bottom: 1px;">' + item.spk_stnk_nama + '</p>\
						<p style="margin:0; margin-bottom: 1px;">' + item.spk_stnk_alamat + '</p>\
						<p style="margin:0;">' + item.spk_pel_kota + '</p>\
					</div>\
				</div>\
				<table width="100%" style="margin-top: 20px;">\
					<tr>\
						<td width="50%"><b>KETERANGAN</b></td>\
						<td width="50%" class="text-right"><b>HARGA &nbsp; &nbsp;</b></td>\
					</tr>\
					<tr>\
						<td width="50%">Harga Off The Road</td>\
						<td width="50%" class="text-right">' + item.variant_off + '</td>\
					</tr>\
					<tr>\
						<td width="50%">BBN</td>\
						<td width="50%" class="text-right">' + item.variant_bbn + '</td>\
					</tr>\
					<tr>\
						<td width="50%">On The Road</td>\
						<td width="50%" class="text-right">' + item.variant_on + '</td>\
					</tr>\
				</table>\
				<div style="border-top: 1px solid #BDBDBD; margin: 10px 0;"></div>\
				<table width="100%">\
					<tr>\
						<td width="50%"></td>\
						<td width="50%" class="text-right"><p style="float: right;">___________________</p></td>\
					</tr>\
					<tr>\
						<td width="50%"></td>\
						<td width="50%" class="text-right"><p style="float: right;">&nbsp;</p></td>\
					</tr>\
					<tr>\
						<td width="50%" style="margin-bottom: 4px;">Pembayaran Konsumen</td>\
						<td width="50%" class="text-right"><p style="float: right; margin-bottom: 4px;">Rp. ' + item.jumlah + '</p></td>\
					</tr>\
					<tr>\
						<td width="50%">Cash Back Dealer</td>\
						<td width="50%" class="text-right">\
							<p style="float: right;">\
								Rp. ' + item.total_diskon + '\
							</p>\
						</td>\
					</tr>\
					<tr>\
						<td width="50%"></td>\
						<td width="50%" class="text-right"><p style="float: right;">___________________</p></td>\
					</tr>\
					<tr>\
						<td width="50%"></td>\
						<td width="50%" class="text-right"><p style="float: right;">&nbsp;</p></td>\
					</tr>' + riwayat_pembayaran +
					'<tr>\
						<td width="50%"></td>\
						<td width="50%" class="text-right"><p style="float: right;">___________________</p></td>\
					</tr>\
					<tr>\
						<td width="50%"></td>\
						<td width="50%" class="text-right"><p style="float: right;">&nbsp;</p></td>\
					</tr>\
					<tr>\
						<td width="50%">Dropping</td>\
						<td width="50%" class="text-right"><p style="float: right;">' + item.spk_po_droping + ' </p></td>\
					</tr>\
				</table>\
				<table style="margin-top: 150px;">\
					<tr>\
						<td width="9%">Jenis Pembayaran</td>\
						<td width="2%"> : </td>\
						<td width="70%">' + (item.spk_pembayaran == '0' ? 'CASH' : 'CREDIT') + '</td>\
					</tr>\
					<tr>\
						<td>Leasing</td>\
						<td> : </td>\
						<td>' + item.spk_po_leasing_nama + ' / ' + item.ajenis_nama + '</td>\
					</tr>\
					<tr>\
						<td>Jangka Waktu</td>\
						<td> : </td>\
						<td>' + item.spk_po_waktu + '</td>\
					</tr>\
				</table>\
				<div style="border-top: 1px solid #000; margin: 10px 0;"></div>\
				<table width="100%" style="margin-top: 20px;">\
					<tr>\
						<td width="50%"><b>Dibuat oleh</b></td>\
						<td width="50%" class="text-right"><b>Pemimpin</b></td>\
					</tr>\
				</table>\
				<table width="100%" style="margin-top: 50px;">\
					<tr>\
						<td width="50%"><b>__________________</b></b></td>\
						<td width="50%" class="text-right"><b>__________________</b></td>\
					</tr>\
				</table>\
			</body>';

		printIframe(html);
	}

	/**
	 * printBastSPK
	 *
	 */
	var printBastSPK = function(id, item)
	{
		var html = '\
			<div style="">\
				<p>\
					<h2 style="margin: 0;">BUKTI SERAH TERIMA KENDARAAN</h2>\
				</p>\
				<div style="display: inline-block; float: left; width: 55%;">\
					<tt>\
					<table class="table" style="font-size: 12px; margin-top: 10px;">\
						<tr>\
							<td class="td"><a style="margin: 30px 0;">PT. Encartha Indonesia</a></td>\
						</tr>\
						<tr>\
							<td class="td"><a style="margin: 30px 0;">JL.Angkatan 45 Palembang 30137</a></td>\
							<td> </td>\
						</tr>\
						<tr>\
							<td class="td"><a style="margin: 30px 0;">Telp. 0711-374000 fax. 0711-374500</a></td>\
							<th></th>\
							<td></td>\
						</tr>\
					</table>\
					<p style="font-size: 12px; margin-top: -2px; margin-left: 3px ">NPWP : 01.596.627.6.308.000 Tgl. Pengukuhan : 07-04-2008</p>\
					</tt>\
				</div>\
				<div style="display: inline-block; float: right; width: 45%;">\
					<tt>\
					<table class="table" style="font-size: 12px; margin-left: 55px; margin-top: 10px;">\
						<tr>\
							<td class="td"><a style="margin: 30px 0;">Nomor</a></td>\
							<th> : </th>\
							<td class="td_isi"> BP{{$data->spk_id}} </td>\
						</tr>\
						<tr>\
							<td class="td"><a style="margin: 30px 0;">Tanggal</a></td>\
							<th> : </th>\
							<td> ' + item.spk_do_tgl + ' </td>\
						</tr>\
						<tr>\
							<td class="td"><a style="margin: 30px 0;">&nbsp;</a></td>\
							<th></th>\
							<td></td>\
						</tr>\
						<tr>\
							<td class="td"><a style="margin: 30px 0;">&nbsp;</a></td>\
							<th></th>\
							<td></td>\
						</tr>\
					</table>\
					</tt>\
				</div>\
				<div style="display: inline-block; float: left; width: 48.5%%;">\
					<tt>\
						<table style="font-size: 12px;  float: left;">\
						<tr>\
							<td><b>Pesanan :</b></td>\
						</tr>\
						<tr>\
							<td><p style="font-size: 12px"> ' + item.spk_pel_nama + ' </p></td>\
						</tr>\
						<tr>\
							<td><p style="font-size: 12px"> ' + item.spk_pel_alamat + ' </p></td>\
						</tr>\
					</table>\
					</tt>\
				</div>\
				<div style="display: inline-block; float: right; width: 48.5%%;">\
					<tt>\
					<table style="font-size: 12px;  float: left;">\
						<tr>\
							<td><b>STNK atas Nama :</b></td>\
						</tr>\
						<tr>\
							<td><p style="font-size: 12px"> ' + item.spk_stnk_nama + ' </p></td>\
						</tr>\
						<tr>\
							<td><p style="font-size: 12px"> ' + item.spk_stnk_alamat + '</p></td>\
						</tr>\
					</table>\
					</tt>\
				</div>\
			</div>\
			<div style="border-top: 1px solid #000; margin-bottom: 10px;"></div>\
			<div style="margin-bottom:;">\
				<div style="width: 50%; float: left;">\
					<table width="100%">\
						<tr>\
							<td width="15%"><p style="font-size: 12px">No. SPK</p></td>\
							<td width="2%"><p style="font-size: 12px">:</p></td>\
							<td width="30%"><p style="font-size: 12px"> ' + item.spk_id + ' </p></td>\
						</tr>\
						<tr>\
							<td width="15%"><p style="font-size: 12px">Tipe</p></td>\
							<td width="2%"><p style="font-size: 12px">:</p></td>\
							<td width="30%"><p style="font-size: 12px"> ' + item.variant_serial + ' </p></td>\
						</tr>\
						<tr>\
							<td width="15%"><p style="font-size: 12px">Jenis Pembayaran</p></td>\
							<td width="2%"><p style="font-size: 12px">:</p></td>\
							<td width="30%"><p style="font-size: 12px"> ' + (item.spk_pembayaran == '0' ? 'CASH' : 'CREDIT') + ' </p></td>\
						</tr>\
						<tr>\
							<td width="15%"><p style="font-size: 12px">No. DH</p></td>\
							<td width="2%"><p style="font-size: 12px">:</p></td>\
							<td width="30%"><p style="font-size: 12px">' + item.kend_beli_dh + ' </p></td>\
						</tr>\
						\
					</table>\
				</div>\
				<div style="width: 50%; float: right;">\
						<table width="100%" style="margin-top: -2px; margin-left: 55px">\
							<tr>\
								<td width="15%"><p style="font-size: 12px; margin-top:-0.5px">1 (satu) Unit DAIHATSU ' + item.variant_nama + ' </p></td>\
							</tr>\
						</table>\
						<table width="100%" style="margin-top: -2px; margin-left: 55px">\
							<tr>\
								<td width="15%"><p style="font-size: 12px">No. Rangka</p></td>\
								<td width="2%"><p style="font-size: 12px">:</p></td>\
								<td width="30%"><p style="font-size: 12px">' + item.kend_beli_rangka + ' </p></td>\
							</tr>\
							<tr>\
								<td width="15%"><p style="font-size: 12px">No. Mesin</p></td>\
								<td width="2%"><p style="font-size: 12px">:</p></td>\
								<td width="30%"><p style="font-size: 12px">' + item.kend_beli_mesin + ' </p></td>\
							</tr>\
							<tr>\
								<td width="15%"><p style="font-size: 12px">Warna</p></td>\
								<td width="2%"><p style="font-size: 12px">:</p></td>\
								<td width="30%"><p style="font-size: 12px">' + item.warna_nama + ' </p></td>\
							</tr>\
							<tr>\
								<td width="15%"><p style="font-size: 12px">Referensi</p></td>\
								<td width="2%"><p style="font-size: 12px">:</p></td>\
								<td width="30%"><p style="font-size: 12px">1612106-BONI FRANSISCA</p></td>\
							</tr>\
						</table>\
				</div>\
			</div>\
			<div style="border-top: 1px solid #000; margin-top: 7px "></div>\
			<div style="margin-top:4px;">\
				<table>\
					<tr>\
						<td width="250px"><p style="margin: 0;">1 (satu) buah Ban SerepKend. Velg</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="75px">Standar</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Racing</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">1 (satu) buah Buku Pedoman Pemilik</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">1 (satu) buah Dongkrak + Gagang</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">1 (satu) buah Lighter + Asbak Rokok</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">1 (satu) set Tools Standar + Kunci Roda</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">1 (satu) set Kaca Spion Kiri, Kanan dan Dalam</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">1 (satu) set Karpet Depan dan Belakang</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">1 (satu) set Karpet Dalam</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">1 (satu) set Karpet Bagasi</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">1 (satu) set Karpet Penahan Lumpur Depan dan Belakang</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">1 (satu) set Pelindung Sinar Matahari Kiri dan Kanan</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">Emblem</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">Gardan Depan</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">Wiper Kaca Depan</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">Lampu Jauh, Dekat, Sinyal, Hasard, Rem dan Mundur</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">Buku Service No..............................................</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="margin:0;">\
				<table>\
					<tr>\
						<td width="503px"><p style="margin: 0;">Plat Profit No........................./STCK</p></td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="">Ada</td>\
						<td><img style="width: 35px;" src="/assets/img/checkbox.png"></td>\
						<td width="100px">Tidak Ada</td>\
					</tr>\
				</table>\
			</div>\
			<div style="border-top: 1px solid #000; margin: 3px 0;"></div>\
			<div style="width: 100%; ">\
				<div  >\
					<table width="100%" style="margin-top:3px">\
						<tr>\
							<td width="40%"><p >Telah diperiksa dan diterima<br>Dengan baik oleh:</p>\
							<br>\
							<br>\
							<br>\
							<br>\
							<p >__________________________<br>Tanda tangan & Nama Jelas</p>\
							</td>\
							<td width="40%"><p >Mengetahui,</p>\
							<br>\
							<br>\
							<br>\
							<br>\
							<p >__________________________</p>\
							</td>\
							<td><p >Yang Menyerahkan,</p>\
							<br>\
							<br>\
							<br>\
							<br>\
							<p >__________________________</p>\
							</td>\
						</tr>\
					</table>\
				</div>	\
			</div>\
			<div style="border-bottom: 3px dashed #000; margin: 10px 0;"></div>\
			<div style="display: inline-block; float: left; width: 50%;">\
				<tt>\
				<table style="font-size: 12px;  float: left;">\
					<tr>\
						<td><b>Pesanan :</b></td>\
					</tr>\
					<tr>\
						<td><p style="font-size: 12px"> ' + item.spk_pel_nama + ' </p></td>\
					</tr>\
					<tr>\
						<td><p style="font-size: 12px"> ' + item.spk_pel_alamat + ' </p></td>\
					</tr>\
					<br>\
					<tr>\
						<td><p style="font-size: 12px">Ket.</p></td>\
					</tr>\
				</table>\
				</tt>\
			</div>\
			<div style="display: inline-block; float: right; width: 50%; ">\
				<tt>\
				<table width="100%" style="margin-top: -2px; margin-left: 55px">\
					<tr>\
						<td width="15%"><p style="font-size: 12px; margin-top:-0.5px">1 (satu) Unit DAIHATSU  ' + item.variant_nama + ' </p></td>\
					</tr>\
				</table>\
				<table width="100%" style="margin-top: -2px; margin-left: 55px">\
					<tr>\
						<td width="15%"><p style="font-size: 12px">No. Rangka</p></td>\
						<td width="2%"><p style="font-size: 12px">:</p></td>\
						<td width="30%"><p style="font-size: 12px"> ' + item.spk_pel_nama + ' </p></td>\
					</tr>\
					<tr>\
						<td width="15%"><p style="font-size: 12px">No. Mesin</p></td>\
						<td width="2%"><p style="font-size: 12px">:</p></td>\
						<td width="30%"><p style="font-size: 12px"> ' + item.kend_beli_mesin + ' </p></td>\
					</tr>\
					<tr>\
						<td width="15%"><p style="font-size: 12px">Warna</p></td>\
						<td width="2%"><p style="font-size: 12px">:</p></td>\
						<td width="30%"><p style="font-size: 12px"> ' + item.warna_nama + ' </p></td>\
					</tr>\
					<tr>\
						<td width="15%"><p style="font-size: 12px">Referensi</p></td>\
						<td width="2%"><p style="font-size: 12px">:</p></td>\
						<td width="30%"><p style="font-size: 12px">1612106-BONI FRANSISCA</p></td>\
					</tr>\
					<tr>\
						<td width="15%"><p style="font-size: 12px">No. DH</p></td>\
						<td width="2%"><p style="font-size: 12px">:</p></td>\
						<td width="30%"><p style="font-size: 12px"> ' + item.spk_match_dh + ' </p></td>\
					</tr>\
				</table>\
				</tt>\
			</div>';

		printIframe(html);
	}

	/**
	 * printBastStok
	 *
	 */
	var printBastStok = function(id, item)
	{
		var html = "X | id: " + id + " | " + item;

		printIframe(html);
	}

	/**
	 * printTagihanLeasing
	 *
	 */
	var printTagihanLeasing = function(id, item)
	{
		var html = '\
			<div style="padding: 35;">\
				<div style="width: 250px; margin: auto; margin-bottom: 20px;"><b><u><h3>SURAT PERNYATAAN</h3></u></b></div>\
				<p style="margin-bottom: 10px;">Yang bertanda tangan dibawah ini :</p>\
				<table style="margin-bottom: 20px;">\
					<tr>\
						<td style="width: 120px;"><b>Nama</b></td>\
						<td style="width: 5px;"> : &nbsp;&nbsp;</td>\
						<td><b>Daniel Saragat</b></td>\
					</tr>\
					<tr>\
						<td><b>Jabatan</td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b>Branch Manager</b></td>\
					</tr>\
					<tr>\
						<td><b>Alamat</td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> Jl. Angkatan 45 Palembang</td>\
					</tr>\
				</table>\
				<br>\
				<div style="margin-bottom: 10px;">Dengan ini menyatakan bahwa BPKB (Buku Pemilik Kendaraan Bermotor) dan Faktur serta dokumen lainnya untuk kendaraan bermotor sbb: </div>\
				<table style="margin-bottom: 20px;">\
					<tr>\
						<td style="width: 120px;"><b>Merk / Type</b></td>\
						<td style="width: 5px;"> : &nbsp;&nbsp;</td>\
						<td><b> DAIHATSU / ' + item.variant_nama + ' </b></td>\
					</tr>\
					<tr>\
						<td><b>Rangka</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.kend_beli_rangka + ' </b></td>\
					</tr>\
					<tr>\
						<td><b>Mesin</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.kend_beli_mesin + ' </b></td>\
					</tr>\
					<tr>\
						<td><b>Warna</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.warna_nama + ' </b></td>\
					</tr>\
					<tr>\
						<td><b>Tahun/CC</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.kend_beli_tahun + ' / 1500 </b></td>\
					</tr>	\
					<tr>\
						<td><b>Nama BPKB</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.spk_samsat_bpkb + ' </b></td>\
					</tr>\
					<tr>\
						<td><b>Alamat</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.spk_pel_alamat + ' </b></td>\
					</tr>\
				</table>\
				<br><br>\
				<p style="text-align: justify;">\
					Surat-surat kendaraan tersebut (BPKB, FAKTUR, NOTICE PAJAK, NIK) masih dalam proses pengurusan di Kantor SAMSAT oleh kami sebagai penjual kendaraan bermotor dan apabila BPKB serta dokumen-dokumen yang dimaksud selesai akan segera kami serahkan langsung kepada ' + item.spk_po_leasing_nama + ', selambat-lambatnya 3 ( <b>Tiga</b> ) bulan sejak surat pernyataan ini dibuat.\
				<br>\
				<br>\
					Dan surat penyataan BPKB ini wajib dikembalikan kepada PT. ENCARTHA INDONESIA pada saat serah terima BPKB.\
				<br>\
				<br>\
					Selanjutnya dengan ini kami nyatakan bahwa di samping BPKB dan dokumen-dokumen dimaksud tidak akan kami berikan kepada siapapun / pihak manapun dengan dalih dan alsan apapun, kecuali kepada ' + item.spk_po_leasing_nama + ', Cab. ' + item.spk_po_leasing_kota + '.\
				<br>\
				<br>\
					Demikianlah surat penyataan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.\
				</p>\
				<br>\
				<table style="width: 250px; text-align: center; margin-left: auto; margin-right: 0px;">\
					<tr>\
						<td>\
							Palembang, ' + formatDate(new Date()) + '\
							<br>\
							<br>\
							<br>\
							<br>\
							<b><u>Daniel Saragat</u></b>\
							<br>\
							<b>Branch Manager</b>\
						</td>\
					</tr>\
				</table>\
				<br>\
			</div>\
			<div style="page-break-before:always; padding: 35px;">\
				Palembang, 16 Januari 2017\
				<br>\
				<br>\
				<table style="width: 250px;">\
					<tr>\
						<td>\
							<p style="margin-bottom: 2px;">Kepada Yth, </p>\
							<p style="margin-bottom: 2px;">' + item.spk_po_leasing_nama + '</p>\
							<p style="margin-bottom: 2px;">' + item.spk_po_leasing_alamat + '</p>\
							<p style="margin-bottom: 2px;">' + item.spk_po_leasing_kota + '</p>\
						</td>\
					</tr>\
				</table>\
				<br>\
				<div style="margin-bottom: 2px;">Dengan hormat, </div>\
				<div style="margin-bottom: 2px;">Sehubungan dengan penjualan 1 (satu) unit kendaraan Daihatsu yang pendanaannya dilakukan oleh <b>' + item.spk_po_leasing_nama + '</b> dengan perincian sbb: </div>\
				<br>\
				<table>\
					<tr>\
						<td style="width: 200px;"><b>Nama</b></td>\
						<td style="width: 2px;"> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.spk_pel_nama + ' </b></td>\
					</tr>\
					<tr>\
						<td><b>Alamat</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.spk_pel_alamat + ' </b></td>\
					</tr>\
					<tr>\
						<td><b>Merk / Type</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> DAIHATSU / ' + item.variant_nama + '</b></td>\
					</tr>\
					<tr>\
						<td><b>Rangka</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.kend_beli_rangka + ' </b></td>\
					</tr>\
					<tr>\
						<td><b>Mesin</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.kend_beli_mesin + ' </b></td>\
					</tr>\
					<tr>\
						<td><b>Warna</td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.warna_nama + ' </td>\
					</tr>\
					<tr>\
						<td><b>STNK a/n</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.spk_stnk_nama + ' </b></td>\
					</tr>\
					<tr>\
						<td><b>Tahun/CC</b></td>\
						<td> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.kend_beli_tahun + ' / 1500 </b></td>\
					</tr>\
				</table>\
				<br>\
				<table>\
					<tr>\
						<td style="width: 75%;">Harga On The Road</td>\
						<td style="width: 2px;"> : &nbsp;&nbsp;</td>\
						<td>Rp. &nbsp;&nbsp;&nbsp; ' + item.kend_beli_harga + '</td>\
					</tr>\
					<tr>\
						<td>Uang Muka</td>\
						<td style="width: 2px;"> : &nbsp;&nbsp;</td>\
						<td>Rp. &nbsp;&nbsp;&nbsp; ' + item.jumlah + '</td>\
					</tr>\
					<tr>\
						<td></td>\
						<td style="width: 2px;"></td>\
						<td> ----------------------- (-) </td>\
					</tr>\
					<tr>\
						<td><b>Total Pelunasan ...........</b></td>\
						<td style="width: 2px;"> : &nbsp;&nbsp;</td>\
						<td>Rp. &nbsp;&nbsp;&nbsp; <b> ' + item.piutang + '</b> </td>\
					</tr>\
				</table>\
				<table>\
					<tr>\
						<td style="width: 50%;">Terbilang</a></td>\
						<td style="width: 2px;"> : &nbsp;&nbsp;</td>\
						<td><b> ' + item.piutang_terbilang + '</b></td>\
					</tr>\
				</table>\
				<p style="margin-bottom: 5px;">Maka kami harapkan sisa pembayaran tersebut dapat ditransfer ke rekening sbb:</p>\
				<table>\
					<tr>\
						<td style="width: 200px;"> Bank </td>\
						<td style="width: 2px;"> : &nbsp;&nbsp;</td>\
						<td><b> Bank Permata Jl. Kol.Atmo No. 479 Palembang</b></td>\
					</tr>\
					<tr>\
						<td> A/C </td>\
						<td style="width: 2px;"> : &nbsp;&nbsp;</td>\
						<td><b> 131.100.5835</b></td>\
					</tr>\
					<tr>\
						<td> A/N </td>\
						<td style="width: 2px;"> : &nbsp;&nbsp;</td>\
						<td><b> PT. Encartha Indonesia</b></td>\
					</tr>\
				</table>\
				<p style="margin-bottom: 2px;">Atas bantuan dan kerjasamanya kami ucapkan terimakasih.</p>\
				<p style="margin-bottom: 2px;">Hormat kami,</p>\
				<br>\
				<b><u>Daniel Saragat</u></b><br>\
				Branch Manager\
			</div>';

		printIframe(html);
	}

	/**
	 * printKwitansiDP
	 *
	 */
	var printKwitansiDP = function(id, item)
	{
		var html = '\
			<style type="text/css">\
				body{\
					font-family: Courier;\
					font-size: 10px;\
				}\
				.head{\
					width: 100%;\
					border-top:1px solid #000;\
					border-bottom:1px solid #000;\
					font-size: 15px;\
				}\
				.bulat{\
					background-color: #000;\
					color: #fff;\
					border-radius: 30px;\
					width: 15px;\
					height: 15px;\
					-webkit-print-color-adjust: exact; \
					text-align: center;	\
				}\
				.col-3{\
					width: 55%;\
					display: inline-block; 	\
				}\
			</style>\
			<div>\
				<div style="float: right; top:0; width: 11%; margin-top: 2%;">\
					<tt><h3 style="font-weight: normal;">No. 43931</h3></tt>\
				</div>\
				<div style="display: inline-block; margin-right:10px;">\
					<img style=" margin-bottom: 20px; max-width: 100%; width: 150px;" src="/assets/img/logo.png">\
					<br>\
					<tt style="font-size: 13px;">PT. ENCARTHA INDONESIA<br>\
					Palembang</tt>\
				</div>\
			</div> \
			<div style="position: absolute; top:120px; left:240px; width: 100%;">\
					<tt><b>JL. Angkatan 45 Palembang 30137<br>\
						SUMSEL - Indonesia\
					</b><br>\
					<span style="display: inline-block;widows: 100px;">\
						<p style="margin-top: 5px;">\
							<p class="bulat"><b>P</b></p>\
						</p>\
						<p style="margin-top: 5px;">\
							<p class="bulat"><b>F</b></p>\
						</p>\
						<p style="margin-top: 5px;">\
							<p class="bulat"><b>W</b></p>\
						</p> \
					</span>\
					<div style="display: inline-block;margin-left: 20px;float: right;margin-top:-60px;position: absolute;">\
						<p style="margin-top: 6px;">\
							0711-374500 (Showroom), 0711-374800 (Service & Part)\
							<div style="margin-top: 19px;"></div>\
							0711-374500\
							<div style="margin-top: 6px;"></div>\
							www.encar.co.id\
						</p>\
					</div>\
				</tt>\
			</div>\
			<tt>\
				<table style="width:100%; margin-top: 30px;">\
					<tr>\
						<td style="text-align: right; width: 50%;">\
							<h2 style="font-weight: normal;">KWITANSI</h2>\
						</td>\
						<td style="width:29%;">&nbsp;</td>\
						<td>\
							<p style="margin-bottom: 2px;"> Nomor :  BP{{ $data->spk_id }} </p>\
							<p style="margin-bottom: 2px;"> Tanggal : {{ $data->spk_tgl }}  </p>\
							<p style="margin-bottom: 2px;"> Akun : 110251 - Bank Permata</p>\
						</td>\
					</tr>\
				</table>\
			</tt>\
			<p style="margin:0;"><tt>Terima : {{ $data->spk_pel_nama }}</tt></p>		<tt>\
				<div style="border-top: 1px solid #000;"></div>\
				<table width="99%">\
					<tr class="tr">\
						<td style="width: 120px;">No.</td>\
						<td style="width: 450px;">Keterangan</td>\
						<td style="text-align: right;">\
					</tr>\
				</table>\
				<div style="border-bottom: 1px solid #000;"></div>\
			</tt>\
			<tt>\
				<table>\
					<tr>\
						<td style="width: 120px;">\
							<br>\
							1.\
							<br><br><br>\
						</td>\
						<td style="width: 450px;">\
							<br>\
							{{ $data->lbayar_ket }} KKB 1 UNIT {{ $data->spk_variant }} :<br>\
							{{ $data->spk_no_rangka }}/{{ $data->spk_no_mesin}}/{{ $data->spk_warna }}\
							<br><br><br>\
						</td>\
						<td style="text-align: right;">\
							{{ $data->lbayar_nominal }}\
							<br><br><br>\
						</td>\
					</tr>\
				</table>\
				<div style="border-bottom: 1px solid #000; margin-top: 100px; margin-bottom: 10px;"></div>\
			</tt>\
			<tt>\
				<table>\
					<tr>\
						<td style="width: 500px;">\
							Terbilang : {{ $data->lbayar_terbilang }}\
						</td>\
						<td style="text-align: right;">\
							Total &nbsp; {{ $data->lbayar_nominal }}\
						</td>\
					</tr>\
					<tr>\
						<td></td>\
						<td style="text-align: center;">\
							<br>\
							<br>\
							<br>\
							Palembang, 12 Januari 2017\
							<br>\
							<br>\
							<br>\
							<br>\
							___________\
							<br>\
							Kasir\
						</td>\
					</tr>\
				</table>\
			</tt>';

		printIframe(html);
	}

	/**
	 * printKwitansiLunas
	 *
	 */
	var printKwitansiLunas = function(id, item)
	{
		var html = "X | id: " + id + " | " + item;

		printIframe(html);
	}

	/**
	 * printTerimaBPKB
	 *
	 */
	var printTerimaBPKB = function(id, item)
	{
		var html = '\
			<div style="padding: 35;">\
				<div style="width: 250px; margin: auto; margin-bottom: 20px;"><b><u><h3>TANDA TERIMA</h3></u></b></div>\
				<table style="margin-bottom: 20px;">\
					<tr>\
						<td style="padding-bottom: 10px;">Yang menerima :</td>\
					</tr>\
					<tr>\
						<td>Nama</td>\
						<td> : </td>\
						<td> ' + item.spk_pel_nama + ' </td>\
					</tr>\
					<tr>\
						<td>Alamat</td>\
						<td> : </td>\
						<td> ' + item.spk_pel_alamat + ' </td>\
					</tr>				\
				</table>\
				<div style="margin-bottom: 10px;">telah menerima BPKB dan Faktur asli dengan spesifikasi sebagai berikut :</div>\
				<table style="margin-bottom: 20px;">\
					<tr>\
						<td style="width: 100px;">BPKB a/n</td>\
						<td> : </td>\
						<td> ' + item.spk_stnk_nama + ' </td>\
					</tr>\
					<tr>\
						<td>No. BPKB</td>\
						<td> : </td>\
						<td> ' + item.spk_samsat_bpkb + ' </td>\
					</tr>\
					<tr>\
						<td>No. Mesin</td>\
						<td> : </td>\
						<td> ' + item.kend_beli_mesin + ' </td>\
					</tr>\
					<tr>\
						<td>No. Rangka</td>\
						<td> : </td>\
						<td> ' + item.kend_beli_rangka + ' </td>\
					</tr>\
					<tr>\
						<td>No. Polisi</td>\
						<td> : </td>\
						<td> ' + item.spk_samsat_nopol + ' </td>\
					</tr>	\
					<tr>\
						<td>No. DH</td>\
						<td> : </td>\
						<td> ' + item.spk_match_dh + ' </td>\
					</tr>\
					<tr>\
						<td>Leasing</td>\
						<td> : </td>\
						<td> ' + item.spk_po_leasing_nama + ' / ' + item.ajenis_nama + ' </td>\
					</tr>					\
				</table>\
				<br><br>\
				<table style="text-align: center;">\
					<tr>\
						<td>\
							Palembang, ' + formatDate(new Date()) + ' <br>\
						</td>\
					</tr>\
					<tr>\
						<td style="width: 30%;">\
							Yang menerima\
							<br><br><br><br>\
							___________________\
						</td>\
						<td style="width: 30%;">\
							Yang Menyerahkan\
							<br><br><br><br>\
							___________________\
						</td>\
						<td style="width: 30%;">\
							Mengetahui\
							<br><br><br><br>\
							___________________\
						</td>\
					</tr>\
				</table>\
			</div>';

		printIframe(html);
	}

	/**
	 * printPembayaran
	 *
	 */
	var printPembayaran = function(id, item)
	{
		var html = '\
		<style type="text/css">\
			body{\
				font-family: Courier;\
				font-size: 10px;\
			}\
			.head{\
				width: 100%;\
				border-top:1px solid #000;\
				border-bottom:1px solid #000;\
				font-size: 15px;\
			}\
			.bulat{\
				background-color: #000;\
				color: #fff;\
				border-radius: 30px;\
				width: 15px;\
				height: 15px;\
				-webkit-print-color-adjust: exact; \
				text-align: center;	\
			}\
			.col-3{\
				width: 55%;\
				display: inline-block; 	\
			}\
			tt{\
				font-size: 10px;\
			}\
		</style>\
		<div>\
			<div style="float: right; top:0; width: 11%; margin-top: 2%;">\
				<tt><h3 style="font-weight: normal;">No. 43931</h3></tt>\
			</div>\
			<div style="display: inline-block; margin-right:10px;">\
				<img style=" margin-bottom: 20px; max-width: 100%; width: 150px;" src="./assets/images/logo.png">\
				<br>\
				<tt style="font-size: 13px;">PT. ENCARTHA INDONESIA<br>\
				Palembang</tt>\
			</div>\
		</div> \
		<div style="margin-top: 5%;">\
			<tt>\
				<b>\
					JL. Angkatan 45 Palembang 30137\
					<br>\
					SUMSEL - Indonesia\
				</b>\
				<br>\
				<br>\
				<table>\
					<tr>\
						<td><span class="bulat"><b>P</b></span></td>\
						<td> 0711-374500 (Showroom), 0711-374800 (Service & Part)</td>\
					</tr>\
					<tr>\
						<td><span class="bulat"><b>F</b></span></td>\
						<td> 0711-374500</td>\
					</tr>\
					<tr>\
						<td><span class="bulat"><b>W</b></span></td>\
						<td> www.encar.co.id</td>\
					</tr>\
				</table>\
			</tt>\
		</div>\
		<tt>\
			<table style="width:100%; margin-top: 20px;">\
				<tr>\
					<td style="text-align: right; width: 50%;">\
						<h2 style="font-weight: normal;">KWITANSI</h2>\
					</td>\
					<td>\
						<p style="margin-bottom: 2px; margin: auto; width: 250px; margin-right: 0px;"> Nomor :  BP{{ $data->spk_id }} </p>\
						<p style="margin-bottom: 2px; margin: auto; width: 250px; margin-right: 0px;"> Tanggal : {{ $data->spk_tgl }}  </p>\
						<p style="margin-bottom: 2px; margin: auto; width: 250px; margin-right: 0px;"> Akun : 110251 - Bank Permata</p>\
					</td>\
				</tr>\
			</table>\
			</tt><p style="margin:0;"><tt>Terima : {{ $data->spk_pel_nama }}</tt</p><tt>\
			<div style="margin-top:5px; border-top: 1px solid #000;"></div>\
			<table width="99%">\
				<tr class="tr">\
					<td style="width: 120px;">No.</td>\
					<td style="width: 450px;">Keterangan</td>\
					<td style="text-align: right;">\
				</tr>\
			</table>\
			<div style="border-bottom: 1px solid #000;"></div>\
		</tt>\
		<tt>\
			<table>\
				<tr>\
					<td style="width: 120px;">\
						<br>\
						1.\
						<br><br><br>\
					</td>\
					<td style="width: 450px;">\
						<br>\
						{{ $data->lbayar_ket }} KKB 1 UNIT {{ $data->spk_variant }} :<br>\
						{{ $data->spk_no_rangka }}/{{ $data->spk_no_mesin}}/{{ $data->spk_warna }}\
						<br><br><br>\
					</td>\
					<td style="text-align: right;">\
						{{ $data->lbayar_nominal }}\
						<br><br><br>\
					</td>\
				</tr>\
			</table>\
			<div style="border-bottom: 1px solid #000; margin-top: 100px; margin-bottom: 10px;"></div>\
		</tt>\
		<tt>\
			<table>\
				<tr>\
					<td style="width: 500px;">\
						Terbilang : {{ $data->lbayar_terbilang }}\
					</td>\
					<td style="text-align: right;">\
						Total &nbsp; {{ $data->lbayar_nominal }}\
					</td>\
				</tr>\
				<tr>\
					<td></td>\
					<td style="text-align: center;">\
						<br>\
						<br>\
						<br>\
						Palembang, 12 Januari 2017\
						<br>\
						<br>\
						<br>\
						<br>\
						___________\
						<br>\
						Kasir\
					</td>\
				</tr>\
			</table>\
		</tt>';

		printIframe(html);
	}