
	var popup=null;
	var dataGrid=null;
	var currentData=null;
	var tabItem = [
		{ index:0, text: "Pelanggan" },
		{ index:1, text: "STNK" },
		{ index:2, text: "Kendaraan" },
		{ index:3, text: "Pembayaran" }
	];

	var showPelanggan = function(data){
		return  $("<div />").addClass("row m-0 p-10").append(
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label'>Kategori</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pel_kategori==0?"PRIBADI":"PERUSAHAAN") + "</td></tr>"),
				$("<tr><td class='label'>Nama Pemesan</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_nama + "</td></tr>"),
				$("<tr><td class='label'>No KTP/KIMS</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_identitas + "</td></tr>"),
				$("<tr><td class='label'>Tanggal Lahir</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pel_lahir!=""?date_format(data.spk_pel_lahir):"") + "</td></tr>"),
				$("<tr><td class='label'>Alamat</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_alamat + "</td></tr>"),
				$("<tr><td class='label'>Provinsi</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_provinsi + "</td></tr>"),
				$("<tr><td class='label'>Kota</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_kota + "</td></tr>"),
				$("<tr><td class='label'>Kecamatan</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_kecamatan + "</td></tr>"),
				$("<tr><td class='label'>Kelurahan</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_kelurahan + "</td></tr>"),
				$("<tr><td class='label'>Kode Pos</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_pos + "</td></tr>"),
			),
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label'>No Telepon</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_telp + "</td></tr>"),
				$("<tr><td class='label'>No HP</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_ponsel + "</td></tr>"),
				$("<tr><td class='label'>Email</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_email + "</td></tr>"),
				$("<tr><td class='label'>NPWP</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_npwp + "</td></tr>"),
				$("<tr><td class='label'>Faktur Pajak</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pajak==0?"TIDAK DIMINTA":"DIMINTA") + "</td></tr>"),
				$("<tr><td class='label' colspan=3>Nama dan Jabatan Contact Person Corporate/Fleet:</td></tr>"),
				$("<tr><td colspan=3>" + data.spk_fleet + "</td></tr>"),
				$("<tr><td colspan=3><img class src='"+base_url+"/public/data/identitas/" + data.spk_pel_fotoid + "'/></td></tr>"),
			)
		);
	}

	var showSTNK = function(data){
		return  $("<div />").addClass("row m-0 p-10").append(
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label'>STNK a.n</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_nama + "</td></tr>"),
				$("<tr><td class='label'>No KTP/KIMS</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_identitas + "</td></tr>"),
				$("<tr><td class='label'>Alamat</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_alamat + "</td></tr>"),
				$("<tr><td class='label'>Provinsi</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_provinsi + "</td></tr>"),
				$("<tr><td class='label'>Kota</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kota + "</td></tr>"),
				$("<tr><td class='label'>Kecamatan</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kecamatan + "</td></tr>"),
				$("<tr><td class='label'>Kelurahan</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kelurahan + "</td></tr>"),
				$("<tr><td class='label'>Kode Pos</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_pos + "</td></tr>"),
				$("<tr><td class='label'>No Telepon</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_telp + "</td></tr>"),
				$("<tr><td class='label'>No HP</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_ponsel + "</td></tr>"),
				$("<tr><td class='label'>Email</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_email + "</td></tr>"),
			),
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label'>Alamat Domisili</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_alamatd + "</td></tr>"),
				$("<tr><td class='label'>Provinsi Domisilii</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_provinsid + "</td></tr>"),
				$("<tr><td class='label'>Kota Domisili</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kotad + "</td></tr>"),
				$("<tr><td class='label'>Kecamatan Domisili</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kecamatand + "</td></tr>"),
				$("<tr><td class='label'>Kelurahan Domisili</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kelurahand + "</td></tr>"),
				$("<tr><td class='label'>Kode Pos Domisili</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_posd + "</td></tr>"),
				$("<tr><td colspan=3><img class src='"+base_url+"/public/data/identitas/" + data.spk_pel_fotoid + "'/></td></tr>"),
			)
		);
	}

	var showKendaraan = function(data){
		return  $("<div />").addClass("row m-0 p-10").append(
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label'>Variant</td><td class='text-center' width=30px>:</td><td>" + data.variant_nama + "</td></tr>"),
				$("<tr><td class='label'>Warna</td><td class='text-center' width=30px>:</td><td>" + data.warna_nama + "</td></tr>"),
			),
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label'>No DH</td><td class='text-center' width=30px>:</td><td>" + data.kend_beli_dh + "</td></tr>"),
				$("<tr><td class='label'>No Rangka</td><td class='text-center' width=30px>:</td><td>" + data.kend_beli_rangka + "</td></tr>"),
				$("<tr><td class='label'>No Mesin</td><td class='text-center' width=30px>:</td><td>" + data.kend_beli_mesin + "</td></tr>"),
				$("<tr><td class='label'>Warna</td><td class='text-center' width=30px>:</td><td>" + data.kend_beli_warna + "</td></tr>"),
			)
		);
	}

	var showPembayaran = function(data, bayar){
		var detail_bayar="<table class='table-field table-bordered'><thead><tr><th>Tanggal</th><th>Keterangan</th><th>Jumlah (RP)</th></tr></thead><tbody>";
		var no = 1;
		var total_bayar = 0;
		var total_harga = data.spk_harga;
		var potongan = 0;

		if (data.spk_diskon_status==1){
			potongan = data.spk_diskon_cashback;
			total_harga = data.spk_harga - data.spk_diskon_cashback;
		}

		$.each(bayar,function(id, item){
			detail_bayar+="<tr><td>"+date_format(item.spk_bayar_tgl)+"</td><td>"+ item.spk_bayar_ket +"</td><td class='text-right'> " + number_format(item.spk_bayar_jumlah) +"</td></tr>";
			total_bayar += item.spk_bayar_jumlah;
			no++;
		});

		detail_bayar+="</tbody></table>";

		return  $("<div />").addClass("row m-0 p-10").append(
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label'>Metode Pembayaran</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pembayaran==0?"TUNAI":"KREDIT") + "</td></tr>"),
				$("<tr><td class='label'>Keterangan Harga</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pembayaran==0?"ON THE ROAD":"OFF THE ROAD") + "</td></tr>"),
				$("<tr><td class='label'>Harga (1)</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(data.spk_harga) + "</td></tr>"),
				$("<tr><td class='label'>Potongan Harga (2)</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(potongan) + "</td></tr>"),
				$("<tr><td class='label'>Total Harga (1-2)</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(total_harga) + "</td></tr>"),
			),
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label' colspan=3>Riwayat Pembayaran:</td></tr>"),
				$("<tr><td colspan=3>" + detail_bayar + "</td></tr>"),
				$("<tr><td class='label'>Total Bayar (3)</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(total_bayar) + "</td></tr>"),
				$("<tr><td class='label'>Kurang Bayar (1-2-3)</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(total_harga - total_bayar) + "</td></tr>"),
			)
		);
	}

	var showInfo = function(id, data) {
		var aksesoris="";
		popup =  $("#x-detail").dxPopup({
			showTitle: true,
			title:"Detail SPK "+id,
			width:800,
			height:380,
			visible: false,
			dragEnabled: true,
			shadingColor: "rgba(0,0,0,0.18)",
			closeOnOutsideClick: true,

			contentTemplate: function() {
				var template =  $("<div />");
				template.append("<div class='tabs'></div>");
				var tabcontainer =  $("<div />").addClass("tabs-container");
				$.each(tabItem,function(i,item){
				tabcontainer.append("<div id='"+ item.text +"' class='tabs-item'></div>")
				});
				template.append(tabcontainer);

				return template;
			},

			onShowing: function(e){
				$("#Pelanggan").html(showPelanggan(data)).addClass("show");
				$("#STNK").html(showSTNK(data));
				$("#Kendaraan").html(showKendaraan(data));

				$.ajax({
					url: base_url + "/api/penjualan/bayar/" + data.spk_id,
					dataType:"json",
					success: function(result) {
						$("#Pembayaran").html(showPembayaran(data, result));
					},error: function(e) {
						$("#Pembayaran").html(showPembayaran(data, ""));
					},
					timeout: 10000
				});

				$(".dx-popup-content").addClass("p-0");

				var tabsInstance = $(".tabs").dxTabs({
					dataSource: tabItem,
					selectedIndex: 0,
					onItemClick: function(e) {
						$(".tabs-item").removeClass("show");
						$("#"+e.itemData.text).addClass("show");
					}
				}).dxTabs("instance");
			}
		}).dxPopup("instance");

	popup.show();
	};
