	var popup=null;
	var dataGrid=null;
	var currentData=null;
	var tabItem = [
		{ index:0, text: "Diskon" }
	];

	var showDiskon = function(data, aksesoris) {
		var detail_aksesoris = "";
		var no = 1;

		$.each(aksesoris,function(id, item){
			detail_aksesoris+="<tr><td class='label'> &nbsp;&nbsp;"+no+". "+item.spk_aksesoris_nama+"</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(item.spk_aksesoris_harga) +"</td></tr>";
			no++;
		});

		return $("<div />").addClass("row m-0 p-10").append(
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label'>Tanggal Pengajuan</td><td class='text-center' width=30px>:</td><td>" + datetime_format(data.spk_diskon_tgl) + "</td></tr>"),
				$("<tr><td class='label'>Cashback</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(data.spk_diskon_cashback) + "</td></tr>"),
				$("<tr><td class='label' colspan=3>Aksesoris:</td></tr>"),
				$(detail_aksesoris),
				$("<tr><td class='label'>Total Aksesoris</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(data.total_aksesoris) + "</td></tr>"),
				$("<tr><td class='label'>Total Diskon</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(data.total_diskon) + "</td></tr>"),
				$("<tr><td class='label'>Catatan</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_ket + "</td></tr>"),
			),
			$("<table class='col-6 table-field'/>").append(
				$("<tr><td class='label' colspan=3>Referral:</td></tr>"),
				$("<tr><td class='label'>Komisi</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(data.spk_diskon_komisi) + "</td></tr>"),
				$("<tr><td class='label'>Nama Lengkap</td><td class='text-center' width=30px>:</td><td> " + data.spk_diskon_nama + "</td></tr>"),
				$("<tr><td class='label'>Alamat</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_alamat + "</td></tr>"),
				$("<tr><td class='label'>No Telepon</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_telp + "</td></tr>"),
				$("<tr><td class='label'>Hubungan</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_hubungan + "</td></tr>"),
				$("<tr><td class='label'>Bank</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_bank + "</td></tr>"),
				$("<tr><td class='label'>No Rekening</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_rek + "</td></tr>"),
				$("<tr><td class='label'>Atas Nama</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_an + "</td></tr>"),
				$("<tr><td class='label'>APPROVAL SPV</td><td class='text-center' width=30px>:</td><td>" + (data.spk_diskon_spv==1?"<i class='icon-terima s7-check-circle'></i>":"") + "</td></tr>"),
				$("<tr><td class='label'>APPROVAL KACAB</td><td class='text-center' width=30px>:</td><td>" + (data.spk_diskon_status==1?"<i class='icon-terima s7-check-circle'></i>":"") + "</td></tr>"),
			)
		);
	}

	var showInfo = function(id, data) {
		var aksesoris = "";
		popup =  $("#x-detail").dxPopup({
			showTitle: true,
			title:id,
			width:800,
			height:380,
			visible: false,
			dragEnabled: true,
			shadingColor: "rgba(0,0,0,0.18)",
			closeOnOutsideClick: true,

			contentTemplate: function() {
				var template =  $("<div />");
				template.append("<div class='tabs'></div>");
				var tabcontainer =  $("<div />").addClass("tabs-container");
				$.each(tabItem,function(i,item){
					tabcontainer.append("<div id='"+ item.text +"' class='tabs-item'></div>")
				});
				template.append(tabcontainer);

				return template;
			},

			onShowing: function(e){
				$.ajax({
					url: base_url + "api/penjualan/aksesoris/" + data.spk_id,
					dataType:"json",
					success: function(result) {
						$("#Diskon").html(showDiskon(data, result)).addClass("show");;
					},error: function(e) {
						$("#Diskon").html(showDiskon(data, "")).addClass("show");;
					},
					timeout: 10000
				});

				$(".dx-popup-content").addClass("p-0");
				var tabsInstance = $(".tabs").dxTabs({
					dataSource: tabItem,
					selectedIndex: 0,
					onItemClick: function(e) {
						$(".tabs-item").removeClass("show");
						$("#"+e.itemData.text).addClass("show");
					}
				}).dxTabs("instance");
			}
		}).dxPopup("instance");
		popup.show();
	};
