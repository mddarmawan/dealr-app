
	var popup=null;
	var dataGrid=null;
	var currentData=null;
	var tabItem = [
			{ index:0, text: "Informasi" },
			{ index:1, text: "Pelanggan" },
			{ index:2, text: "STNK" },
			{ index:3, text: "Kendaraan" },
			{ index:4, text: "Diskon" },
			{ index:5, text: "Pembayaran" }
	];

	function konfirmasi(e){
			id = $(e).data("id");
			if($(e).attr("aria-label")==="Revisi"){
				catatan = $("#txtCatatan").val();
				if (catatan!=""){
					if (confirm("Anda yakin akan mengirim revisi SPK "+id+"?") == true) {
						$.ajax({
							url: base_url + "/api/penjualan/spk/revisi/" + id,
							method:"POST",
							dataType:"json",
							success: function(result) {
								if (result.success==1){
									revisi_spk(catatan,currentData);
									if (popup!=null && dataGrid!=null){
										popup.hide();
										dataGrid.refresh();
									}
								}else{
									alert("Revisi SPK gagal dikirim!");
								}
							},error: function(e) {
								alert("Revisi SPK gagal dikirim!");
							},
							timeout: 10000
						});
					}
				}else{
					alert("Catatan revisi tidak boleh kosong!");
				}
			}else if($(e).attr("aria-label")==="Terima"){
				automatch = 0;
				if($("#chkMatch").prop('checked')){
					automatch=1;
				}
				if (confirm("Anda yakin akan menerima SPK "+id+"?") == true) {
					$.ajax({
						url: base_url + "/api/penjualan/spk/terima/" + id,
						method:"POST",
						data:{spk_automatch:automatch},
						dataType:"json",
						success: function(result) {
							if (result.success==1){
								acc_spk(currentData);
								if (popup!=null && dataGrid!=null){
									popup.hide();
									dataGrid.refresh();
								}
							}else{
								alert("SPK gagal diterima!");
							}
						},error: function(e) {
							alert("SPK gagal diterima!");
						},
						timeout: 10000
					});
				}

			}
	}

	var showInformasi = function(data){
		currentData = data;
		action_confirm = "<div class='tabs-footer dx-toolbar dx-widget dx-visibility-change-handler dx-collection dx-popup-bottom'>" +
			(data.spk_spv==1?'<div class="dx-toolbar-items-container"><div class="dx-toolbar-before"><div class="dx-item dx-toolbar-item dx-toolbar-button"><div class="dx-item-content dx-toolbar-item-content"><div class="dx-button dx-button-normal dx-widget dx-button-has-text dx-button-green spk-terima" data-id='+ data.spk_id+' onclick="konfirmasi(this);" role="button" aria-label="Terima" tabindex="0"><div class="dx-button-content"><span class="dx-button-text">Terima</span></div></div></div></div></div><div class="dx-toolbar-after"><div class="dx-item dx-toolbar-item dx-toolbar-button"><div class="dx-item-content dx-toolbar-item-content"><div class="dx-button dx-button-normal dx-widget dx-button-has-text dx-button-yellow spk-revisi" data-id='+ data.spk_id+' onclick="konfirmasi(this);" role="button" aria-label="Revisi" tabindex="0"><div class="dx-button-content"><span class="dx-button-text">Revisi</span></div></div></div></div></div></div></div>':"") +
			"</div>";
		return  $("<div />").addClass("row m-0 p-10").append(
								$("<table class='col-6 table-field'/>").append(
                  $("<tr><td class='label'>Tanggal</td><td class='text-center' width=30px>:</td><td>" + datetime_format(data.spk_tgl) + "</td></tr>"),
                  $("<tr><td class='label'>Sales</td><td class='text-center' width=30px>:</td><td>" + data.karyawan_nama + "</td></tr>"),
                  $("<tr><td class='label'>Tim</td><td class='text-center' width=30px>:</td><td>" + data.team_nama + "</td></tr>"),
									$("<tr><td class='label' colspan=3>* Isi catatan untuk Revisi:</td></tr>"),
									$("<tr><td colspan=3><textarea id='txtCatatan' placeholder='Isi catatan untuk Revisi...' style='width:94%;margin-left:12px;height:90px'></textarea></td></tr>"),
									$('<label class="custom-control custom-checkbox mt-2"  style="margin-left:12px;"><input type="checkbox" id="chkMatch" class="custom-control-input"><span class="custom-control-indicator"></span><span class="custom-control-description">Auto Matching</span></label>'),
									$("<tr><td colspan=3>"+action_confirm+"</td></tr>")
								),
								$("<table class='col-6 table-field'/>").append(
                 	$("<tr><td class='label' colspan=3>Catatan:</td></tr>"),
                  $("<tr><td colspan=3>" + data.spk_ket + "</td></tr>"),
                  $("<tr><td class='label'>APPROVAL SPV</td><td class='text-center' width=30px>:</td><td>" + (data.spk_spv==1?"<i class='icon-terima s7-check-circle'></i>":"") + "</td></tr>"),
                  $("<tr><td class='label'>APPROVAL ADH</td><td class='text-center' width=30px>:</td><td>" + (data.spk_status==1?"<i class='icon-terima s7-check-circle'></i>":"") + "</td></tr>"),
								),
						);
	}

	var showPelanggan = function(data){
		return  $("<div />").addClass("row m-0 p-10").append(
								$("<table class='col-6 table-field'/>").append(
                  $("<tr><td class='label'>Kategori</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pel_kategori==0?"PRIBADI":"PERUSAHAAN") + "</td></tr>"),
                  $("<tr><td class='label'>Nama Pemesan</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_nama + "</td></tr>"),
                  $("<tr><td class='label'>No KTP/KIMS</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_identitas + "</td></tr>"),
                  $("<tr><td class='label'>Tanggal Lahir</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pel_lahir!=""?date_format(data.spk_pel_lahir):"") + "</td></tr>"),
                  $("<tr><td class='label'>Alamat</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_alamat + "</td></tr>"),
                  $("<tr><td class='label'>Provinsi</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_provinsi + "</td></tr>"),
                  $("<tr><td class='label'>Kota</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_kota + "</td></tr>"),
                  $("<tr><td class='label'>Kecamatan</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_kecamatan + "</td></tr>"),
                  $("<tr><td class='label'>Kelurahan</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_kelurahan + "</td></tr>"),
                  $("<tr><td class='label'>Kode Pos</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_pos + "</td></tr>"),
								),
								$("<table class='col-6 table-field'/>").append(
                  $("<tr><td class='label'>No Telepon</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_telp + "</td></tr>"),
                  $("<tr><td class='label'>No HP</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_ponsel + "</td></tr>"),
                  $("<tr><td class='label'>Email</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_email + "</td></tr>"),
                  $("<tr><td class='label'>NPWP</td><td class='text-center' width=30px>:</td><td>" + data.spk_pel_npwp + "</td></tr>"),
                  $("<tr><td class='label'>Faktur Pajak</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pajak==0?"TIDAK DIMINTA":"DIMINTA") + "</td></tr>"),
                  $("<tr><td class='label' colspan=3>Nama dan Jabatan Contact Person Corporate/Fleet:</td></tr>"),
                  $("<tr><td colspan=3>" + data.spk_fleet + "</td></tr>"),
                  $("<tr><td colspan=3><img class src='"+base_url+"/public/data/identitas/" + data.spk_pel_fotoid + "'/></td></tr>"),
								)
						);
	}
	var showSTNK = function(data){
		return  $("<div />").addClass("row m-0 p-10").append(
								$("<table class='col-6 table-field'/>").append(
                  $("<tr><td class='label'>STNK a.n</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_nama + "</td></tr>"),
                  $("<tr><td class='label'>No KTP/KIMS</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_identitas + "</td></tr>"),
                  $("<tr><td class='label'>Alamat</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_alamat + "</td></tr>"),
                  $("<tr><td class='label'>Provinsi</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_provinsi + "</td></tr>"),
                  $("<tr><td class='label'>Kota</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kota + "</td></tr>"),
                  $("<tr><td class='label'>Kecamatan</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kecamatan + "</td></tr>"),
                  $("<tr><td class='label'>Kelurahan</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kelurahan + "</td></tr>"),
                  $("<tr><td class='label'>Kode Pos</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_pos + "</td></tr>"),
	                $("<tr><td class='label'>No Telepon</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_telp + "</td></tr>"),
	                $("<tr><td class='label'>No HP</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_ponsel + "</td></tr>"),
	                $("<tr><td class='label'>Email</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_email + "</td></tr>"),
								),
								$("<table class='col-6 table-field'/>").append(
									$("<tr><td class='label'>Alamat Domisili</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_alamatd + "</td></tr>"),
									$("<tr><td class='label'>Provinsi Domisilii</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_provinsid + "</td></tr>"),
									$("<tr><td class='label'>Kota Domisili</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kotad + "</td></tr>"),
									$("<tr><td class='label'>Kecamatan Domisili</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kecamatand + "</td></tr>"),
									$("<tr><td class='label'>Kelurahan Domisili</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_kelurahand + "</td></tr>"),
									$("<tr><td class='label'>Kode Pos Domisili</td><td class='text-center' width=30px>:</td><td>" + data.spk_stnk_posd + "</td></tr>"),
                  $("<tr><td colspan=3><img class src='"+base_url+"/public/data/identitas/" + data.spk_pel_fotoid + "'/></td></tr>"),
								)
						);
		}
		var showKendaraan = function(data){
			return  $("<div />").addClass("row m-0 p-10").append(
									$("<table class='col-6 table-field'/>").append(
	                  $("<tr><td class='label'>Variant</td><td class='text-center' width=30px>:</td><td>" + data.variant_nama + "</td></tr>"),
	                  $("<tr><td class='label'>Warna</td><td class='text-center' width=30px>:</td><td>" + data.warna_nama + "</td></tr>"),
									),
									$("<table class='col-6 table-field'/>").append(
										$("<tr><td class='label'>No DH</td><td class='text-center' width=30px>:</td><td>" + data.kend_beli_dh + "</td></tr>"),
										$("<tr><td class='label'>No Rangka</td><td class='text-center' width=30px>:</td><td>" + data.kend_beli_rangka + "</td></tr>"),
										$("<tr><td class='label'>No Mesin</td><td class='text-center' width=30px>:</td><td>" + data.kend_beli_mesin + "</td></tr>"),
										$("<tr><td class='label'>Warna</td><td class='text-center' width=30px>:</td><td>" + data.kend_beli_warna + "</td></tr>"),
									)
							);
		}

		var showDiskon = function(data, aksesoris){
			var detail_aksesoris="";
			var no=1;
			$.each(aksesoris,function(id, item){
					detail_aksesoris+="<tr><td class='label'> &nbsp;&nbsp;"+no+". "+item.spk_aksesoris_nama+"</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(item.spk_aksesoris_harga) +"</td></tr>";
					no++;
			});
			return  $("<div />").addClass("row m-0 p-10").append(
									$("<table class='col-6 table-field'/>").append(
	                  $("<tr><td class='label'>Tanggal Pengajuan</td><td class='text-center' width=30px>:</td><td>" + datetime_format(data.spk_diskon_tgl) + "</td></tr>"),
	                  $("<tr><td class='label'>Cashback</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(data.spk_diskon_cashback) + "</td></tr>"),
	                  $("<tr><td class='label' colspan=3>Aksesoris:</td></tr>"),
										$(detail_aksesoris),
	                  $("<tr><td class='label'>Total Aksesoris</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(data.total_aksesoris) + "</td></tr>"),
	                  $("<tr><td class='label'>Total Diskon</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(data.total_diskon) + "</td></tr>"),
	                  $("<tr><td class='label'>Catatan</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_ket + "</td></tr>"),
									),
									$("<table class='col-6 table-field'/>").append(
										$("<tr><td class='label' colspan=3>Referral:</td></tr>"),
										$("<tr><td class='label'>Komisi</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(data.spk_diskon_komisi) + "</td></tr>"),
										$("<tr><td class='label'>Nama Lengkap</td><td class='text-center' width=30px>:</td><td> " + data.spk_diskon_nama + "</td></tr>"),
										$("<tr><td class='label'>Alamat</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_alamat + "</td></tr>"),
										$("<tr><td class='label'>No Telepon</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_telp + "</td></tr>"),
										$("<tr><td class='label'>Hubungan</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_hubungan + "</td></tr>"),
										$("<tr><td class='label'>Bank</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_bank + "</td></tr>"),
										$("<tr><td class='label'>No Rekening</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_rek + "</td></tr>"),
										$("<tr><td class='label'>Atas Nama</td><td class='text-center' width=30px>:</td><td>" + data.spk_diskon_an + "</td></tr>"),
										$("<tr><td class='label'>APPROVAL SPV</td><td class='text-center' width=30px>:</td><td>" + (data.spk_diskon_spv==1?"<i class='icon-terima s7-check-circle'></i>":"") + "</td></tr>"),
										$("<tr><td class='label'>APPROVAL KACAB</td><td class='text-center' width=30px>:</td><td>" + (data.spk_diskon_status==1?"<i class='icon-terima s7-check-circle'></i>":"") + "</td></tr>"),
									)
							);
		}

		var showPembayaran = function(data, bayar){
			var detail_bayar="<table class='table-field table-bordered'><thead><tr><th>Tanggal</th><th>Keterangan</th><th>Jumlah (RP)</th></tr></thead><tbody>";
			var no=1;
			var total_bayar=0;
			var total_harga=data.spk_harga;
			var potongan = 0;
			if (data.spk_diskon_status==1){
				potongan = data.spk_diskon_cashback;
				total_harga = data.spk_harga - data.spk_diskon_cashback;
			}
			$.each(bayar,function(id, item){
					detail_bayar+="<tr><td>"+date_format(item.spk_bayar_tgl)+"</td><td>"+ item.spk_bayar_ket +"</td><td class='text-right'> " + number_format(item.spk_bayar_jumlah) +"</td></tr>";
					total_bayar += item.spk_bayar_jumlah;
					no++;
			});
			detail_bayar+="</tbody></table>";
			return  $("<div />").addClass("row m-0 p-10").append(
									$("<table class='col-6 table-field'/>").append(
	                  $("<tr><td class='label'>Metode Pembayaran</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pembayaran==0?"TUNAI":"KREDIT") + "</td></tr>"),
	                  $("<tr><td class='label'>Keterangan Harga</td><td class='text-center' width=30px>:</td><td>" + (data.spk_pembayaran==0?"ON THE ROAD":"OFF THE ROAD") + "</td></tr>"),
										$("<tr><td class='label'>Harga (1)</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(data.spk_harga) + "</td></tr>"),
										$("<tr><td class='label'>Potongan Harga (2)</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(potongan) + "</td></tr>"),
										$("<tr><td class='label'>Total Harga (1-2)</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(total_harga) + "</td></tr>"),
									 ),
									$("<table class='col-6 table-field'/>").append(
										$("<tr><td class='label' colspan=3>Riwayat Pembayaran:</td></tr>"),
										$("<tr><td colspan=3>" + detail_bayar + "</td></tr>"),
										$("<tr><td class='label'>Total Bayar (3)</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(total_bayar) + "</td></tr>"),
										$("<tr><td class='label'>Kurang Bayar (1-2-3)</td><td class='text-center' width=30px>:</td><td class='text-right p-r-15'><span class='pull-left'>Rp</span> " + number_format(total_harga - total_bayar) + "</td></tr>"),
									)
							);
		}

		var showInfo = function(id, data) {
			var aksesoris="";
    popup =  $("#x-detail").dxPopup(
				{
						showTitle: true,
						title:id,
						width:800,
						height:380,
						visible: false,
						dragEnabled: true,
						shadingColor: "rgba(0,0,0,0.18)",
						closeOnOutsideClick: true,
						contentTemplate: function() {
							var template =  $("<div />");
							template.append("<div class='tabs'></div>");
							var tabcontainer =  $("<div />").addClass("tabs-container");
							$.each(tabItem,function(i,item){
								tabcontainer.append("<div id='"+ item.text +"' class='tabs-item'></div>")
							});
							template.append(tabcontainer);

							return template;
						},
						onShowing: function(e){
							$("#Informasi").html(showInformasi(data)).addClass("show");
							$("#Pelanggan").html(showPelanggan(data));
							$("#STNK").html(showSTNK(data));
							$("#Kendaraan").html(showKendaraan(data));
							$.ajax({
								url: base_url + "/api/penjualan/aksesoris/" + data.spk_id,
								dataType:"json",
								success: function(result) {
									$("#Diskon").html(showDiskon(data, result));
								},error: function(e) {
									$("#Diskon").html(showDiskon(data, ""));
								},
								timeout: 10000
							});
							$.ajax({
								url: base_url + "/api/penjualan/bayar/" + data.spk_id,
								dataType:"json",
								success: function(result) {
									$("#Pembayaran").html(showPembayaran(data, result));
								},error: function(e) {
									$("#Pembayaran").html(showPembayaran(data, ""));
								},
								timeout: 10000
							});
							$(".dx-popup-content").addClass("p-0");
							var tabsInstance = $(".tabs").dxTabs({
										dataSource: tabItem,
										selectedIndex: 0,
										onItemClick: function(e) {
											$(".tabs-item").removeClass("show");
											$("#"+e.itemData.text).addClass("show");
										}
								}).dxTabs("instance");
						}
				}
			).dxPopup("instance");
			popup.show();
};
