function formatDate(date) {
	var monthNames = [
	"Januari", "Februari", "Maret",
	"April", "Mei", "Juni", "Juli",
	"Agustus", "September", "Oktober",
	"November", "Desember"
	];

	var day = date.getDate();
	var monthIndex = date.getMonth();
	var year = date.getFullYear();

	return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

function printIframe(id, item) {
	var html = 'Proses Samsat.';

	$("#printf").contents().find("body").append(html);
	$("#printf").get(0).contentWindow.print();
	$("#printf").contents().find("body").html("");
}