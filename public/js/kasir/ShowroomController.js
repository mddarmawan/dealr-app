
	var popup = null;

	var customAdd = function() {
		popup =  $("#x-custom-add").dxPopup({
			title: "Pembayaran",
			maxWidth: "80%",
			showCloseButton: true,
			contentTemplate: function (contentElement) {
				contentElement.append(
					$('<div class="col-lg-12" style="padding-bottom:10px;border-bottom: 1px solid #ddd">').append(
						$('<table style="width:auto;margin:0;margin-bottom:10px;">').append(
							$('<tr>').append(
								$('<td style="padding-right:45px!important;">').append(
									$('<b>No. SPK: </b>')
								),
								$('<td style="padding-right:45px!important;">').append(
									$('<div class="input-group" style="margin:0; width: 284px; position: relative;">').append(
										$('<input type="text" id="spk_spk" class="input-filter uppercase" placeholder="Masukkan No. SPK/Nama Pemesan" value="" style="position: relative;">'),
										$('<span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>')
									)
								),
								$('<td><b>Total Invoice: </b></td>'),
								$('<td style="padding-right:15px!important;" id="total_invoice">Rp 0,-</td>'),
								$('<td><b>Total Bayar: </b></td>'),
								$('<td style="padding-right:15px!important" id="total_bayar">Rp 0,-</td>'),
								$('<td><b>Kurang Bayar: </b>'),
								$('<td style="padding-right:15px!important" id="total_kurang">Rp 0,-</td>')
							)
						)
					)
				);
			}
		}).dxPopup("instance");
		popup.show();

		$("#myTextBox").dxTextBox({
			placeholder: 'Type your text here'
		})
	};