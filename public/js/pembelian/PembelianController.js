	var importForm = function() {
		$('#import_beli').ajaxForm({
			beforeSend: function() {
				$("#import_beli_progress").html("0%");
				$("#import_beli_status").html("Mohon tunggu...");
			},

			uploadProgress: function(event, position, total, percentComplete) {
				$("#import_beli_progress").html(percentComplete + "%");
			},

			complete: function(xhr) {
				$("#import_beli_status").html(xhr.responseText);
			}
		});

		$('#import_onhand').ajaxForm({
			beforeSend: function() {
				$("#import_onhand_progress").html("0%");
				$("#import_onhand_status").html("Mohon tunggu...");
			},

			uploadProgress: function(event, position, total, percentComplete) {
				$("#import_onhand_progress").html(percentComplete + "%");
			},

			complete: function(xhr) {
				$("#import_onhand_status").html(xhr.responseText);
			}
		});
	}

	var popup = null;

	var showImport = function() {
		popup =  $("#x-import").dxPopup({
			width: 550,
			height: 300,
			title: "Import File",
			maxWidth: "50%",
			showCloseButton: true,
			contentTemplate: function (contentElement) {
				contentElement.append(
					$('<div style="padding: 15px;"></div>').addClass('row').append(
						$('<div style="padding: 5px;"></div>').addClass('col-lg-6').append(
							$('<h3></h3>').text('Pembelian File'),
							$('<form id="import_beli" action="/api/pembelian/import/beli" method="post" enctype="multipart/form-data"></form>').append(
									$('<div></div>').addClass('form-group').append(
										$('<input id="import_beli_file" type="file" name="import_beli_file"/>'),
									),
									$('<button type="submit" class="btn btn-primary">Submit</button>'),
								),
							$('<br>'),
							$('<h3>Progress: <span class="label label-primary" id="import_beli_progress">-</span></h3>'),
							$('<h3>Status: <span class="label label-primary" id="import_beli_status">-</span></h3>'),
						),
						$('<div style="padding: 5px;"></div>').addClass('col-lg-6').append(
							$('<h3></h3>').text('Stock On Hand File'),
							$('<form id="import_onhand" action="/api/pembelian/import/onhand" method="post" enctype="multipart/form-data"></form>').append(
									$('<div></div>').addClass('form-group').append(
										$('<input id="import_onhand_file" type="file" name="import_onhand_file"/>'),
									),
									$('<button type="submit" class="btn btn-primary">Submit</button>'),
								),
							$('<br>'),
							$('<h3>Progress: <span class="label label-primary" id="import_onhand_progress">-</span></h3>'),
							$('<h3>Status: <span class="label label-primary" id="import_onhand_status">-</span></h3>'),
						),
					),
				);
			}
		}).dxPopup("instance");
		popup.show();
		importForm();
	};
