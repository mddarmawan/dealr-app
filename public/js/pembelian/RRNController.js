	var importForm = function() {
		$('#import_rrn').ajaxForm({
			beforeSend: function() {
				$("#import_rrn_progress").html("0%");
				$("#import_rrn_status").html("Mohon tunggu...");
			},

			uploadProgress: function(event, position, total, percentComplete) {
				$("#import_rrn_progress").html(percentComplete + "%");
			},

			complete: function(xhr) {
				$("#import_rrn_status").html(xhr.responseText);
			}
		});
	}

	var popup = null;

	var showImport = function() {
		popup =  $("#x-import").dxPopup({
			width: 350,
			height: 300,
			title: "Import File",
			maxWidth: "50%",
			showCloseButton: true,
			contentTemplate: function (contentElement) {
				contentElement.append(
					$('<div style="padding: 5px;"></div>').addClass('col-lg-12').append(
						$('<h3></h3>').text('Import RRN File'),
						$('<form id="import_rrn" action="/api/pembelian/import/rrn" method="post" enctype="multipart/form-data"></form>').append(
								$('<div></div>').addClass('form-group').append(
									$('<input id="import_rrn_file" type="file" name="import_rrn_file"/>'),
								),
								$('<button type="submit" class="btn btn-primary">Submit</button>'),
							),
						$('<br>'),
						$('<h3>Progress: <span class="label label-primary" id="import_rrn_progress">-</span></h3>'),
						$('<h3>Status: <span class="label label-primary" id="import_rrn_status">-</span></h3>'),
					),
				);
			}
		}).dxPopup("instance");
		popup.show();
		importForm();
	};
