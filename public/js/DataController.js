	var base_url = "http://127.0.0.1:8000/";

	function getData(dataURL, id){		
		return new DevExpress.data.CustomStore({
			loadMode: "process",
			key:id,

			load: function (loadOptions) {
				var deferred = $.Deferred();
				
				$.ajax({
					url: dataURL,
					success: function(result) {
						deferred.resolve(result);
					},
					error: function(e) {
						deferred.reject("Data Loading Error");
					},
					timeout: 10000
				});
		
				return deferred.promise();
			},

			insert: function (values) {
				var deferred = $.Deferred();
				$.post(dataURL, {data:JSON.stringify(values)}).done(function(data) {
					deferred.resolve(data.key);
				})
				return deferred.promise();
			},

			update: function (key, values){
				var deferred = $.Deferred();
				$.ajax({
					url: dataURL + key,
					method: "POST",
					dataType: "json",
					data: {data:JSON.stringify(values)}
				}).done(function(){
					deferred.resolve(key);
				});
				return deferred.promise();
			},

			remove: function (key) {
				var deferred = $.Deferred();
				$.ajax({
					url: dataURL + "delete/" + key,
					method: "POST",
				}).done(function(result){
					if (result) {
						deferred.resolve(key);
					} else {
						deferred.resolve(key);
						alert("Maaf, tidak dapat menghapus.");
					}
				});
				return deferred.promise();
			}
		});
	}