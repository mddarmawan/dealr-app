function moveEditColumnToLeft(dataGrid) {
	dataGrid.columnOption("command:edit", {
		visibleIndex: -1,
		width: 80
	});
}

$.when(
    $.getJSON("assets/lib/cldr/main/id/numbers.json"),
    $.getJSON("assets/lib/cldr/main/id/currencies.json"),
    //$.getJSON("assets/lib/cldr/supplemental/likelySubtags.json"),
    //$.getJSON("assets/lib/cldr/supplemental/timeData.json"),
    //$.getJSON("assets/lib/cldr/supplemental/weekData.json"),
    //$.getJSON("assets/lib/cldr/supplemental/currencyData.json"),
    //$.getJSON("assets/lib/cldr/supplemental/numberingSystems.json")
).then(function () {
    //The following code converts the got results into an array
    return [].slice.apply( arguments, [0] ).map(function( result ) {
        return result[ 0 ];
    });
}).then(
    Globalize.load //loads data held in each array item to Globalize
).then(function(){
	DevExpress.config({defaultCurrency: 'IDR', decimalSeparator:",",thousandsSeparator:".", forceIsoDateParsing:true });
});