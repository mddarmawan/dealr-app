function getDateTime(){
		var date = new Date();
	 var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

	 return str;
}
function getDate(){
		var date = new Date();
	 var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();

	 return str;
}
function date_format(mdate){
	var d = new Date(mdate);
	var day = d.getDate();
	var month = d.getMonth();
	var year = d.getFullYear();

	if (day <10){
		day = "0" + day;
	}

	if (month <10){
		month = "0" + month;
	}

	return day + "/" + month + "/" + year;
}

function number_format(x) {
	if (isNaN(x)){
		return 0;
	}
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}
function string_format(x) {
	return parseInt(x.toString().replace(/\./g, ''));
}
function toInt(x){
	x = string_format(x);
	if (isNaN(x)){
		return 0;
	}
	return x;
}
function selected(param1, param2){
	if (param1==param2){
		return ' selected ';
	}
	return '';
}
function checked(param1, param2){
	if (param1==param2){
		return ' checked ';
	}
	return '';
}
function date_format(x){
	var y = x.substr(0,4);
	var m = x.substr(5,2);
	var d = x.substr(8,2);

	return d+"/"+m+"/"+y;
}
function datetime_format(x){
	var y = x.substr(0,4);
	var m = x.substr(5,2);
	var d = x.substr(8,2);
	var t = x.substr(11,8);

	return d+"/"+m+"/"+y+" "+t;
}
