<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class WebApp
{
    private function get_modul(){			
		$parent = DB::table("tb_modul")->where("modul_status","1")->where("modul_parent","0")->orderBy("modul_order")->get();
		
		return $parent;
	}
	
    public function handle($request, Closure $next)
    {		
        view()->share('modul', $this->get_modul());
        return $next($request);
    }
}