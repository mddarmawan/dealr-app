<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class PenggunaController extends Controller
{

      public function safa()
      {
        return view('modules.pengaturan.safa');
      }
      public function salmon()
      {
        return view('modules.pengaturan.salmon');
      }
}
