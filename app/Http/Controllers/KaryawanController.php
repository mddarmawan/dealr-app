<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class KaryawanController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('modules.karyawan.data');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$karwayan = DB::table("tb_karyawan")->orderBy("karwayan_id", "desc")->first();
		$id = 'K' . sprintf("%03s", substr($karyawan->karyawan_id, 1) + 1);

		$params = array(
			"karyawan_id"			=> $id,
			"karyawan_jabatan"		=> $request->karyawan_jabatan,
			"karyawan_jenis"		=> $request->karyawan_jenis,
			"karyawan_identitas"	=> $request->karyawan_identitas,
			"karyawan_nama"			=> $request->karyawan_nama,
			"karyawan_lahir"		=> $request->karyawan_lahir,
			"karyawan_alamat"		=> $request->karyawan_alamat,
			"karyawan_telp"			=> $request->karyawan_telp,
			"karyawan_email"		=> $request->karyawan_email,
			"karyawan_bank"			=> NULL,
			"karyawan_rek"			=> NULL,
			"karyawan_npwp"			=> NULL,
		);

		$data = DB::table("tb_karyawan")->insert($params);

		if ($data) {
			return redirect('/karyawan')->with('message', 'Berhasil menambah.');
		} else {
			return redirect('/karyawan')->with('message', 'Gagal menambah.');
		}
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$id = $request->karyawan_id;

		$params = array(
			"karyawan_jabatan"		=> $request->karyawan_jabatan,
			"karyawan_jenis"		=> $request->karyawan_jenis,
			"karyawan_identitas"	=> $request->karyawan_identitas,
			"karyawan_nama"			=> $request->karyawan_nama,
			"karyawan_lahir"		=> $request->karyawan_lahir,
			"karyawan_alamat"		=> $request->karyawan_alamat,
			"karyawan_telp"			=> $request->karyawan_telp,
			"karyawan_email"		=> $request->karyawan_email,
			"karyawan_bank"			=> NULL,
			"karyawan_rek"			=> NULL,
			"karyawan_npwp"			=> NULL,
		);

		$data = DB::table("tb_karyawan")->where("karyawan_id", $id)->update($params);

		return redirect('/karyawan')->with('message', 'Berhasil mengedit.');
	}

	/**
	 * Patch the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function patch(Request $request, $id)
	{
		switch ($request->action) {
			case 'Trash':
				$params = array(
					"karyawan_hapus"	=> 1,
				);
				break;

			case 'Recover':
				$params = array(
					"karyawan_hapus"	=> 0,
				);
				break;

			case 'Activate':
				$params = array(
					"karyawan_status"	=> 1,
				);
				break;

			case 'Inactivate':
				$params = array(
					"karyawan_status"	=> 0,
				);
				break;
			
			default:
				$params = array();
				break;
		}

		$data = DB::table("tb_karyawan")->where("karyawan_id", $id)->update($params);

		if ($data) {
			return redirect('/karyawan')->with('message', 'Berhasil mengubah status data.');
		} else {
			return redirect('/karyawan')->with('message', 'Gagal mengubah status data.');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$data = DB::table("tb_karyawan")->where("karyawan_id", $id)->delete();

		if ($data) {
			return redirect('/karyawan')->with('message', 'Berhasil menghapus.');
		} else {
			return redirect('/karyawan')->with('message', 'Gagal menghapus.');
		}
	}
}
