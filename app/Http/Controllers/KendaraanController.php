<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class KendaraanController extends Controller
{
    public function tipe()
    {
      return view('modules.pengaturan.kendaraantipe');
    }

     public function variant()
     {
       $data['tipe'] = (new Api\ApiKendaraanTypeController)->data();
       return view('modules.pengaturan.kendaraanvariant', compact("data"));
     }

      public function harga()
      {
        $data['tipe'] = (new Api\ApiKendaraanTypeController)->data();
        return view('modules.pengaturan.kendaraanharga', compact("data"));
      }


      public function warna()
      {
        $data['warna'] = (new Api\ApiWarnaController)->data();
        $data['tipe'] = (new Api\ApiKendaraanTypeController)->data();
        return view('modules.pengaturan.kendaraanwarna', compact("data"));
      }

      public function aksesoris()
      {
        $data['type'] = (new Api\ApiKendaraanTypeController)->data();
        $data['vendor'] = (new Api\ApiKontakController)->order("tipe",2);
        return view('modules.pengaturan.aksesoris', compact("data"));
      }
}
