<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class LeasingController extends Controller
{
	function index(){
		$data = DB::table('tb_leasing')
				->where('leasing_status', 1)
				->where("leasing_hapus", 0)
				->get();

			$result = array();
			foreach($data as $r){
			$item = array();
			$item['leasing_nama'] = $r->leasing_nama;
			$item['leasing_nick'] = $r->leasing_nick;
			$item['leasing_alamat'] = $r->leasing_alamat;
			$item['leasing_kota'] = $r->leasing_kota;
			$item['leasing_telp'] = $r->leasing_telp;
			$item['edit'] = "
				<form action='/leasing/".$r->leasing_id."' method='post'>
					<input type='hidden' name='_token' value='".csrf_token()."'>
					<input type='hidden' name='_method' value='delete'>
					<input type='submit' name='submit' value='submit' style='display: none;'>
					<a href='#' class='orange-text' onclick='javascript:edit(".$r->leasing_id.");'>
						<span class='s7-pen'></span> Update
					</a> ||
					<span class='red-text s7-trash'></span>
					<input style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='red-text' onclick='return confirm(\"Anda yakin ingin menghapus?\"); this.submit();' value='Delete'>
						
					</input>
				</form> ";

			if((!request("leasing_nama") || strrpos(strtolower($item['leasing_nama']), strtolower(request("leasing_nama"))) > -1) &&
				(!request("leasing_nick") || strrpos(strtolower($item['leasing_nick']), strtolower(request("leasing_nick"))) > -1) &&
				(!request("leasing_alamat") || strrpos(strtolower($item['leasing_alamat']), strtolower(request("leasing_alamat"))) > -1) &&
				(!request("leasing_kota") || strrpos(strtolower($item['leasing_kota']), strtolower(request("leasing_kota"))) > -1) &&
				(!request("leasing_telp") || strrpos(strtolower($item['leasing_telp']), strtolower(request("leasing_telp"))) > -1))  
			array_push($result, $item);
		 }

		return json_encode($result);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id){
		$data = DB::table('tb_leasing')
				->where('leasing_status', 1)
				->where("leasing_hapus", 0)
				->where("leasing_id", $id)
				->first();

		unset($data->created_at);
		unset($data->updated_at);

		return json_encode($data);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$params = array(
    		"leasing_nama" => $request->leasing_nama,
    		"leasing_nick" => $request->leasing_nick,
    		"leasing_alamat" => $request->leasing_alamat,
    		"leasing_kota" => $request->leasing_kota,
    		"leasing_telp" => $request->leasing_telp
    	);

    	$data = DB::table("tb_leasing")->insert($params);

		if ($data) {
			return redirect('/leasing')->with('message', 'Berhasil menambah.');
		} else {
			return redirect('/leasing')->with('message', 'Gagal menambah.');
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	// $id = $request->leasing_id;
    	$params = array(
    		"leasing_id" => $request->leasing_id,
    		"leasing_nama" => $request->leasing_nama,
    		"leasing_nick" => $request->leasing_nick,
    		"leasing_alamat" => $request->leasing_alamat,
    		"leasing_kota" => $request->leasing_kota,
    		"leasing_telp" => $request->leasing_telp
    	);

    	$data = DB::table("tb_leasing")->where("leasing_id", $id)->update($params);

		return redirect('/leasing')->with('message', 'Berhasil mengedit.');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$data = DB::table("tb_leasing")->where("leasing_id", $id)->delete();

		if ($data) {
			return redirect('/leasing')->with('message', 'Berhasil menghapus.');
		} else {
			return redirect('/leasing')->with('message', 'Gagal menghapus.');
		}
	}
}