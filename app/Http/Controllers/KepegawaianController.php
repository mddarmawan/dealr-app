<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class KepegawaianController extends Controller
{
    public function jabatan()
    {
      return view('modules.pengaturan.jabatan');
    }

     public function grade()
     {
       return view('modules.pengaturan.grade');
     }


      public function team()
      {
        $data['kota'] = DB::table("tb_kota")->get();
        $data['karyawan'] = DB::table("tb_karyawan")
            ->join("tb_jabatan","karyawan_jabatan","=","jabatan_id")
            ->whereRaw("jabatan_nama NOT LIKE '%sales%'")
            ->where("karyawan_hapus","0")
            ->get();
        return view('modules.pengaturan.team', compact("data"));
      }

      public function karyawan()
      {
        $data['bank'] = DB::table("tb_bank_kategori")->get();
        $data['jabatan'] = DB::table("tb_jabatan")->where("jabatan_status","=","1")->get();
        return view('modules.pengaturan.karyawan', compact("data"));
      }

      public function salesman()
      {
        $data['team'] = DB::table("tb_team")->get();
        $data['grade'] = DB::table("tb_grade")->get();
        return view('modules.pengaturan.salesman', compact("data"));
      }
}
