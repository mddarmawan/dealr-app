<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Api\ApiKontakKategoriController;

class UmumController extends Controller
{

	public function kontak()
	{
		$tipe = array();
		foreach(json_decode((new Api\ApiKontakTipeController)->data()) as $d){
			$item['id'] = $d->kontak_tipe_id;
			$item['text'] = $d->kontak_tipe_nama;
			array_push($tipe,$item);
		};
		$data['tipe'] = (new Api\ApiKontakTipeController)->data();
		$data['kategori'] = (new Api\ApiKontakKategoriController)->data();
		return view('modules.pengaturan.kontak', compact("data"));
	}

	public function tipe()
	{
		return view('modules.pengaturan.kontaktipe');
	}

	public function klasifikasi()
	{
		return view('modules.pengaturan.kontakklasifikasi');
	}

	public function warna()
    {
      return view('modules.pengaturan.warna');
    }

	public function gudang()
	{
		$data['kota'] = DB::table("tb_kota")->get();
		return view('modules.pengaturan.gudang', compact("data"));
	}

	public function bank()
	{
	  return view('modules.pengaturan.bankkategori');
	}

	public function nospk()
	{
		$data['sales'] = (new Api\ApiSalesmanController)->data();
		$data['spk'] = (new Api\Penjualan\ApiSPKController)->filter("SALES");
		$data['team'] = DB::table("tb_team")->get();

		return view('modules.umum.nospk', compact('data'));
	}

	public function pelanggan()
	{
		$data['sales'] = (new Api\ApiSalesmanController)->data();
		$data['spk'] = (new Api\Penjualan\ApiSPKController)->filter("SALES");
		$data['team'] = DB::table("tb_team")->get();

		return view('modules.umum.pelanggan', compact('data'));
	}
}