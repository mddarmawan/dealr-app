<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	private function get_modul(){
		$modul = array();
		
		$parent = DB::table("tb_modul")->where("modul_status","1")->where("modul_parent","0")->orderBy("modul_order")->get();
		foreach ($parent as $p){
			$parent_item['modul_nama'] = $p->modul_nama;
			$parent_item['modul_link'] = $p->modul_link;
			
			$parent_sub = DB::table("tb_modul")->where("modul_status","1")->where("modul_parent",$p->modul_id)->orderBy("modul_order")->get();
			$sub=array();
			foreach($parent_sub as $s){
				$child_item['modul_nama'] = $s->modul_nama;
				$child_item['modul_link'] = $s->modul_link;
				
				$sub_child = DB::table("tb_modul")->where("modul_status","1")->where("modul_parent",$s->modul_id)->orderBy("modul_order")->get();
				$child = array();
				foreach($sub_child as $c){
					$item['modul_nama'] = $c->modul_nama;
					$item['modul_link'] = $c->modul_link;
					
					$item['modul_item'] = DB::table("tb_modul")->where("modul_status","1")->where("modul_parent",$c->modul_id)->orderBy("modul_order")->get();
					array_push($child,$item);
				}
				$child_item['modul_child'] = $child;
				array_push($sub, $child_item);
			}
			$parent_item['modul_sub'] = $sub;
			array_push($modul,$parent_item);
		}
		return $modul;
	}
	public function __construct()
    {	
		$this->middleware(function ($request, $next) {
				view()->share('modul', $this->get_modul());

				return $next($request);
		});
	}
}
