<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Api\ApiKontakKategoriController;

class KasirController extends Controller
{
	public function showroom()
	{
		$data['spk'] = (new Api\Penjualan\ApiSPKController)->data();
		$data['pelanggan'] = (new Api\Umum\ApiPelangganController)->data();
		$data['type'] = (new Api\ApiKendaraanTypeController)->data();
		$data['variant'] = (new Api\ApiKendaraanVariantController)->data();
		$data['warna'] = (new Api\ApiWarnaController)->data();
		$data['sales'] = (new Api\ApiSalesmanController)->data();
		$data['team'] = (new Api\ApiTeamController)->data();
		$data['akun'] = DB::table("tb_bank")->get();

		return view('modules.kasir.showroom', compact('data'));
	}

	public function operasional()
	{
		$data['spk'] = (new Api\Penjualan\ApiSPKController)->data();
		$data['pelanggan'] = (new Api\Umum\ApiPelangganController)->data();
		$data['type'] = (new Api\ApiKendaraanTypeController)->data();
		$data['variant'] = (new Api\ApiKendaraanVariantController)->data();
		$data['warna'] = (new Api\ApiWarnaController)->data();
		$data['sales'] = (new Api\ApiSalesmanController)->data();
		$data['team'] = (new Api\ApiTeamController)->data();
		$data['akun'] = DB::table("tb_bank")->get();

		return view('modules.kasir.operasional', compact('data'));
	}
}