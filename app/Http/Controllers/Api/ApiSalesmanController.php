<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiSalesmanController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_sales";
		$this->column = "sales";
	}

	public function show($id)
	{
		$this->editing = TRUE;

		$where = array(
			$this->column . "_kode" => $id
		);

		return $this->data($where);
	}

	public function inactive()
	{
		$this->inactive = TRUE;

		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	public function deleted()
	{
		$this->deleted = TRUE;

		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	public function data($where = NULL)
	{
		$result = array();

		if (empty($where)) {
			$where = array();
		}



		if (!isset($where[$this->column . "_hapus"])) {
			$where["karyawan_hapus"] = "0";
		}

		$data = DB::table("tb_karyawan")
      ->join("tb_jabatan","jabatan_id","=", "karyawan_jabatan")
			->leftjoin($this->table, "sales_karyawan","=", "karyawan_id")
      ->whereRaw("jabatan_nama LIKE '%sales%'")
			->where($where)
			->get();

		return json_encode($data);
	}

   protected function set(Request $request, $id)
   {
  	 $params = json_decode($request->data, TRUE);


     $check = DB::table($this->table)->where($this->column."_karyawan", $id)->count();
     if ($check>0){
       //update
       	$proses = DB::table($this->table)->where($this->column."_karyawan", $id)->update($params);
     }else{
        $params['sales_karyawan'] = $id;
     	 $proses = DB::table($this->table)->insert($params);
     }


  	 if ($proses) {
  			return response()->json(array("success"=>1), 200);
  		} else {
  			return response()->json(array("success"=>0), 200);
  		}
   }

}
