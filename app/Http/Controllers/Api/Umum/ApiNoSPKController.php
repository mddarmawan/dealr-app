<?php

namespace App\Http\Controllers\Api\Umum;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiNoSPKController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_sales_spk";
		$this->column = "sales_spk";
		$this->id = "sales_spk_id";
	}

	public function data()
	{
		$data = DB::table("tb_sales_spk")
			->join("tb_sales", "tb_sales.sales_uid", "=", "tb_sales_spk.sales_spk_sales")
			->join("tb_team", "tb_team.team_id", "=", "tb_sales.sales_team")
			->join("tb_karyawan", "tb_karyawan.karyawan_id", "=", "tb_sales.sales_karyawan");
		
		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}

	public function store(Request $request = NULL, $import = NULL)
	{
		$result['success'] = 0;
		$params = json_decode($request->data, TRUE);

		$spk = array(
			(!empty($params["sales_spk_id_0"]) ? $params["sales_spk_id_0"] : NULL),
			(!empty($params["sales_spk_id_1"]) ? $params["sales_spk_id_1"] : NULL)
		);

		foreach ($spk as $key => $value) 
		{
			if ($value)
			{
				$insert = array(
					"sales_spk_sales" => $params["sales_spk_sales"],
					"sales_spk_id" => $value
				);

				if (DB::table($this->table)->insert($insert))
				{
					$result['success'] = 1;
				} else {
					$result['success'] = 0;
				}

				$check = DB::table("tb_spk")->where("spk_id", $value)->first();

				if (count($check) > 0)
				{
					$update = array("spk_sales" => $params["sales_spk_sales"]);
					DB::table("tb_spk")->where("spk_id", $value)->update($update);
				}
			}
		}

		return response()->json($result, 200);
	}

	public function destroy($id)
	{
		if ($this->id !== null) {
			$this->id = $this->id;
		} else {
			$this->id = $this->column . "_id";
		}

		$check = DB::table("tb_spk")->where("spk_id", $id)->first();

		if (count($check) > 0)
		{
			$update = array("spk_sales" => NULL);
			DB::table("tb_spk")->where("spk_id", $id)->update($update);
		}

		$proses = DB::table($this->table)
			->where($this->id, $id)
			->delete();

		if ($proses) {
			return response()->json(array("success" => 1), 200);
		} else {
			return response()->json(array("success" => 0), 200);
		}
	}
}