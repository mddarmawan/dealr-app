<?php

namespace App\Http\Controllers\Api\Umum;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiPelangganController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_sales_spk";
		$this->column = "sales_spk";
		$this->id = "sales_spk_id";
	}

	public function data()
	{
		$data = DB::table("tb_pelanggan")
			// ->leftjoin("tb_spk_pelanggan", "tb_spk_pelanggan.spk_pel_id", "=", "tb_pelanggan.pel_id");
			->leftjoin("tb_sales_pelanggan", "tb_sales_pelanggan.sales_pel_id", "=", "tb_pelanggan.pel_id")
			->leftjoin("tb_sales", "tb_sales.sales_uid", "=", "tb_sales_pelanggan.sales_pel_sales")
			->leftjoin("tb_team", "tb_team.team_id", "=", "tb_sales.sales_team")
			->leftjoin("tb_karyawan", "tb_karyawan.karyawan_id", "=", "tb_sales.sales_karyawan");

		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}
}