<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiKendaraanWarnaController extends ApiController
{

	/**
	 * Menentukan Table dan Kolom yang akan di gunakan selanjutnya.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->table = "tb_type_warna";
		$this->column = "type_warna";


	}

	/**
	 * Menampilkan Data yang terpilih.
	 *
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		$where = array(
			$this->column . "_id" => $id
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang tidak di aktifkan.
	 *
	 * @return void
	 */
	public function inactive()
	{
		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang masuk ke Recycle Bin.
	 *
	 * @return void
	 */
	public function deleted()
	{
		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan seluruh Data.
	 *
	 * @param array $where
	 * @return void
	 */
	public function data($where = NULL)
	{
		if (!empty($where)) {
			$where = $where;
		} else {
			$where = array();
		}

		if (!isset($where[$this->column . "_hapus"])) {
			$where[$this->column . "_hapus"] = "0";
		}

		$data = DB::table($this->table)
        ->join("tb_type","type_id","=","type_warna_type")
        ->join("tb_warna","warna_id","=","type_warna_id")
        ->where("type_status","1")
        ->where("type_hapus","0")
        ->where("warna_status","1")
        ->where("warna_hapus","0")
				->where($where)
				->get();


		return json_encode($data);
	}


}
