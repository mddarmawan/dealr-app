<?php

namespace App\Http\Controllers\Api\Penjualan;

use Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiSPKController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_spk";
		$this->column = "spk";
	}

	public function filter($filter, $startDate = NULL, $endDate = NULL)
	{
		$where = NULL;
		$whereNotNull = NULL;
		$whereRaw = NULL;
		$whereSearch = NULL;
		$whereDate = NULL;
		$whereNull = NULL;

		switch ($filter)
		{
			case "SALES":
				$whereNull = array("spk_do_spk");
			break;

			case "MANUAL":
				$where = array(
					"spk_automatch" => '0'
				);

				$whereRaw = "((spk_pel_kategori = '0' AND jumlah >= 3500000) OR spk_pel_kategori = '1')";
				$whereNull = array("kend_beli_dh", "spk_match_spk");
			break;

			case "CANCEL":
				$where = array(
					"spk_cancel" => '1'
				);
			break;

			case "SPV":
				$where = array(
					"spk_spv"		=> '0',
					"spk_cancel"	=> '0'
				);
			break;

			case "ADH":
				$where = array(
					"spk_status"	=> '0',
					"spk_cancel"	=> '0'
				);
			break;

			case "VALID":
				$where = array(
					"spk_status"	=> '1',
					"spk_cancel"	=> '0'
				);

				$whereRaw = "((spk_pel_kategori = '0' AND jumlah >= 3500000) OR spk_pel_kategori = '1')";
			break;

			case "MATCHING":
				$where = array(
					"spk_status"	=> '1',
					"spk_cancel"	=> '0'
				);

				$whereNotNull = array("spk_match_spk");
			break;

			case "DO":
				$where = array(
					"spk_status"	=> '1',
					"spk_cancel"	=> '0'
				);

				$whereNotNull = array("spk_do_spk");
			break;

			case "DOREADY":
				$where = array(
					"spk_status"	=> '1',
					"spk_cancel"	=> '0'
				);

				$whereNotNull = array("spk_match_spk", "spk_po_spk");
			break;

			case "GI":
				$where = array(
					"spk_status"	=> '1',
					"spk_cancel"	=> '0'
				);

				$whereNotNull = array("spk_gi_spk");
			break;

			case "INVALID":
				$where = array(
					"spk_status"	=> '1',
					"spk_cancel"	=> '0'
				);

				$whereNull = array("spk_do_spk","spk_gi_spk");
			break;

			case "ALL":
				$where = array();
			break;

			default:
				$where = array(
					"spk_status"	=> '0',
					"spk_cancel"	=> '0'
				);
			break;
		}

		if ($startDate != NULL && $startDate != "ALL")
		{
			$whereDate[] = array("spk_tgl", ">=", $startDate . "  00:00:00");
		}

		if ($endDate !== NULL && $endDate !== "ALL")
		{
			$whereDate[] = array("spk_tgl", "<=", $endDate . "  23:59:59");
		}

		return $this->data($where, $whereNotNull, $whereRaw, $whereDate, $whereNull);
	}

	public function data($where = NULL, $whereNotNull = NULL, $whereRaw = NULL, $whereDate = NULL, $whereNull = NULL)
	{
		$result = array();

		if (empty($where)) {
			$where = array();
		}

		$data = DB::table("tb_spk")
				->leftjoin("tb_sales", "sales_uid", "=", "spk_sales")
				->leftjoin("tb_team", "team_id", "=", "sales_team")
				->leftjoin("tb_karyawan", "karyawan_id", "=", "sales_karyawan")
				->leftjoin("tb_spk_pelanggan", "tb_spk_pelanggan.spk_pel_spk", "=", "tb_spk.spk_id")
				->leftjoin("tb_spk_stnk", "tb_spk_stnk.spk_stnk_spk", "=", "tb_spk.spk_id")
				->leftjoin("tb_warna", "tb_warna.warna_id", "=", "tb_spk.spk_warna")
				->leftjoin("tb_spk_match", "tb_spk_match.spk_match_spk", "=", "tb_spk.spk_id")
				->leftjoin(DB::raw("(SELECT kend_beli_dh, kend_beli_rangka, kend_beli_mesin, warna_nama as kend_beli_warna FROM tb_kend_beli JOIN tb_warna ON kend_beli_warna = warna_id) kend_beli"), "kend_beli_dh", "=", "tb_spk_match.spk_match_dh")
				->leftjoin("tb_spk_do", "tb_spk_do.spk_do_spk", "=", "tb_spk.spk_id")
				->leftjoin("tb_spk_po", "tb_spk_po.spk_po_spk", "=", "tb_spk.spk_id")
				->leftjoin("tb_spk_gi", "tb_spk_gi.spk_gi_spk", "=", "tb_spk.spk_id")
				->leftjoin("tb_variant", "tb_variant.variant_id", "=", "tb_spk.spk_kendaraan")
				->leftjoin("tb_type", "tb_variant.variant_type", "=", "tb_type.type_id")
				->leftjoin(DB::raw(
					"(SELECT
						spk_bayar_spk,
						CASE
							WHEN SUM(spk_bayar_jumlah) IS NULL THEN CAST(0 AS INTEGER)
							ELSE CAST(SUM(spk_bayar_jumlah) AS INTEGER)
						END as jumlah
					FROM tb_spk_bayar
					GROUP BY spk_bayar_spk) AS bayar"
				), "bayar.spk_bayar_spk", "=", "tb_spk.spk_id")
				->leftjoin(DB::raw(
					"(SELECT
						spk_diskon_spk,
						spk_diskon_nama,
						spk_diskon_alamat,
						spk_diskon_telp,
						spk_diskon_hubungan,
						spk_diskon_bank,
						spk_diskon_rek,
						spk_diskon_an,
						spk_diskon_status,
						spk_diskon_spv,
						CASE
							WHEN spk_diskon_komisi IS NULL THEN CAST(0 AS INTEGER)
							ELSE CAST(spk_diskon_komisi AS INTEGER)
						END as spk_diskon_komisi,
						CASE
							WHEN spk_diskon_cashback IS NULL THEN CAST(0 AS INTEGER)
							ELSE CAST(spk_diskon_cashback AS INTEGER)
						END as spk_diskon_cashback,
						created_at as spk_diskon_tgl,
						spk_diskon_ket
					FROM tb_spk_diskon) AS diskon"
				),"diskon.spk_diskon_spk", "=", "tb_spk.spk_id")
						->leftjoin(DB::raw(
							"(SELECT
								spk_aksesoris_spk,
								CASE
									WHEN SUM(spk_aksesoris_harga) IS NULL THEN CAST(0 AS INTEGER)
									ELSE CAST(SUM(spk_aksesoris_harga) AS INTEGER)
								END as total_aksesoris
							FROM tb_spk_aksesoris
							GROUP BY spk_aksesoris_spk) AS aksesoris"
						), "aksesoris.spk_aksesoris_spk", "=", "diskon.spk_diskon_spk")

				->where($where);

		if (!empty($whereNotNull)) {
			foreach ($whereNotNull as $w) {
				$data->whereNotNull($w);
			}
		}

		if (!empty($whereNull)) {
			foreach ($whereNull as $w) {
				$data->whereNull($w);
			}
		}

		if (!empty($whereRaw)) {
			$data->whereRaw($whereRaw);
		}

		if (!empty($whereDate)) {
			$data->where(function ($query) use ($whereDate) {
				foreach ($whereDate as $key => $value) {
					$query->where($whereDate[$key][0], $whereDate[$key][1], $whereDate[$key][2]);
				}
			});
		}

		$status = "
			IF((spk_pel_kategori = '0' AND jumlah >= 3500000) OR spk_pel_kategori = '1', TRUE, FALSE) as spk_valid, 
			IF(spk_match_spk IS NULL, FALSE, TRUE) as spk_match, 
			IF(spk_po_spk IS NULL, FALSE, TRUE) as spk_po,
			IF(spk_diskon_spv = 1, TRUE, FALSE) AS spk_diskon";

		$status_number = "
			CASE
				WHEN spk_gi_spk IS NOT NULL THEN CAST(4 AS INTEGER)
				WHEN spk_do_spk IS NOT NULL THEN CAST(3 AS INTEGER)
				WHEN spk_match_spk IS NOT NULL THEN CAST(2 AS INTEGER)
				WHEN (spk_pel_kategori = '0' AND jumlah >= 3500000) OR spk_pel_kategori = '1' THEN CAST(1 AS INTEGER)
				ELSE CAST(0 AS INTEGER)
			END AS spk_status_number";

		$data->orderBy("spk_status", "ASC")->orderBy("tb_spk.spk_id", "DESC");
		$data->select("*",DB::raw("(spk_harga - jumlah) as piutang"),DB::raw("(spk_diskon_cashback + total_aksesoris) as total_diskon"), DB::raw($status), DB::raw($status_number));

		$result = $data->get();

		for ($i = 0; $i < count($result); $i++)
		{
			foreach ($result[$i] as $key => $value)
			{
				$result[$i]->riwayat_pembayaran = DB::table("tb_spk_bayar")->where("spk_bayar_spk", $result[$i]->spk_id)->get();

				if (!strcmp($key, "spk_valid") || !strcmp($key, "spk_match") || !strcmp($key, "spk_po") || !strcmp($key, "spk_diskon")) 
				{
					$result[$i]->$key = false;

					if ($value == "1")
					{
						$result[$i]->$key = true;
					}
				}
			}
		}

		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($result)
			);
	}

	public function getOne($id)
	{
		return $this->data(array("spk_id" => $id));
	}

	public function getLast()
	{
		$data = DB::table("tb_spk")->orderBy("spk_id", "desc")->first();

		if (count($data) > 0)
		{
			$prefix = substr($data->spk_id, 0, 2);
			$suffix = sprintf("%05s", substr($data->spk_id, 3) + 1);
			return $prefix . '-' . $suffix;
		}
		else
		{
			return date("y") . "-" . "00001";
		}
	}

	public function getLastDO()
	{
		$data = DB::table("tb_spk_do")->orderBy("spk_do_spk", "desc")->first();

		if (count($data) > 0)
		{
			$prefix = "BP" . date("y");
			$suffix = sprintf("%05s", substr($data->spk_do_spk, 4) + 1);
			return $prefix . $suffix;
		}
		else
		{
			return "BP" . date("y") . "00001";
		}
	}

	public function sales($id)
	{
		$data = DB::table("tb_sales_spk")
			->leftjoin("tb_spk", "tb_spk.spk_id", "=", "tb_sales_spk.sales_spk_id")
			->leftjoin("tb_spk_do", "tb_spk_do.spk_do_spk", "=", "tb_spk.spk_id")
			->whereNull("spk_do_spk")
			->where("sales_spk_sales", $id)
			->get();

		$count = count($data);

		return $count;
	}

	public function pelanggan($id)
	{
		$data = DB::table("tb_spk_pelanggan")
			->leftjoin("tb_spk", "tb_spk.spk_id", "=", "tb_spk_pelanggan.spk_pel_spk")
			->leftjoin("tb_spk_do", "tb_spk_do.spk_do_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_spk_stnk", "tb_spk_stnk.spk_stnk_spk", "=", "tb_spk.spk_sales")
			->leftjoin("tb_sales", "tb_sales.sales_uid", "=", "tb_spk.spk_sales")
			->leftjoin("tb_team", "tb_team.team_id", "=", "tb_sales.sales_team")
			->leftjoin("tb_karyawan", "tb_karyawan.karyawan_id", "=", "tb_sales.sales_karyawan")
			->leftjoin("tb_kontak", "tb_kontak.kontak_id", "=", "tb_spk.spk_leasing")
			->leftjoin("tb_variant", "tb_variant.variant_id", "=", "tb_spk.spk_kendaraan")
			->leftjoin("tb_type", "tb_variant.variant_type", "=", "tb_type.type_id")
			->where("spk_pel_id", $id);

		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}

	public function data_aksesoris($id)
	{
		$data = DB::table("tb_spk_aksesoris")
			->where("spk_aksesoris_spk",$id)
			->get();

		return json_encode($data);
	}

	public function data_bayar($id)
	{
		$data = DB::table("tb_spk_bayar")
			->where("spk_bayar_spk",$id)
			->get();

		return json_encode($data);
	}

	public function terima_spk($id)
	{
		$result['success'] = 0;
		$data = array(
			"spk_status"	=> 1,
			"spk_automatch"	=> Request::get("spk_automatch")
		);

		$proses = DB::table("tb_spk")
			->where("spk_id",$id)
			->update($data);
		if ($proses>0)
		{
			$result['success'] = 1;
		}

		return json_encode($result);
	}

	public function revisi_spk($id)
	{
		$result['success'] = 0;
		$proses = DB::table("tb_spk")
			->where("spk_id",$id)
			->update(array("spk_status" => 9));

		if ($proses > 0)
		{
			$result['success'] = 1;
		}

		return json_encode($result);
	}

	public function matching($id)
	{
		$result['success'] = 0;
		$params = json_decode(request("data"), TRUE);

		$insert = array(
			"spk_match_spk"	=> $id,
			"spk_match_dh"	=> $params["kend_beli_dh"],
			"spk_match_tgl"	=> date("Y-m-d"),
			"spk_match_exp"	=> date("Y-m-d")
		);

		if (DB::table("tb_spk_match")->insert($insert))
		{
			$result['success'] = 1;
		}

		return response()->json($result, 200);
	}

	public function terbitkan($id)
	{
		$result['success'] = 0;
		$params = json_decode(request("data"), TRUE);
		$insert = array("spk_po_spk" => $id);
		$check = DB::table("tb_spk")->where("spk_id", $id)->first();

		if ($check->spk_pembayaran == 1)
		{
			if (DB::table("tb_spk_po")->insert($insert))
			{
				$result['success'] = 1;
			}
			else
			{
				$result['success'] = 0;
			}
		}

		$insert = array("spk_samsat_spk" => $id);

		if (DB::table("tb_spk_samsat")->insert($insert))
		{
			$result['success'] = 1;
		}
		else
		{
			$result['success'] = 0;
		}

		$insert = array("spk_do_spk" => $id, "spk_do_faktur" => $this->getLastDO(), "spk_do_tgl" => date("Y-m-d"));

		if (DB::table("tb_spk_do")->insert($insert))
		{
			$result['success'] = 1;
		}
		else
		{
			$result['success'] = 0;
		}

		return response()->json($result, 200);
	}
}