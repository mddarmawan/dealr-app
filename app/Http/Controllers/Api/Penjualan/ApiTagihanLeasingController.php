<?php

namespace App\Http\Controllers\Api\Penjualan;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiTagihanLeasingController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_spk_po";
		$this->column = "spk_po";
		$this->id = "spk_po_spk";
		$this->dateColumn = "spk_tgl";
	}

	public function data($whereDate = NULL)
	{
		$data = DB::table("tb_spk_po")
			->join("tb_spk", "tb_spk.spk_id", "=", "tb_spk_po.spk_po_spk")
			->join("tb_spk_match", "tb_spk_match.spk_match_spk", "tb_spk.spk_id")
			->join("tb_kend_beli", "tb_kend_beli.kend_beli_dh", "=", "tb_spk_match.spk_match_dh")
			// ->join("tb_spk_do", "tb_spk.spk_id", "=", "tb_spk_do.spk_do_spk")
			->leftjoin("tb_variant", "tb_variant.variant_id", "=", "tb_spk.spk_kendaraan")
			->leftjoin("tb_type", "tb_variant.variant_type", "=", "tb_type.type_id")
			->leftjoin("tb_warna", "tb_warna.warna_id", "=", "tb_spk.spk_warna")
			->leftjoin("tb_spk_pelanggan", "tb_spk_pelanggan.spk_pel_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_spk_stnk", "tb_spk_stnk.spk_stnk_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_spk_samsat", "tb_spk_samsat.spk_samsat_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_spk_gi", "tb_spk_gi.spk_gi_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_sales", "tb_sales.sales_uid", "=", "tb_spk.spk_sales")
			->leftjoin("tb_karyawan", "tb_karyawan.karyawan_id", "=", "tb_sales.sales_karyawan")
			->leftjoin("tb_jabatan", "tb_jabatan.jabatan_id", "=", "tb_karyawan.karyawan_jabatan")
			->leftjoin("tb_team", "tb_team.team_id", "=", "tb_sales.sales_team")
			->leftjoin(DB::raw(
				"(SELECT
					spk_bayar_spk,
					CASE
						WHEN SUM(spk_bayar_jumlah) IS NULL THEN CAST(0 AS INTEGER)
						ELSE CAST(SUM(spk_bayar_jumlah) AS INTEGER)
					END as jumlah
				FROM tb_spk_bayar
				GROUP BY spk_bayar_spk) AS bayar"
			), "bayar.spk_bayar_spk", "=", "tb_spk.spk_id")
			->leftjoin(DB::raw(
				"(SELECT
					kontak_id AS spk_po_leasing,
					kontak_nama AS spk_po_leasing_nama,
					kontak_kota AS spk_po_leasing_kota,
					kontak_alamat AS spk_po_leasing_alamat
				FROM tb_kontak) AS leasing"
			), function ($join) {
				$join->on("leasing.spk_po_leasing", "=", "tb_spk_po.spk_po_leasing");
			})
			->leftjoin(DB::raw(
				"(SELECT
					kontak_id AS spk_po_asuransi,
					kontak_nama AS spk_po_asuransi_nama,
					kontak_kota AS spk_po_asuransi_kota,
					kontak_alamat AS spk_po_asuransi_alamat
				FROM tb_kontak) AS asuransi"
			), function ($join) {
				$join->on("asuransi.spk_po_asuransi", "=", "tb_spk_po.spk_po_asuransi");
			})
			->leftjoin("tb_asuransi_jenis", "tb_asuransi_jenis.ajenis_id", "=", "tb_spk_po.spk_po_jenis_asuransi");

		if (!empty($whereDate)) {
			$data->where(function ($query) use ($whereDate) {
				foreach ($whereDate as $key => $value) {
					$query->where($whereDate[$key][0], $whereDate[$key][1], $whereDate[$key][2]);
				}
			});
		}

		$status = "IF(spk_po_refund IS NOT NULL, CAST(1 AS INTEGER), CAST(0 AS INTEGER)) as spk_status";

		$data->select(
			"*", 
			DB::raw("DATEDIFF(spk_gi_tgl, spk_po_waktu) AS spk_po_wgi"),
			DB::raw("DATEDIFF(spk_po_cetak, spk_gi_tgl) AS spk_po_wcetak"),
			DB::raw("DATEDIFF(spk_po_tagihan, spk_po_cetak) AS spk_po_wtagihan"),
			DB::raw("DATEDIFF(spk_po_lunas, spk_po_tagihan) AS spk_po_wlunas"),
			DB::raw("DATEDIFF(spk_po_refund, spk_po_lunas) AS spk_po_wrefund"),
			DB::raw("(spk_harga - jumlah) as piutang"),
			DB::raw($status)
		);
		
		$result = $data->get();
		for ($i = 0; $i < count($result); $i++)
		{
			foreach ($result[$i] as $key => $value)
			{
				if (strpos($key, "iutang"))
				{
					$result[$i]->piutang_terbilang = ucfirst($this->terbilang($result[$i]->piutang));
				}
				else if (strpos($key, "gi_tgl") || strpos($key, "po_waktu") || strpos($key, "po_cetak") || strpos($key, "po_tagihan") || strpos($key, "po_lunas") || strpos($key, "po_wgi") || strpos($key, "po_wcetak") || strpos($key, "po_wtagihan") || strpos($key, "po_wlunas") || strpos($key, "po_wrefund") || strpos($key, "po_refund"))
				{
					if ($value === null)
					{
						$result[$i]->$key = null;
					}
				}
				else if (strpos($key, "po_dokumen"))
				{
					$result[$i]->$key = false;

					if ($value == "1")
					{
						$result[$i]->$key = true;
					}
				}
				else if ($value === null)
				{
					$result[$i]->$key = "KOSONG";
				}
				else if ($value === "")
				{
					$result[$i]->$key = "-";
				}
			}
		}

		return json_encode($result);
	}

	protected function update(Request $request, $id)
	{
		$success = 1;
		if ($this->id !== null) 
		{
			$this->id = $this->id;
		} 
		else 
		{
			$this->id = $this->column . "_id";
		}

		$params = json_decode($request->data, TRUE);
		foreach ($params as $key => $value) 
		{
			if (strpos($key, "gi_tgl"))
			{
				$gi_tgl = date_format(date_create($value),"Y-m-d");

				$check = DB::table("tb_spk_gi")
					->where("spk_gi_spk", $id)
					->first();

				if (count($check) > 0)
				{
					$proses = DB::table("tb_spk_gi")
						->where("spk_gi_spk", $id)
						->update(array("spk_gi_tgl" => $gi_tgl));
				}
				else
				{
					$proses = DB::table("tb_spk_gi")
						->where("spk_gi_spk", $id)
						->insert(array("spk_gi_spk" => $id, "spk_gi_tgl" => $gi_tgl));
				}

				unset($params[$key]);

				if (!$proses) 
				{
					$success = 0;
				}

				return response()->json(array("success" => $proses), 200);
			}
			else if (strpos($key, "kerja") || strpos($key, "lahir") || strpos($key, "po_cetak") || strpos($key, "po_tagihan") || strpos($key, "po_lunas") || strpos($key, "po_refund")) 
			{
				$params[$key] = date_format(date_create($value),"Y-m-d");
			}
		}

		if (!empty($params))
		{
			$proses = DB::table($this->table)
				->where($this->id, $id)
				->update($params);

			if (!$proses) 
			{
				$success = 0;
			}
		}

		return response()->json(array("success" => $success), 200);
	}

	public function terbilang($angka) {
		$angka = (float)$angka;

		$bilangan = array('','satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan','sepuluh','sebelas');

		if ($angka < 12)
		{
			return $bilangan[$angka];
		}
		else if ($angka < 20)
		{
			return $bilangan[$angka - 10] . ' belas';
		} 
		else if ($angka < 100)
		{
			$hasil_bagi = (int)($angka / 10);
			$hasil_mod = $angka % 10;

			return trim(sprintf('%s puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
		} 
		else if ($angka < 200)
		{
			return sprintf('seratus %s', $this->terbilang($angka - 100));
		} 
		else if ($angka < 1000)
		{
			$hasil_bagi = (int)($angka / 100);
			$hasil_mod = $angka % 100;

			return trim(sprintf('%s ratus %s', $bilangan[$hasil_bagi], $this->terbilang($hasil_mod)));
		} 
		else if ($angka < 2000)
		{
			return trim(sprintf('seribu %s', $this->terbilang($angka - 1000)));
		} 
		else if ($angka < 1000000)
		{
			$hasil_bagi = (int)($angka / 1000);
			$hasil_mod = $angka % 1000;

			return sprintf('%s ribu %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod));
		} 
		else if ($angka < 1000000000)
		{
			$hasil_bagi = (int)($angka / 1000000);
			$hasil_mod = $angka % 1000000;

			return trim(sprintf('%s juta %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod)));
		} 
		else if ($angka < 1000000000000)
		{
			$hasil_bagi = (int)($angka / 1000000000);
			$hasil_mod = fmod($angka, 1000000000);

			return trim(sprintf('%s milyar %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod)));
		} 
		else if ($angka < 1000000000000000)
		{
			$hasil_bagi = $angka / 1000000000000;
			$hasil_mod = fmod($angka, 1000000000000);
			return trim(sprintf('%s triliun %s', $this->terbilang($hasil_bagi), $this->terbilang($hasil_mod)));
		} 
		else
		{
			return 'Wow...';
		}
	}
}