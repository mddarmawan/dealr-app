<?php

namespace App\Http\Controllers\Api\Penjualan;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiProsesSamsatController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_spk_samsat";
		$this->column = "spk_samsat";
		$this->id = "spk_samsat_spk";
		$this->dateColumn = "spk_samsat_tglfaktur";
	}

	public function data($whereDate = NULL)
	{
		$data = DB::table($this->table)
			->join("tb_spk", "tb_spk.spk_id", "=", "tb_spk_samsat.spk_samsat_spk")
			->join("tb_spk_match", "tb_spk_match.spk_match_spk", "tb_spk.spk_id")
			->join("tb_spk_po", "tb_spk_po.spk_po_spk", "tb_spk.spk_id")
			->join("tb_kend_beli", "tb_kend_beli.kend_beli_dh", "=", "tb_spk_match.spk_match_dh")
			->join("tb_spk_pelanggan", "tb_spk_pelanggan.spk_pel_spk", "=", "tb_spk.spk_id")
			->join("tb_variant", "tb_variant.variant_id", "=", "tb_spk.spk_kendaraan")
			->join("tb_type", "tb_variant.variant_type", "=", "tb_type.type_id")
			->leftjoin("tb_spk_stnk", "tb_spk_stnk.spk_stnk_spk", "=", "tb_spk.spk_id")
			->leftjoin(DB::raw(
				"(SELECT
					kontak_id AS spk_po_leasing,
					kontak_nama AS spk_po_leasing_nama,
					kontak_kota AS spk_po_leasing_kota,
					kontak_alamat AS spk_po_leasing_alamat
				FROM tb_kontak) AS leasing"
			), function ($join) {
				$join->on("leasing.spk_po_leasing", "=", "tb_spk_po.spk_po_leasing");
			})
			->leftjoin(DB::raw(
				"(SELECT
					kontak_id AS spk_po_asuransi,
					kontak_nama AS spk_po_asuransi_nama,
					kontak_kota AS spk_po_asuransi_kota,
					kontak_alamat AS spk_po_asuransi_alamat
				FROM tb_kontak) AS asuransi"
			), function ($join) {
				$join->on("asuransi.spk_po_asuransi", "=", "tb_spk_po.spk_po_asuransi");
			})
			->leftjoin("tb_asuransi_jenis", "tb_asuransi_jenis.ajenis_id", "=", "tb_spk_po.spk_po_jenis_asuransi");

		if (!empty($whereDate)) {
			$data->where(function ($query) use ($whereDate) {
				foreach ($whereDate as $key => $value) {
					$query->where($whereDate[$key][0], $whereDate[$key][1], $whereDate[$key][2]);
				}
			});
		}
		
		$result = $data->get();
		for ($i = 0; $i < count($result); $i++)
		{
			foreach ($result[$i] as $key => $value)
			{
				if (strpos($key, "tglfaktur") || strpos($key, "tglstnk") || strpos($key, "tglbpkb") || strpos($key, "ujitipe"))
				{
					if ($value === null)
					{
						$result[$i]->$key = null;
					}
				}
				else if (strpos($key, "samsat_plat") || strpos($key, "samsat_notis") || strpos($key, "samsat_stnk")) 
				{
					$result[$i]->$key = false;

					if ($value == "1")
					{
						$result[$i]->$key = true;
					}
				}
				else if ($value === null)
				{
					$result[$i]->$key = "KOSONG";
				}
				else if ($value === "")
				{
					$result[$i]->$key = "-";
				}
			}
		}

		return json_encode($result);
	}

	public function update(Request $request, $id)
	{
		if ($this->id !== null) {
			$this->id = $this->id;
		} else {
			$this->id = $this->column . "_id";
		}

		$params = json_decode($request->data, TRUE);
		foreach ($params as $key => $value)
		{
			if (strpos($key, "tglfaktur") || strpos($key, "tglstnk") || strpos($key, "tglbpkb") || strpos($key, "ujitipe"))
			{
				$params[$key] = date_format(date_create($value),"Y-m-d");
			} 
			else if (strpos($key, "samsat_plat") || strpos($key, "samsat_notis") || strpos($key, "samsat_stnk")) 
			{
				if ($value == true) 
				{
					$params[$key] = "1";
				} 
				else 
				{
					$params[$key] = "0";
				}
			}
		}

		$proses = DB::table($this->table)
			->where($this->id, $id)
			->update($params);

		if ($proses) {
			return response()->json(array("success" => 1), 200);
		} else {
			return response()->json(array("success" => 0), 200);
		}
	}
}