<?php

namespace App\Http\Controllers\Api\Penjualan;

use Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiDiskonsController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_diskon";
		$this->column = "spk";
	}

	public function filter($filter, $startDate = NULL, $endDate = NULL)
	{
		$where = NULL;
		$whereDate = NULL;

		switch ($filter)
		{
			case "SPV":	
				$where = array(
					["spk_diskon_spv", "NOT LIKE", '0']
				);
			break;
			
			case "KACAB":	
				$where = array(
					["spk_diskon_spv", "NOT LIKE", '1'],
					["spk_diskon_status", "NOT LIKE", '0']
				);
			break;

			case "ALL":
				$where = NULL;
			break;
			
			default:
				$where = NULL;
			break;
		}

		if ($startDate != NULL && $startDate != "ALL")
		{
			$whereDate[] = array("spk_tgl", ">=", $startDate . "  00:00:00");
		}

		if ($endDate !== NULL && $endDate !== "ALL")
		{
			$whereDate[] = array("spk_tgl", "<=", $endDate . "  23:59:59");
		}

		return $this->data($where, $whereDate);
	}

	public function data($where = NULL)
	{
		$data = DB::table("tb_spk")
				->join("tb_spk_pelanggan", "tb_spk_pelanggan.spk_pel_spk", "=", "tb_spk.spk_id")
				->join("tb_spk_stnk", "tb_spk_stnk.spk_stnk_spk", "=", "tb_spk.spk_id")
				->leftjoin("tb_variant", function ($join) {
					$join->on("tb_variant.variant_id", "=", "tb_spk.spk_kendaraan")
						->leftjoin("tb_type", "tb_type.type_id", "tb_variant.variant_type");
				})
				->join("tb_warna", "tb_warna.warna_id", "=", "tb_spk.spk_warna")
				->leftjoin("tb_spk_match", "tb_spk_match.spk_match_spk", "=", "tb_spk.spk_id")
				->leftjoin("tb_spk_do", "tb_spk_do.spk_do_spk", "=", "tb_spk.spk_id")
				->leftjoin("tb_spk_po", "tb_spk_po.spk_po_spk", "=", "tb_spk.spk_id")
				->leftjoin("tb_spk_gi", "tb_spk_gi.spk_gi_spk", "=", "tb_spk.spk_id")
				->leftjoin(DB::raw(
					"(SELECT 
						spk_diskon_spk, 
						spk_diskon_nama, 
						spk_diskon_alamat, 
						spk_diskon_telp, 
						spk_diskon_hubungan, 
						spk_diskon_bank, 
						spk_diskon_rek, 
						spk_diskon_an, 
						spk_diskon_status, 
						spk_diskon_spv, 
						spk_diskon_komisi, 
						spk_diskon_cashback, 
						created_at as spk_diskon_tgl, 
						spk_diskon_ket
					FROM tb_spk_diskon) AS diskon"
				), function ($join) {
					$join->on("diskon.spk_diskon_spk", "=", "tb_spk.spk_id")
						->leftjoin(DB::raw(
							"(SELECT 
								spk_aksesoris_spk, 
								SUM(spk_aksesoris_harga) AS total_aksesoris 
							FROM tb_spk_aksesoris 
							GROUP BY spk_aksesoris_spk) AS aksesoris"
						), "aksesoris.spk_aksesoris_spk", "=", "diskon.spk_diskon_spk");
				})
				->leftjoin(DB::raw(
					"(SELECT
						spk_aksesoris_spk,
						CASE
							WHEN SUM(spk_aksesoris_harga) IS NULL THEN CAST(0 AS INTEGER)
							ELSE CAST(SUM(spk_aksesoris_harga) AS INTEGER)
						END as total_aksesoris
					FROM tb_spk_aksesoris
					GROUP BY spk_aksesoris_spk) AS aksesoris"
				), "aksesoris.spk_aksesoris_spk", "=", "diskon.spk_diskon_spk");

		if (!empty($whereDate)) {
			$data->where(function ($query) use ($whereDate) {
				foreach ($whereDate as $key => $value) {
					$query->where($whereDate[$key][0], $whereDate[$key][1], $whereDate[$key][2]);
				}
			});
		}

		if (!empty($where)) {
			$data->where(function ($query) use ($where) {
				foreach ($where as $key => $value) {
					$query->where($where[$key][0], $where[$key][1], $where[$key][2]);
				}
			});
		}

		$data->orderBy("spk_diskon_tgl", "DESC");
		
		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}
}
