<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Api\ApiController;
use DB;


class ApiSalmonController extends ApiController
{

	public function __construct()
	{
		$this->table = "users";
		$this->column = "";
	}

	public function show($id)
	{
		$where = array(
			"username" => $id
		);


    		$data = DB::table("tb_team")
            ->join("tb_karyawan","karyawan_id","team_spv")
            ->leftjoin($this->table,"users_karyawan","karyawan_id")
            ->select("username","karyawan_id","karyawan_nama","_status","scope")
            ->where("karyawan_status","1")
            ->where("karyawan_hapus","0")
    				->where($where)
    				->count();

    if ($data>0){
      return 2;
    }

		return 1;
	}

	public function data($where = NULL)
	{
		if (!empty($where)) {
			$where = $where;
		} else {
			$where = array();
		}

		$data = DB::table("tb_team")
        ->join("tb_karyawan","karyawan_id","team_spv")
        ->leftjoin($this->table,"users_karyawan","karyawan_id")
        ->select("username","karyawan_id","karyawan_nama","_status","scope")
        ->where("karyawan_status","1")
        ->where("karyawan_hapus","0")
				->where($where)
				->get();
    $result = array();
    foreach($data as $d){
      $item['username'] = $d->username;
      $item['karyawan_id'] = $d->karyawan_id;
      $item['karyawan_nama'] = $d->karyawan_nama;
      $item['_status'] = $d->_status;
      $item['scope'] = $d->scope;
      if ($item['scope']=='{"spv"}'){
        $item['scope'] = "1";
      }else if ($item['scope']=='{"kacab"}'){
        $item['scope'] = "2";
      }else if ($item['scope']=='{"spv","kacab"}'){
        $item['scope'] = "3";
      }
      array_push($result,$item);
    }



		return json_encode($result);
	}


     protected function set(Request $request, $id,$kode)
     {
    	 $params = json_decode($request->data, TRUE);
       $scope = '{"spv"}';
       if (isset($params['scope'])){
          $tmp = $params['scope'];
          if ($tmp==1){
             $scope = '{"spv"}';
          }else if ($tmp==2){
             $scope = '{"kacab"}';
          }else if ($tmp==3){
             $scope = '{"spv","kacab"}';
          }
          $params['scope'] = $scope;
       }

       if($id!="null"){
        $proses = DB::table($this->table)->where("users_karyawan", $kode)->update($params);
       }else{
         $data['client_secret'] = $params['uid'];
         $data['users_karyawan'] = $kode;
         $data['password'] = Hash::make($params['password']);
         $data['username'] = $params['username'];
         $data['scope'] = $params['scope'];
         $data['client_id'] = "encardms";
         $check = DB::table($this->table)->where("users_karyawan", $kode)->count();
         if ($check>0){
           $proses = DB::table($this->table)->where("users_karyawan", $kode)->update($data);
         }else{
           $proses = DB::table($this->table)->insert($data);
         }
       }


    	 if ($proses) {
    			return response()->json(array("success"=>1), 200);
    		} else {
    			return response()->json(array("success"=>0), 200);
    		}
     }
}
