<?php

namespace App\Http\Controllers\Api\Pembelian;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiReturController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_kend_retur";
		$this->column = "kend_retur";
		$this->id = "kend_retur_dh";
	}

	public function store(Request $request = NULL, $import = NULL)
	{
		$result['success'] = 0;
		$params = json_decode($request->data, TRUE);

		foreach ($params as $key => $value)
		{
			if (strpos($key, "tgl"))
			{
				$params[$key] = date_format(date_create($value),"Y-m-d");
			}
		}

		$insert = array(
			"kend_retur_dh" => $params["kend_beli_dh"],
			"kend_retur_tgl" => $params["kend_retur_tgl"],
			"kend_retur_ket" => $params["kend_retur_ket"],
		);

		if (DB::table($this->table)->insert($insert))
		{
			$result['success'] = 1;
		}

		return response()->json($result, 200);
	}

	public function data($where = NULL)
	{
		$data = DB::table("tb_kend_retur")
			->join("tb_kend_beli", "tb_kend_beli.kend_beli_dh", "=", "tb_kend_retur.kend_retur_dh")
			->leftjoin("tb_kend_onhand", "tb_kend_onhand.kend_onhand_rangka", "=", "tb_kend_beli.kend_beli_rangka")
			->leftjoin("tb_kend_rrn", "tb_kend_rrn.kend_rrn_id", "=", "tb_kend_onhand.kend_onhand_rrn")
			->leftjoin("tb_variant", "tb_variant.variant_serial", "=", "tb_kend_beli.kend_beli_variant")
			->leftjoin("tb_spk", "tb_spk.spk_kendaraan", "=", "tb_variant.variant_id")
			->leftjoin("tb_type", "tb_variant.variant_type", "=", "tb_type.type_id")
			->leftjoin("tb_warna", "tb_warna.warna_id", "=", "tb_kend_beli.kend_beli_warna")
			->leftjoin("tb_spk_pelanggan", "tb_spk_pelanggan.spk_pel_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_spk_gi", "tb_spk_gi.spk_gi_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_spk_match", "tb_spk_match.spk_match_spk", "=", "tb_spk.spk_id");
		
		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}

	public function destroy($id)
	{
		if ($this->id !== null) {
			$this->id = $this->id;
		} else {
			$this->id = $this->column . "_id";
		}

		$proses = DB::table($this->table)
			->where($this->id, $id)
			->delete();

		if ($proses) {
			return response()->json(array("success" => 1), 200);
		} else {
			return response()->json(array("success" => 0), 200);
		}
	}
}