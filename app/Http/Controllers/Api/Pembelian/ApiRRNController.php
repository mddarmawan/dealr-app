<?php

namespace App\Http\Controllers\Api\Pembelian;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use Excel;
use DB;

class ApiRRNController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_kend_rrn";
		$this->column = "kend_rrn";
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function import(Request $request)
	{
		if (!empty(request('import_rrn_file'))) {
			$inserted = 0;
			$skipped = 0;
			$failed = 0;

			$path = request('import_rrn_file')->getRealPath();
			$datas = Excel::load($path, function($reader) {
				$reader->ignoreEmpty();
			})->get();


			foreach ($datas as $data) {
				foreach ($data as $value) {
					$check = DB::table('tb_kend_rrn')
						->where('kend_rrn_id', $value->rrn)
						->first();

					if (count($check)) {
						$skipped++;
						continue;
					}

					$insert = [
						'kend_rrn_id' => $value->rrn,
						'kend_rrn_produksi' => date("Y-m-d", strtotime($value->tanggal_produksi)),
						'kend_rrn_variant' => $value->tipe,
						'kend_rrn_warna' => $value->warna,
						'kend_rrn_upgrade' => $value->upgrade,
					];	

					if (!empty($insert)) {
						$inserted++;
						$this->store(NULL, $insert);
					} else {
						$failed++;
					}
				}
			}

			echo "Di Input: $inserted, Di Lewati: $skipped, Gagal: $failed.";
		} else {
			return "No File!";
		}
	}

	public function data($where = NULL)
	{
		$data = DB::table("tb_kend_rrn")
			->leftjoin("tb_kend_onhand", "tb_kend_onhand.kend_onhand_rrn", "=", "tb_kend_rrn.kend_rrn_id")
			->leftjoin("tb_kend_beli", "tb_kend_beli.kend_beli_dh", "=", "tb_kend_onhand.kend_onhand_rrn")
			->leftjoin("tb_variant", "tb_variant.variant_serial", "=", "tb_kend_beli.kend_beli_variant")
			->leftjoin("tb_type", "tb_variant.variant_type", "=", "tb_type.type_id")
			->leftjoin("tb_warna", "tb_warna.warna_id", "=", "tb_kend_beli.kend_beli_warna")
			->leftjoin("tb_spk", "tb_spk.spk_kendaraan", "=", "tb_variant.variant_id")
			->leftjoin("tb_spk_gi", "tb_spk_gi.spk_gi_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_spk_match", "tb_spk_match.spk_match_spk", "=", "tb_spk.spk_id")
			->whereNull("kend_onhand_rrn");
		
		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}

	public function destroy($id)
	{
		if ($this->id !== null) {
			$this->id = $this->id;
		} else {
			$this->id = $this->column . "_id";
		}

		$proses = DB::table($this->table)
			->where($this->id, $id)
			->delete();

		if ($proses) {
			return response()->json(array("success" => 1), 200);
		} else {
			return response()->json(array("success" => 0), 200);
		}
	}
}