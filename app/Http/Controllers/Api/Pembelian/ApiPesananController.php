<?php

namespace App\Http\Controllers\Api\Pembelian;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiPesananController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_spk";
		$this->column = "skp";
		$this->id = "skp_id";
	}

	public function data($where = NULL)
	{
		$data = DB::table("tb_spk")
			->join("tb_spk_pelanggan", "tb_spk_pelanggan.spk_pel_spk", "=", "tb_spk.spk_id")
			->join("tb_variant", "tb_variant.variant_id", "=", "tb_spk.spk_kendaraan")
			->join("tb_type", "tb_variant.variant_type", "=", "tb_type.type_id")
			->join("tb_warna", "tb_warna.warna_id", "=", "tb_spk.spk_warna")
			->leftjoin("tb_sales", "tb_sales.sales_uid", "=", "tb_spk.spk_sales")
			->leftjoin("tb_karyawan", "tb_karyawan.karyawan_id", "=", "tb_sales.sales_karyawan")
			->leftjoin("tb_team", "tb_team.team_id", "=", "tb_sales.sales_team")
			->leftjoin("tb_kend_beli", "tb_kend_beli.kend_beli_variant", "=", "tb_variant.variant_serial")
			->leftjoin("tb_spk_gi", "tb_spk_gi.spk_gi_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_spk_match", "tb_spk_match.spk_match_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_kend_retur", "tb_kend_retur.kend_retur_dh", "=", "tb_kend_beli.kend_beli_dh")
			->whereNull("kend_beli_dh")
			->whereNull("spk_gi_spk")
			->whereNull("spk_match_spk")
			->whereNull("kend_retur_dh");
		
		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}
}