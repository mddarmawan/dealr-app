<?php

namespace App\Http\Controllers\Api\Pembelian;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use Excel;
use DB;

class ApiOnHandController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_kend_onhand";
		$this->column = "kend_onhand";
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function import(Request $request)
	{
		if (!empty(request('import_onhand_file'))) {
			$inserted = 0;
			$skipped = 0;
			$failed = 0;

			$path = request('import_onhand_file')->getRealPath();
			config(['excel.import.startRow' => 9 ]);
			$datas = Excel::load($path, function($reader) {
				$reader->ignoreEmpty();
			})->get();


			foreach ($datas as $value) {
				$check = DB::table('tb_kend_onhand')
					->where('kend_onhand_rrn', $value->rrnumber)
					->first();

				if ((count($check) > 0) || empty($value->rrnumber)) {
					$skipped++;
					continue;
				} else {
				}

				$insert = [
					'kend_onhand_rrn' => $value->rrnumber,
					'kend_onhand_rangka' => $value->nik,
					'kend_onhand_mesin' => $value->engine,
					'kend_onhand_warna' => $value->color_description,
					'kend_onhand_tglrec' => date("Y-m-d", strtotime($value->rec_date)),
					'kend_onhand_tgldo' => date("Y-m-d", strtotime($value->do_date))
				];	

				if (!empty($insert)) {
					$inserted++;
					$this->store(NULL, $insert);
				} else {
					$failed++;
				}
			}

			return "Di Input: $inserted, Di Lewati: $skipped, Gagal: $failed.";
		} else {
			return "No File!";
		}
	}

	public function data($where = NULL)
	{
		$data = "";
		
		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}
}
