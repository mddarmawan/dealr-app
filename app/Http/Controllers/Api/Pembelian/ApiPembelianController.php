<?php

namespace App\Http\Controllers\Api\Pembelian;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use Excel;
use DB;

class ApiPembelianController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_kend_beli";
		$this->column = "kend_beli";
		$this->id = "kend_beli_dh";
		$this->dateColumn = "kend_beli_tgl";
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function import(Request $request)
	{
		if (!empty(request('import_beli_file'))) {
			$inserted = 0;
			$skipped = 0;
			$failed = 0;

			$path = request('import_beli_file')->getRealPath();
			config(['excel.import.startRow' => 1 ]);
			$datas = Excel::load($path, function($reader) {
				$reader->ignoreEmpty();
			})->get();

			$no_faktur = 'no._faktur';
			$tgl_faktur = 'tgl._faktur';
			$no_mesin = 'no._mesin';
			$kend_beli = DB::table('tb_kend_beli')->orderBy('kend_beli_dh', 'desc')->first();
			$no = ($kend_beli != NULL ? substr($kend_beli->kend_beli_dh, 4) : 0);

			foreach ($datas as $data) {
				foreach ($data as $value) {
					$variant = DB::table('tb_variant')->where('variant_serial', $value->tipe_kendaraan)->first();
					$warna = DB::table('tb_warna')->where('warna_nama', $value->color_description)->first();

					$check = DB::table('tb_kend_beli')
						->where('kend_beli_rangka', $value->chasis)
						->orWhere('kend_beli_mesin', $value->$no_mesin)
						->first();

					if ((count($check) > 0) || (count($variant) < 1)) {
						$skipped++;
						continue;
					} else {
						$no++;
					}

					$insert = [
						'kend_beli_dh' => $value->construction_year . sprintf("%04s", $no),
						'kend_beli_faktur' => $value->$no_faktur,
						'kend_beli_tgl' => date("Y-m-d H:i:s", strtotime($value->$tgl_faktur)),
						'kend_beli_tgltempo' => date("Y-m-d H:i:s", strtotime($value->tanggal_jatuh_tempo)),
						'kend_beli_variant' => $variant->variant_serial,
						'kend_beli_warna' => $warna->warna_id,
						'kend_beli_rangka' => $value->chasis,
						'kend_beli_mesin' => $value->$no_mesin,
						'kend_beli_tahun' => $value->construction_year,
						'kend_beli_harga' => $value->harga_unit,
						'kend_beli_diskon' => $value->harga_discount,
						'kend_beli_pbm' => $value->harga_pbm,
						'kend_beli_interest' => $value->interest,
					];	

					if (!empty($insert)) {
						$inserted++;
						$this->store(NULL, $insert);
					} else {
						$failed++;
					}
				}
			}

			return "Di Input: $inserted, Di Lewati: $skipped, Gagal: $failed.";
		} else {
			return "No File!";
		}
	}

	public function data($where = NULL)
	{
		$data = DB::table("tb_kend_beli")
			->leftjoin("tb_kend_onhand", "tb_kend_onhand.kend_onhand_rangka", "tb_kend_beli.kend_beli_rangka")
			->join("tb_variant", "tb_variant.variant_serial", "=", "tb_kend_beli.kend_beli_variant")
			->join("tb_type", "tb_variant.variant_type", "=", "tb_type.type_id")
			->join("tb_warna", "tb_warna.warna_id", "=", "tb_kend_beli.kend_beli_warna");

		if ($where !== NULL)
		{
			$data->where($where);
		}

		$data->orderBy("kend_beli_dh", "ASC");
		
		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}

	public function destroy($id)
	{
		if ($this->id !== null) {
			$this->id = $this->id;
		} else {
			$this->id = $this->column . "_id";
		}

		$check = DB::table($this->table)
			->leftjoin("tb_kend_retur", "tb_kend_retur.kend_retur_dh", "tb_kend_beli.kend_beli_dh")
			->whereNotNull("kend_retur_dh")
			->where("kend_beli_dh", $id)
			->get();

		if (count($check) > 0) {
			return 0;
		} else {
			$proses = DB::table($this->table)
			->where($this->id, $id)
			->get();

			if ($proses) {
				return 1;
			} else {
				return 0;
			}
		}
	}
}
