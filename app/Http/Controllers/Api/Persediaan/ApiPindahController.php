<?php

namespace App\Http\Controllers\Api\Persediaan;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiPindahController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_kend_gudang";
		$this->column = "kend_gudang";
		$this->id = "kend_gudang_beli";
	}

	public function store(Request $request = NULL, $import = NULL)
	{
		$result['success'] = 0;
		$params = json_decode($request->data, TRUE);

		foreach ($params as $key => $value)
		{
			if (strpos($key, "tgl") || strpos($key, "keluar"))
			{
				$params[$key] = date_format(date_create($value),"Y-m-d");
			}
		}

		$kend = DB::table("tb_kend_beli")->where("kend_beli_dh", $params["kend_beli_dh"])->first();

		$insert = array(
			"kend_gudang_beli" => $params["kend_beli_dh"],
			"kend_gudang_lokasi" => 1,
			"kend_gudang_masuk" => $kend->kend_beli_tgl,
			"kend_gudang_keluar" => $params["kend_gudang_keluar"],
			// "kend_gudang_ket" => $params[""],
			"kend_gudang_tujuan" => $params["kend_gudang_tujuan"],
			// "kend_gudang_user" => $params[""],
		);

		if (DB::table($this->table)->insert($insert))
		{
			$result['success'] = 1;
		}

		return response()->json($result, 200);
	}

	public function data($where = NULL)
	{
		return (new ApiStokController)->data(NULL, "RETUR");
	}

	public function destroy($id)
	{
		if ($this->id !== null) {
			$this->id = $this->id;
		} else {
			$this->id = $this->column . "_id";
		}

		$proses = DB::table($this->table)
			->where($this->id, $id)
			->delete();

		if ($proses) {
			return response()->json(array("success" => 1), 200);
		} else {
			return response()->json(array("success" => 0), 200);
		}
	}
}