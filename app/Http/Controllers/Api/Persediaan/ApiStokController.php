<?php

namespace App\Http\Controllers\Api\Persediaan;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiStokController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_kend_beli";
		$this->column = "kend_beli";
		$this->id = "kend_beli_dh";
	}

	public function filter($filter = NULL)
	{
		return $this->data(NULL, $filter);
	}

	public function data($where = NULL, $filter = NULL)
	{
		$data = DB::table("tb_kend_beli")
			->leftjoin("tb_kend_onhand", "tb_kend_onhand.kend_onhand_rangka", "tb_kend_beli.kend_beli_rangka")
			->leftjoin("tb_kend_rrn", "tb_kend_rrn.kend_rrn_id", "tb_kend_onhand.kend_onhand_rrn")
			->leftjoin("tb_variant", "tb_variant.variant_serial", "=", "tb_kend_beli.kend_beli_variant")
			->leftjoin("tb_spk", "tb_spk.spk_kendaraan", "=", "tb_variant.variant_id")
			->leftjoin("tb_type", "tb_variant.variant_type", "=", "tb_type.type_id")
			->leftjoin("tb_warna", "tb_warna.warna_id", "=", "tb_kend_beli.kend_beli_warna")
			->leftjoin("tb_kend_gudang", "tb_kend_gudang.kend_gudang_beli", "=", "tb_kend_beli.kend_beli_dh")
			->leftjoin("tb_kend_retur", "tb_kend_retur.kend_retur_dh", "=", "tb_kend_beli.kend_beli_dh")
			->leftjoin("tb_spk_match", "tb_spk_match.spk_match_dh", "=", "tb_kend_beli.kend_beli_dh")
			->leftjoin("tb_spk_gi", "tb_spk_gi.spk_gi_spk", "=", "tb_spk_match.spk_match_spk");
		
		if (!empty($filter && $filter == "RETUR")) 
		{
			$data->whereNotNull("kend_gudang_beli");
		}

		if (!empty($where))
		{
			$data->where($where);
		}

		$status = '
			CASE
				WHEN spk_gi_spk IS NOT NULL THEN "GI"
				WHEN spk_match_spk IS NOT NULL THEN "MATCHED"
				WHEN kend_retur_dh IS NOT NULL THEN "RETUR"
				WHEN kend_beli_lokasi IS NOT NULL THEN "STOK"
				ELSE "IN-TRANSIT"
			END AS kend_beli_status
		';

		$filter = "
			IF((kend_gudang_beli IS NOT NULL), CAST(kend_gudang_tujuan AS INTEGER), CAST(kend_beli_lokasi AS INTEGER)) AS kend_beli_lokasi
		";

		$data->orderBy("kend_beli_dh", "ASC");
		$data->select("*", DB::raw($status), DB::raw($filter));
		
		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}
}