<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiAksesorisController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_aksesoris";
		$this->column = "aksesoris";
	}

	public function show($id)
	{
		$this->editing = TRUE;

		$where = array(
			$this->column . "_kode" => $id
		);

		return $this->data($where);
	}

	public function inactive()
	{
		$this->inactive = TRUE;

		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	public function deleted()
	{
		$this->deleted = TRUE;

		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	public function data($where = NULL)
	{
		$result = array();

		if (empty($where)) {
			$where = array();
		}



		if (!isset($where[$this->column . "_hapus"])) {
			$where[$this->column . "_hapus"] = "0";
		}

		$data = DB::table($this->table)
			->leftjoin("tb_type", $this->table . ".aksesoris_kendaraan", "tb_type.type_id")
			->leftjoin("tb_kontak", $this->table . ".aksesoris_vendor", "tb_kontak.kontak_id")
			->where($where)
			->get();

		return json_encode($data);
	}

}
