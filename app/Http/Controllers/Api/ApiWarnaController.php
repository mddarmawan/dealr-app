<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiWarnaController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_warna";
		$this->column = "warna";
	}

	public function show($id)
	{
		$this->editing = TRUE;

		$where = array(
			$this->column . "_id" => $id
		);

		return $this->data($where);
	}

	public function inactive()
	{
		$this->inactive = TRUE;

		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	public function deleted()
	{
		$this->deleted = TRUE;

		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	public function data($where = NULL)
	{
		$result = array();

		if (empty($where)) {
			$where = array();
		}



		if (!isset($where[$this->column . "_hapus"])) {
			$where[$this->column . "_hapus"] = "0";
		}

		$data = DB::table($this->table)
      ->where("warna_id","!=","1")
			->where($where)
			->get();

		return json_encode($data);
	}

}
