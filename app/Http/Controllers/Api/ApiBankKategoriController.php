<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiBankKategoriController extends ApiController
{

	public function __construct()
	{
		$this->table = "tb_bank_kategori";
		$this->column = "bank_kategori";
	}

	public function show($id)
	{
		$where = array(
			$this->column . "_id" => $id
		);

		return $this->data($where);
	}

	public function data($where = NULL)
	{
		if (!empty($where)) {
			$where = $where;
		} else {
			$where = array();
		}

		$data = DB::table($this->table)
				->where($where)
				->get();



		return json_encode($data);
	}
}
