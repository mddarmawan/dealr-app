<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiController extends Controller
{
	protected $table;
	protected $column;
	protected $id = null;
	protected $dateColumn = null;

	// protected function __construct()
	// 	{
	// 		$this->middleware(function ($request, $next) {
	// 			//view()->share('modul', $this->get_modul());

	// 		return $next($request);
	// 	});
	// }

	public function getOne($id)
	{
		$where = array(
			$this->id => $id
		);
		
		return $this->data($where);
	}
	
	public function dateFilter($startDate = NULL, $endDate = NULL)
	{
		if ($this->dateColumn === null)
		{
			$column = "created_at";
		}
		else
		{
			$column = $this->dateColumn;
		}

		$whereDate = NULL;

		if ($startDate != NULL && $startDate != "ALL")
		{
			$whereDate[] = array($column, ">=", $startDate . "  00:00:00");
		}

		if ($endDate !== NULL && $endDate !== "ALL")
		{
			$whereDate[] = array($column, "<=", $endDate . "  23:59:59");
		}

		return $this->data($whereDate);
	}

	protected function store(Request $request = NULL, $import = NULL)
	{
		$result['success'] = 0;

		if (!empty($import)) {
			$params = $import;
		} else {
			$params = json_decode($request->data, TRUE);
		}

		foreach ($params as $key => $value)
		{
			if (strpos($key, "kerja") || strpos($key, "tgl") || strpos($key, "lahir"))
			{
				$params[$key] = date_format(date_create($value),"Y-m-d");
			}
		}

		if (DB::table($this->table)->insert($params))
		{
			$result['success'] = 1;
		}

		return response()->json($result, 200);
	}

	protected function update(Request $request, $id)
	{
		if ($this->id !== null) {
			$this->id = $this->id;
		} else {
			$this->id = $this->column . "_id";
		}

		$params = json_decode($request->data, TRUE);
		foreach ($params as $key => $value) {
			if (strpos($key, "kerja") || strpos($key, "tgl") || strpos($key, "lahir") || strpos($key, "po_cetak") || strpos($key, "po_tagihan") || strpos($key, "po_lunas") || strpos($key, "po_refund")) {
				$params[$key] = date_format(date_create($value),"Y-m-d");
			}
		}

		$proses = DB::table($this->table)
			->where($this->id, $id)
			->update($params);

		if ($proses) {
			return response()->json(array("success" => 1), 200);
		} else {
			return response()->json(array("success" => 0), 200);
		}
	}

	protected function destroy($id)
	{
		if ($this->id !== null) {
			$this->id = $this->id;
		} else {
			$this->id = $this->column . "_id";
		}

		$proses = DB::table($this->table)
			->where($this->id, $id)
			->update(array($this->column."_hapus"=>"1"));

		if ($proses) {
			return response()->json(array("success" => 1), 200);
		} else {
			return response()->json(array("success" => 0), 200);
		}
	}
}
