<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Api\ApiController;
use DB;


class ApiSafaController extends ApiController
{

	public function __construct()
	{
		$this->table = "users";
		$this->column = "";
	}

	public function show($id)
	{
		$where = array(
			"username" => $id
		);


    		$data = DB::table("tb_karyawan")
            ->join("tb_sales","sales_karyawan","karyawan_id")
            ->leftjoin($this->table,"users_karyawan","karyawan_id")
            ->select("username","karyawan_id","karyawan_nama","_status")
            ->where("sales_status","1")
            ->where("karyawan_status","1")
            ->where("sales_hapus","0")
            ->where("karyawan_hapus","0")
    				->where($where)
    				->count();

    if ($data>0){
      return 2;
    }

		return 1;
	}

	public function data($where = NULL)
	{
		if (!empty($where)) {
			$where = $where;
		} else {
			$where = array();
		}

		$data = DB::table("tb_karyawan")
        ->join("tb_sales","sales_karyawan","karyawan_id")
        ->leftjoin($this->table,"users_karyawan","karyawan_id")
        ->select("username","karyawan_id","karyawan_nama","_status")
        ->where("sales_status","1")
        ->where("karyawan_status","1")
        ->where("sales_hapus","0")
        ->where("karyawan_hapus","0")
				->where($where)
				->get();



		return json_encode($data);
	}


     protected function set(Request $request, $id,$kode)
     {
    	 $params = json_decode($request->data, TRUE);

       if($id!="null"){
        $proses = DB::table($this->table)->where("users_karyawan", $kode)->update($params);
       }else{
         $data['client_secret'] = $params['uid'];
         $data['users_karyawan'] = $kode;
         $data['password'] = Hash::make($params['password']);
         $data['username'] = $params['username'];
         $data['scope'] = '{"sales"}';
         $data['client_id'] = "encardms";
         $check = DB::table($this->table)->where("users_karyawan", $kode)->count();
         if ($check>0){
           $proses = DB::table($this->table)->where("users_karyawan", $kode)->update($data);
         }else{
           $proses = DB::table($this->table)->insert($data);
         }
         if ($proses){
           $proses = DB::table("tb_sales")->where("sales_karyawan",$kode)->update(array("sales_uid"=>$data['client_secret']));
         }
       }


    	 if ($proses) {
    			return response()->json(array("success"=>1), 200);
    		} else {
    			return response()->json(array("success"=>0), 200);
    		}
     }
}
