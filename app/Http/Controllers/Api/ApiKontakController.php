<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiKontakController extends ApiController
{

	public function __construct()
	{
		$this->table = "tb_kontak";
		$this->column = "kontak";
	}

	public function show($id)
	{
		$this->editing = TRUE;

		$where = array(
			$this->column . "_id" => $id
		);

		return $this->data($where);
	}

	public function order($type, $id)
	{
		$column = $this->column . "_" . $type;

		$where = array(
			$column => $id
		);

		return $this->data($where);
	}

	public function inactive()
	{
		$this->inactive = TRUE;

		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	public function deleted()
	{
		$this->deleted = TRUE;

		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	public function generateid($tipe){
		$tmp = DB::table("tb_kontak_tipe")->where("kontak_tipe_id",$tipe)->first();
		$kode = $tmp->kontak_tipe_kode;
		$id = $kode."001";

		$data = DB::table($this->table)
			->where("kontak_tipe",$tipe)->orderBy("kontak_id","DESC")->first();
		if (count($data)>0){
			$id = $data->kontak_id;
			$no = str_replace($kode,"",$id);
			$no++;
			if (strlen($no)==1){
				$id = $kode."00".$no;
			}else if (strlen($no)==2){
				$id = $kode."0".$no;
			}else{
				$id = $kode.$no;
			}
		}

		return json_encode($id);
	}

	public function data($where = NULL)
	{
		$result = array();

		if (empty($where)) {
			$where = array();
		}

		if (!isset($where[$this->column . "_status"])) {
			$where[$this->column . "_status"] = "1";
		}

		if (!isset($where[$this->column . "_hapus"])) {
			$where[$this->column . "_hapus"] = "0";
		}

		$data = DB::table($this->table)
			->join("tb_kontak_tipe","kontak_tipe","=","kontak_tipe_id")
			->leftjoin("tb_kontak_kategori","kontak_kategori","=","kontak_kategori_id")
			->where($where)->get();

		return json_encode($data);
	}
}
