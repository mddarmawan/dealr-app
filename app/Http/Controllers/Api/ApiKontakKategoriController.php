<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiKontakKategoriController extends Controller
{
	private $table;
	private $column;

	/**
	 * Menentukan Table dan Kolom yang akan di gunakan selanjutnya.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->table = "tb_kontak_kategori";
		$this->column = "kontak_kategori";
	}

	/**
	 * Menampilkan Data yang terpilih.
	 *
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		$where = array(
			$this->column . "_tipe" => $id
		);

		return $this->data($where);
	}
	/**
	 * Menampilkan seluruh Data.
	 *
	 * @param array $where
	 * @return void
	 */
	public function data($where = NULL)
	{
		if (!empty($where)) {
			$where = $where;
		} else {
			$where = array();
		}

		$data = DB::table($this->table)
				->where($where)
				->get();



		return json_encode($data);
	}
}
