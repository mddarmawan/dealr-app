<?php

namespace App\Http\Controllers\Api\Kasir;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use DB;

class ApiShowroomController extends ApiController
{
	public function __construct()
	{
		$this->table = "tb_spk_bayar";
		$this->column = "spk_bayar";
		$this->dateColumn = "spk_bayar_tgl";
	}

	public function data_bayar($id){
		$data = DB::table("tb_spk_bayar")
			->where("spk_bayar_spk",$id)
			->get();

		return json_encode($data);
	}

	public function data($whereDate = NULL)
	{
		$data = DB::table("tb_spk_bayar")
			->join("tb_spk", "tb_spk.spk_id", "=", "tb_spk_bayar.spk_bayar_spk")
			->leftjoin("tb_sales", "sales_uid", "=", "spk_sales")
			->leftjoin("tb_team", "team_id", "=", "sales_team")
			->leftjoin("tb_karyawan", "karyawan_id", "=", "sales_karyawan")
			->leftjoin("tb_bank", "tb_bank.bank_id", "tb_spk_bayar.spk_bayar_akun")
			->leftjoin("tb_spk_pelanggan", "tb_spk_pelanggan.spk_pel_spk", "=", "tb_spk.spk_id")
			->leftjoin("tb_pelanggan", "tb_pelanggan.pel_id", "=", "tb_spk_pelanggan.spk_pel_id")
			->leftjoin("tb_warna", "tb_warna.warna_id", "=", "tb_spk.spk_warna")
			->leftjoin("tb_variant", "tb_variant.variant_id", "=", "tb_spk.spk_kendaraan")
			->leftjoin("tb_type", "tb_variant.variant_type", "=", "tb_type.type_id");

		if (!empty($whereDate)) {
			$data->where(function ($query) use ($whereDate) {
				foreach ($whereDate as $key => $value) {
					$query->where($whereDate[$key][0], $whereDate[$key][1], $whereDate[$key][2]);
				}
			});
		}

		return 
			str_replace(
				array(':null', ':""'),
				array(':"KOSONG"', ':"-"'),
				json_encode($data->get())
			);
	}

	public function store(Request $request = NULL, $import = NULL)
	{
		$result['success'] = 0;
		$params = json_decode($request->data, TRUE);

		foreach ($params as $key => $value)
		{
			if (strpos($key, "kerja") || strpos($key, "tgl") || strpos($key, "lahir"))
			{
				$params[$key] = date_format(date_create($value),"Y-m-d");
			}
		}

		$params["spk_bayar_id"] = $this->getLast(TRUE);

		if (DB::table($this->table)->insert($params))
		{
			$result['success'] = 1;
		}

		return response()->json($result, 200);
	}

	public function getLast($array = FALSE)
	{
		$proses = 0;
		$data = DB::table("tb_spk_bayar")->orderBy("spk_bayar_id", "desc")->first();

		if (count($data) > 0)
		{
			$prefix = substr($data->spk_bayar_id, 0, 4);
			$suffix = sprintf("%05s", substr($data->spk_bayar_id, 4) + 1);
			$proses = 1;

			if ($array)
			{
				return $prefix . $suffix;
			}
			else
			{
				return response()->json(
					array(
						"success" => $proses,
						"result" => $prefix . $suffix
					), 200);
			}
		}
		else
		{
			$proses = 1;

			if ($array)
			{
				return "F-" . date("y") . "00001";
			}
			else
			{
				return response()->json(
					array(
						"success" => $proses,
						"result" => "F-" . date("y") . "00001"
					), 200);
			}
		}
	}
}
