<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class PenjualanController extends Controller
{
	public function permintaan()
	{
		$data['sales'] = DB::table("tb_sales")->join("tb_karyawan", "tb_karyawan.karyawan_id", "=", "tb_sales.sales_karyawan")->get();
		$data['pelanggan'] = DB::table("tb_pelanggan");
		$data['variant'] = DB::table("tb_type")->join("tb_variant", "tb_variant.variant_type", "=", "tb_type.type_id")->get();
		$data['vendor'] = (new Api\ApiKontakController)->order("tipe", 2);
		$data['warna'] = (new Api\ApiWarnaController)->data();

		return view('modules.penjualan.permintaan', compact('data'));
	}

	public function pesanan()
	{
		$data['sales'] = DB::table("tb_sales")->join("tb_karyawan", "tb_karyawan.karyawan_id", "=", "tb_sales.sales_karyawan")->get();
		$data['pelanggan'] = DB::table("tb_pelanggan");
		$data['variant'] = DB::table("tb_type")->join("tb_variant", "tb_variant.variant_type", "=", "tb_type.type_id")->get();
		$data['vendor'] = (new Api\ApiKontakController)->order("tipe", 2);
		$data['warna'] = (new Api\ApiWarnaController)->data();

		return view('modules.penjualan.pesanan', compact('data'));
	}

	public function diskon()
	{
		$data['spk'] = (new Api\Penjualan\ApiSPKController)->data();
		$data['sales'] = DB::table("tb_sales")->join("tb_karyawan", "tb_karyawan.karyawan_id", "=", "tb_sales.sales_karyawan")->get();

		return view('modules.penjualan.diskon', compact('data'));
	}

	public function matching()
	{
		$data['type'] = DB::table("tb_type")->get();
		$data['variant'] = DB::table("tb_variant")->get();
		$data['warna'] = (new Api\ApiWarnaController)->data();

		return view('modules.penjualan.matching', compact('data'));
	}

	public function leasing()
	{
		$data['sales'] = DB::table("tb_karyawan")->where("karyawan_jabatan", 1)->get();
		$data['team'] = DB::table("tb_team")->get();
		$data['leasing'] = (new Api\ApiKontakController)->order("tipe", 5);
		$data['asuransi'] = (new Api\ApiKontakController)->order("tipe", 6);
		$data['ajenis'] = DB::table("tb_asuransi_jenis")->get();

		return view('modules.penjualan.leasing', compact('data'));
	}

	public function penjualan()
	{
		$data['sales'] = DB::table("tb_sales")->join("tb_karyawan", "tb_karyawan.karyawan_id", "=", "tb_sales.sales_karyawan")->get();
		$data['pelanggan'] = DB::table("tb_pelanggan");
		$data['variant'] = DB::table("tb_type")->join("tb_variant", "tb_variant.variant_type", "=", "tb_type.type_id")->get();
		$data['vendor'] = (new Api\ApiKontakController)->order("tipe", 2);
		$data['warna'] = (new Api\ApiWarnaController)->data();

		return view('modules.penjualan.penjualan', compact('data'));
	}

	public function samsat()
	{
		$data['type'] = DB::table("tb_type")->get();
		$data['variant'] = DB::table("tb_variant")->get();

		return view('modules.penjualan.samsat', compact('data'));
	}
}
