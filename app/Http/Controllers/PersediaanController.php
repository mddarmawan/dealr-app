<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class PersediaanController extends Controller
{
	public function stok()
	{
		$data['type'] = DB::table("tb_type")->get();
		$data['variant'] = DB::table("tb_variant")->get();
		$data['warna'] = (new Api\ApiWarnaController)->data();
		$data['gudang'] = (new Api\ApiGudangController)->data();
		$data['ekspedisi'] = (new Api\ApiKontakController)->order("tipe", 3);

		return view('modules.persediaan.stok', compact('data'));
	}

	public function pindah()
	{
		$data['type'] = DB::table("tb_type")->get();
		$data['variant'] = DB::table("tb_variant")->get();
		$data['warna'] = (new Api\ApiWarnaController)->data();
		$data['stok'] = (new Api\Persediaan\ApiStokController)->filter("ALL");
		$data['gudang'] = (new Api\ApiGudangController)->data();

		return view('modules.persediaan.pindah', compact('data'));
	}
}
