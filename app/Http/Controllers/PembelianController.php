<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class PembelianController extends Controller
{
	public function pesanan()
	{
		$data['type'] = DB::table("tb_type")->get();
		$data['variant'] = DB::table("tb_variant")->get();
		$data['warna'] = (new Api\ApiWarnaController)->data();

		return view('modules.pembelian.pesanan', compact('data'));
	}

	public function pembelian()
	{
		$data['type'] = DB::table("tb_type")->get();
		$data['variant'] = DB::table("tb_variant")->get();
		$data['warna'] = (new Api\ApiWarnaController)->data();

		return view('modules.pembelian.pembelian', compact('data'));
	}

	public function retur()
	{
		$data['type'] = DB::table("tb_type")->get();
		$data['variant'] = DB::table("tb_variant")->get();
		$data['warna'] = (new Api\ApiWarnaController)->data();
		$data['stok'] = (new Api\Persediaan\ApiStokController)->filter("ALL");

		return view('modules.pembelian.retur', compact('data'));
	}

	public function rrn()
	{
		$data['variant'] = DB::table("tb_variant")->get();
		$data['warna'] = (new Api\ApiWarnaController)->data();

		return view('modules.pembelian.rrn', compact('data'));
	}

}
